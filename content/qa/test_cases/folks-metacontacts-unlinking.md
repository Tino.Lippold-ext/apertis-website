+++
date = "2018-06-25"
weight = 100

title = "folks-metacontacts-unlinking"

aliases = [
    "/old-wiki/QA/Test_Cases/folks-metacontacts-unlinking"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
