+++
date = "2018-11-01"
weight = 100

title = "apparmor-gstreamer1.0"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-gstreamer1.0"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
