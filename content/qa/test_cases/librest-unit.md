+++
date = "2018-10-26"
weight = 100

title = "librest-unit"

aliases = [
    "/qa/test_cases/librest.md",
    "/old-wiki/QA/Test_Cases/librest",
    "/old-wiki/QA/Test_Cases/librest-unit"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
