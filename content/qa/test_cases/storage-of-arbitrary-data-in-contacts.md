+++
date = "2018-06-25"
weight = 100

title = "storage-of-arbitrary-data-in-contacts"

aliases = [
    "/old-wiki/QA/Test_Cases/storage-of-arbitrary-data-in-contacts"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
