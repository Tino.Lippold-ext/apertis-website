+++
date = "2018-06-25"
weight = 100

title = "webkit2gtk-lazy-click"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2gtk-lazy-click"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
