+++
date = "2018-10-03"
weight = 100

title = "apparmor-session-lockdown-launcher-whitelist"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-session-lockdown-launcher-whitelist"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
