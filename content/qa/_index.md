+++
date = "2019-02-28"
weight = 100

title = "Quality Assurance"

aliases = [
    "/old-wiki/QA"
]
+++

This is the page for the Quality Assurance team. It includes links for
QA services, documentation, tests and any information related to
the different QA processes in the project.

# Apertis issues

Apertis uses the [Apertis issues board](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues) to keep track of the current known issues with the project, as well as any proposed enhancement from the community.

Community members are encouraged to contribute to it by reporting any issues they may find while working with Apertis, or by suggesting improvements to the project.

# Weekly tests reports

The images are tested weekly using both manual and automated tests as
documented on the [test case site](https://qa.apertis.org/).
Reports of the testing performed are available on the
[test report site](https://qa.apertis.org/).

# Services

- [LAVA (Linaro Automated Validation Architecture)](https://lava.collabora.co.uk)

  - [LAVA Documentation](https://lava.collabora.co.uk/static/docs/v2/index.html)

# Tools

Tools used for QA tasks and infrastructure.

- [apertis-test-cases](https://gitlab.apertis.org/tests/apertis-test-cases):
  Source of automated and manual test cases. Used to generate the
  [test case site](https://qa.apertis.org/) and to perform automated tests.
- [apertis-test-cases-web](https://gitlab.apertis.org/tests/apertis-test-cases-web):
  Source used to generate the index page for the
  [test case site](https://qa.apertis.org/).
- [Apertis infrastructure dashboard](https://gitlab.apertis.org/infrastructure/dashboard):
  Tracking of potential packaging issues.
- [lqa](https://gitlab.collabora.com/collabora/lqa): It submits the
  automated tests jobs to LAVA. It also offers a LAVA API that can be
  used by other scripts.
