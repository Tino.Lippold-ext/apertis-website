+++
date = "2023-03-16"
weight = 100

title = "v2024dev2 Release schedule"
+++

The v2024dev2 release cycle is scheduled to start in April 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2023-04-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2023-05-17        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2023-05-24        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2023-06-21        |
| RC testing                                                                                               | 2023-06-01..06-14 |
| v2024dev2 release                                                                                        | 2023-06-22        |

{{% notice warning %}}
The release v2024dev2 will be the first based in Debian Bookworm, after the rebase effort is completed. Due to
this fact and the early stage of this release in the release cycle the following decisions have been made:
- The RC date for v2024dev2 was moved to 2023.06.21 in order to provide a more robust images after the rebase.
- The RC testing will be only one day since only automated LAVA testing will be done.
{{% /notice %}}

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
