+++
date = "2023-03-16"
weight = 100

title = "v2024dev1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024dev1** is the second development release of the Apertis
v2024 stable release flow that will lead to the LTS **Apertis v2024.0**
release in March 2024.

This Apertis release is built on top of Debian Bullseye with several
customizations. It currently ships with the Linux kernel 6.1.x series.
Later releases in the v2024 channel will be tracking newer kernel versions
up to the next LTS, as well as Debian Bookworm.

Test results for the v2024dev1 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024dev1/20230227.0018/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024dev1/20230227.0018/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024dev1/20230227.0018/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024dev1/20230227.0018/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - **2023 Q1: v2024dev1**
  - 2023 Q2: v2024dev2
  - 2023 Q3: v2024dev3
  - 2023 Q4: v2024pre
  - 2024 Q1: v2024.0
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024dev1.0 images](https://images.apertis.org/release/v2024dev1/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/fixedfunction/apertis_ostree_v2024dev1-fixedfunction-amd64-uefi_v2024dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/hmi/apertis_ostree_v2024dev1-hmi-amd64-uefi_v2024dev1.0.img.gz) | [base SDK](https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/basesdk/apertis_v2024dev1-basesdk-amd64-sdk_v2024dev1.0.ova) | [SDK](https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/sdk/apertis_v2024dev1-sdk-amd64-sdk_v2024dev1.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev1/v2024dev1.0/armhf/fixedfunction/apertis_ostree_v2024dev1-fixedfunction-armhf-uboot_v2024dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev1/v2024dev1.0/armhf/hmi/apertis_ostree_v2024dev1-hmi-armhf-uboot_v2024dev1.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev1/v2024dev1.0/arm64/fixedfunction/apertis_ostree_v2024dev1-fixedfunction-arm64-uboot_v2024dev1.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024dev1/v2024dev1.0/arm64/fixedfunction/apertis_ostree_v2024dev1-fixedfunction-arm64-rpi64_v2024dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev1/v2024dev1.0/arm64/hmi/apertis_ostree_v2024dev1-hmi-arm64-rpi64_v2024dev1.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2024dev1 package list

The full list of packages available from the v2024dev1 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024dev1](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024dev1.tsv)

#### Apertis v2024dev1 repositories

    deb https://repositories.apertis.org/apertis/ v2024dev1 target development sdk

## New features

## Initial support for running integration tests on MRs

Following the guidelines described in the [Apertis test strategy]({{< ref "test-strategy.md" >}})
this release provides initial support for running automated tests on LAVA in the
context of a MR. With this feature, packages changes can be validated by running integration tests
before landing them to main branches. 

It is currently recommended that only packages where potential regressions
may have a high-impact on the system stability have this feature enabled, given the extra
resources required to run the tests.

As before, all available integration tests are also run regularly on daily image builds.

## Build and integration

### Support for more fine-grained access roles in QA Report App

With the goal of providing better access control to the QA Report App, now three different roles
are defined to allow performing the following actions: view results, submit manual tests results and
tag images. These changes should allow teams to better split responsibilities, by only allowing
the necessary access to specific users/groups. 

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev1-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #155](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/155) aum-offline-upgrade-branch: test failed
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #169](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/169) aum-out-of-space: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up FS corruption issue - part 2
- [Issue #218](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/218) agl-compositor not showing on UP SQUARED 6000 board setup of lilliput display
- [Issue #228](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/228) sdk-debos-image-building: test failed
- [Issue #270](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/270) Issue in playing the Video on HMI

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #217](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/217) AUM out of space tests fail on UP Squared 6000 board
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #247](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/247) tiny-container-user-basic: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #256](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/256) OBS: Backend doesn’t start immediately upon restart
- [Issue #257](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/257) sdk-performance-tools-gprof: test failed
- [Issue #260](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/260) tiny-container-user-folder-sharing: test failed
- [Issue #262](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/262) tiny-container-system-aa-enforcement: test failed
- [Issue #272](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/272) QA Report App does not handle task-per-release when using Phabricator
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #281](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/281) tiny-container-user-aa-enforcement: test failed

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #40](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/40) evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #93](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/93) "Error: GDBus.Error:org.freedesktop.DBus.Error.InvalidArgs" logs seen during bluez-phone test
