+++
date = "2022-12-15"
weight = 100

title = "v2024dev1 Release schedule"
+++

The v2024dev1 release cycle is scheduled to start in January 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2023-01-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2023-02-15        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2023-02-22        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2023-03-01        |
| RC testing                                                                                               | 2023-03-02..03-15 |
| v2024dev1 release                                                                                        | 2023-03-16        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
