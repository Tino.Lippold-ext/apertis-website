+++
date = "2023-06-08"
weight = 100

title = "v2023.2 Release schedule"
+++

The v2023.2 release cycle is scheduled to start in July 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2023-07-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2023-08-09        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2023-08-16        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2023-08-23        |
| RC testing                                                                                               | 2023-08-24..09-06 |
| v2023.0 release                                                                                          | 2023-09-07        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
