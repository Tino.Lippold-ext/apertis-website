+++
date = "2020-06-08"
weight = 100

title = "v2019.3 ReleaseNotes"
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis v2019.3 Release

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2019.3** is the fourth **stable** release of the Apertis
v2019 [release
flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2019 release stream until June
2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2019.3 release are available in the following test
reports:

  - [APT     images](https://qa.apertis.org/report/v2019/20200605.0) ([internal](https://qa.apertis.org/report/v2019/20200605.1))
  - [OSTree  images](https://qa.apertis.org/report/v2019/20200605.0/ostree) ([internal](https://qa.apertis.org/report/v2019/20200605.1/ostree))
  - [NFS  artifacts](https://qa.apertis.org/report/v2019/20200605.0/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2019/20200605.0/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2019.7.

## Release flow

  - 2019 Q1: v2019dev0
  - 2019 Q2: v2019pre
  - 2019 Q3: v2019.0
  - 2019 Q4: v2019.1
  - 2020 Q1: v2019.2
  - 2020 Q2: **v2019.3**
  - 2020 Q3: v2019.4
  - 2020 Q4: v2019.5
  - 2021 Q1: v2019.6
  - 2021 Q2: v2019.7

### Release downloads

| [Apertis v2019.3 images](https://images.apertis.org/release/v2019/v2019.3/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2019/v2019.3/amd64/minimal/apertis_v2019-minimal-amd64-uefi_v2019.3.img.gz) | [target](https://images.apertis.org/release/v2019/v2019.3/amd64/target/apertis_v2019-target-amd64-uefi_v2019.3.img.gz) | [base SDK](https://images.apertis.org/release/v2019/v2019.3/amd64/basesdk/apertis_v2019-basesdk-amd64-sdk_v2019.3.vdi.gz) | [SDK](https://images.apertis.org/release/v2019/v2019.3/amd64/sdk/apertis_v2019-sdk-amd64-sdk_v2019.3.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2019/v2019.3/armhf/minimal/apertis_v2019-minimal-armhf-uboot_v2019.3.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2019/v2019.3/arm64/minimal/apertis_v2019-minimal-arm64-uboot_v2019.3.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2019 repositories

    deb https://repositories.apertis.org/apertis/ v2019 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2019-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Buster.

## Deprecations and ABI/API breaks

Being a point release, no new deprecations or ABI breaks are part of
this release

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2019 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2019)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2019/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at:

  https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (7)
 - [https://phabricator.apertis.org/T6885](T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [https://phabricator.apertis.org/T6889](T6889)	gcr package contains GPLV3 AND MPL1.1 code 
 - [https://phabricator.apertis.org/T7011](T7011)	No audio is heard after rooting audio to jack and performing a reboot
 - [https://phabricator.apertis.org/T7047](T7047)	Image build failed for minimal armhf and SDK images
 - [https://phabricator.apertis.org/T7056](T7056)	FAILED: Error running command: ['ssh-copy-id', 'user@127.0.0.1' errors are shown when running the sdk-persistent testcases
 - [https://phabricator.apertis.org/T7124](T7124)	dbus-dos-reply-time: 6_dbus-dos-reply-time test failed
 - [https://phabricator.apertis.org/T7139](T7139)	Incomplete description of the contribution process into Apertis

### Normal (162)
 - [https://phabricator.apertis.org/T2043](T2043)	property changed signal in org.bluez.Network1 is not emiting when PAN disconnects
 - [https://phabricator.apertis.org/T2853](T2853)	GStreamer playbin prioritises imxeglvivsink over clutterautovideosink
 - [https://phabricator.apertis.org/T2896](T2896)	Crash when initialising egl on ARM target
 - [https://phabricator.apertis.org/T2930](T2930)	Develop test case for out of screen events in Wayland images
 - [https://phabricator.apertis.org/T3121](T3121)	Test apps are failing in Liblightwood with the use of GTest
 - [https://phabricator.apertis.org/T3210](T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [https://phabricator.apertis.org/T3217](T3217)	VirtualBox display freezes when creating multiple notifications at once and interacting (hover and click) with them
 - [https://phabricator.apertis.org/T3233](T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [https://phabricator.apertis.org/T3291](T3291)	tracker tests: Error creating thumbnails: No poster key found in metadata
 - [https://phabricator.apertis.org/T3321](T3321)	libgles2-vivante-dev is not installable
 - [https://phabricator.apertis.org/T3920](T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [https://phabricator.apertis.org/T3970](T3970)	Ensure that the arch:all packages in the archive match the arch-specific versions
 - [https://phabricator.apertis.org/T4092](T4092)	Containers fail to load on Gen4 host
 - [https://phabricator.apertis.org/T4242](T4242)	gnutls depends on old abandoned package gmp4 due to licensing reasons
 - [https://phabricator.apertis.org/T4293](T4293)	Preseed action is needed for Debos
 - [https://phabricator.apertis.org/T4307](T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [https://phabricator.apertis.org/T4394](T4394)	gupnp-services: browsing and introspection tests fail
 - [https://phabricator.apertis.org/T4409](T4409)	tracker-indexing-mass-storage test case fails
 - [https://phabricator.apertis.org/T4422](T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [https://phabricator.apertis.org/T4444](T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [https://phabricator.apertis.org/T4502](T4502)	SDK hangs when trying to execute bluez-hfp testcase
 - [https://phabricator.apertis.org/T4568](T4568)	Ribchester mount unit depends on Btrfs
 - [https://phabricator.apertis.org/T4660](T4660)	Eclipse Build is not working for HelloWorld App
 - [https://phabricator.apertis.org/T4693](T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [https://phabricator.apertis.org/T4755](T4755)	Seek/Pause option does not work correctly on YouTube
 - [https://phabricator.apertis.org/T5053](T5053)	Canterbury needs to explicitly killed to relaunch the Mildenhall compositor 
 - [https://phabricator.apertis.org/T5128](T5128)	cgroups-resource-control: cpu-shares test failed
 - [https://phabricator.apertis.org/T5153](T5153)	tracker-indexing-local-storage: test failed
 - [https://phabricator.apertis.org/T5210](T5210)	apparmor-tracker: tracker.normal.expected test failed
 - [https://phabricator.apertis.org/T5256](T5256)	gupnp-services: test_service_introspection test failed
 - [https://phabricator.apertis.org/T5284](T5284)	gupnp-services: test_service_browsing test failed
 - [https://phabricator.apertis.org/T5335](T5335)	"Setup Install to Target" option is not showing on eclipse 
 - [https://phabricator.apertis.org/T5351](T5351)	rhosydd: integration test failed
 - [https://phabricator.apertis.org/T5468](T5468)	build-snapshot: allow to build packages without `autogen.sh` script
 - [https://phabricator.apertis.org/T5487](T5487)	Wi-Fi search button is missing in wifi application
 - [https://phabricator.apertis.org/T5498](T5498)	traprain: gnome-desktop-testing test failed
 - [https://phabricator.apertis.org/T5576](T5576)	bluez-hfp testcase fails
 - [https://phabricator.apertis.org/T5611](T5611)	`kbd` package has broken/problematic dependencies
 - [https://phabricator.apertis.org/T5739](T5739)	Debos crash when a recipes embbed recipes with ostree-commit and ostree-deploy
 - [https://phabricator.apertis.org/T5747](T5747)	The /boot mountpoint is not empty
 - [https://phabricator.apertis.org/T5748](T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [https://phabricator.apertis.org/T5757](T5757)	apparmor-gstreamer1-0: run-test-sh test failed
 - [https://phabricator.apertis.org/T5763](T5763)	Fix the RFS tiny images filesystem structure
 - [https://phabricator.apertis.org/T5765](T5765)	apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed
 - [https://phabricator.apertis.org/T5769](T5769)	cgroups-resource-control: 8_cgroups-resource-control test failed
 - [https://phabricator.apertis.org/T5770](T5770)	folks-alias-persistence: 4_folks-alias-persistence test failed
 - [https://phabricator.apertis.org/T5803](T5803)	aa_get_complaints.sh script needs to be run with sudo permission
 - [https://phabricator.apertis.org/T5818](T5818)	apparmor-tumbler: 9_apparmor-tumbler test failed
 - [https://phabricator.apertis.org/T5819](T5819)	cgroups-resource-control: 6_cgroups-resource-control test failed
 - [https://phabricator.apertis.org/T5821](T5821)	gettext-i18n: gettext-i18n test failed
 - [https://phabricator.apertis.org/T5833](T5833)	apparmor-tumbler: 8_apparmor-tumbler test failed
 - [https://phabricator.apertis.org/T5837](T5837)	apparmor-utils: apparmor-utils test failed
 - [https://phabricator.apertis.org/T5838](T5838)	gettext-i18n: 11_gettext-i18n test failed
 - [https://phabricator.apertis.org/T5848](T5848)	canterbury: 3_canterbury test failed
 - [https://phabricator.apertis.org/T5852](T5852)	Terminal comes up inside the Launcher
 - [https://phabricator.apertis.org/T5857](T5857)	gettext-i18n: 10_gettext-i18n test failed
 - [https://phabricator.apertis.org/T5861](T5861)	dbus-installed-tests: trying to overwrite mktemp.1.gz
 - [https://phabricator.apertis.org/T5863](T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [https://phabricator.apertis.org/T5896](T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [https://phabricator.apertis.org/T5897](T5897)	apparmor-ofono test fails
 - [https://phabricator.apertis.org/T5900](T5900)	evolution-sync-bluetooth test fails
 - [https://phabricator.apertis.org/T5901](T5901)	eclipse-plugins-apertis-management package is missing
 - [https://phabricator.apertis.org/T5902](T5902)	A2DP test is failing as part of the bluez-phone test
 - [https://phabricator.apertis.org/T5905](T5905)	No audio/sound is heard while making a call in the bluez-hfp testcase
 - [https://phabricator.apertis.org/T5906](T5906)	Video does not stream in WebKit on the i.MX6 internal images 
 - [https://phabricator.apertis.org/T5913](T5913)	ofono-tests package is missing 
 - [https://phabricator.apertis.org/T5929](T5929)	connman-pan-tethering test fail 
 - [https://phabricator.apertis.org/T5930](T5930)	connman-pan-network-access test fails
 - [https://phabricator.apertis.org/T5931](T5931)	connman-usb-tethering test fails
 - [https://phabricator.apertis.org/T5933](T5933)	ifconfig command need to be run with sudo permission
 - [https://phabricator.apertis.org/T5935](T5935)	libfolks-ofono25 package not found
 - [https://phabricator.apertis.org/T5988](T5988)	canterbury: 2_canterbury test failed
 - [https://phabricator.apertis.org/T5991](T5991)	newport: 7_newport test failed
 - [https://phabricator.apertis.org/T5992](T5992)	newport: 6_newport test failed
 - [https://phabricator.apertis.org/T5993](T5993)	rhosydd: 8_rhosydd test failed
 - [https://phabricator.apertis.org/T5994](T5994)	rhosydd: 7_rhosydd test failed
 - [https://phabricator.apertis.org/T5997](T5997)	traprain: 10_traprain test failed
 - [https://phabricator.apertis.org/T5998](T5998)	traprain: 9_traprain test failed
 - [https://phabricator.apertis.org/T6001](T6001)	eclipse-plugins-remote-debugging test fails
 - [https://phabricator.apertis.org/T6003](T6003)	Thumb nails of songs are not seen on the SDK
 - [https://phabricator.apertis.org/T6008](T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [https://phabricator.apertis.org/T6011](T6011)	Booting the SDK in VirtualBox appears to be broken with the 6.0.4 VirtualBox Guest Extensions
 - [https://phabricator.apertis.org/T6012](T6012)	webkit2gtk-event-handling-redesign test fails on the amd64 ostree images
 - [https://phabricator.apertis.org/T6015](T6015)	simple-agent not found
 - [https://phabricator.apertis.org/T6017](T6017)	bluez-avrcp-volume test fails on the sdk and base sdk
 - [https://phabricator.apertis.org/T6024](T6024)	folks-inspect: command not found 
 - [https://phabricator.apertis.org/T6052](T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue) 
 - [https://phabricator.apertis.org/T6057](T6057)	gupnp-services: 13_gupnp-services test failed
 - [https://phabricator.apertis.org/T6077](T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [https://phabricator.apertis.org/T6078](T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [https://phabricator.apertis.org/T6111](T6111)	traprain: 7_traprain test failed
 - [https://phabricator.apertis.org/T6130](T6130)	frome: 5_frome test failed
 - [https://phabricator.apertis.org/T6131](T6131)	frome: 6_frome test failed
 - [https://phabricator.apertis.org/T6231](T6231)	gitlab-to-obs: Handle packages changing component across releases 
 - [https://phabricator.apertis.org/T6243](T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [https://phabricator.apertis.org/T6277](T6277)	ldconfig: Warning comes up when we do an apt-get upgrade on the i.MX6 
 - [https://phabricator.apertis.org/T6291](T6291)	Generated lavaphabbridge error report email provides wrong link for full report link 
 - [https://phabricator.apertis.org/T6292](T6292)	gettext-i18n: 9_gettext-i18n test failed
 - [https://phabricator.apertis.org/T6296](T6296)	gupnp-services: 11_gupnp-services test failed
 - [https://phabricator.apertis.org/T6307](T6307)	apparmor-pulseaudio: 14_apparmor-pulseaudio test failed
 - [https://phabricator.apertis.org/T6349](T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [https://phabricator.apertis.org/T6350](T6350)	sdk-debug-tools-gdb: 4_sdk-debug-tools-gdb test failed
 - [https://phabricator.apertis.org/T6351](T6351)	sdk-debug-tools-strace: 5_sdk-debug-tools-strace test failed
 - [https://phabricator.apertis.org/T6352](T6352)	sdk-debug-tools-valgrind: 6_sdk-debug-tools-valgrind test failed
 - [https://phabricator.apertis.org/T6366](T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [https://phabricator.apertis.org/T6369](T6369)	apparmor-gstreamer1-0: 12_apparmor-gstreamer1-0 test failed
 - [https://phabricator.apertis.org/T6444](T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [https://phabricator.apertis.org/T6446](T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [https://phabricator.apertis.org/T6466](T6466)	Thumbnails for songs/videos are not shown when opening the app too soon
 - [https://phabricator.apertis.org/T6614](T6614)	aum-update-rollback-tests/armhf: Rollback situation is not reproduced on public armhf target and internal images
 - [https://phabricator.apertis.org/T6620](T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [https://phabricator.apertis.org/T6652](T6652)	tests: apparmor-tracker, apparmor-geoclue: absent malicious library
 - [https://phabricator.apertis.org/T6656](T6656)	apparmor-geoclue: 8_apparmor-geoclue test failed
 - [https://phabricator.apertis.org/T6657](T6657)	apparmor-tracker: 21_apparmor-tracker test failed
 - [https://phabricator.apertis.org/T6662](T6662)	SDK: command-not-found package is broken
 - [https://phabricator.apertis.org/T6666](T6666)	apparmor-geoclue: 7_apparmor-geoclue test failed
 - [https://phabricator.apertis.org/T6667](T6667)	apparmor-tracker: 20_apparmor-tracker test failed
 - [https://phabricator.apertis.org/T6669](T6669)	Stop building cross compilers tools and libraries for not supported platforms 
 - [https://phabricator.apertis.org/T6670](T6670)	Remove or move git-mediawiki package from the development repo
 - [https://phabricator.apertis.org/T6680](T6680)	Drop mkdocs package from development repository
 - [https://phabricator.apertis.org/T6681](T6681)	Fix btrfs packages in the development repository
 - [https://phabricator.apertis.org/T6682](T6682)	Drop or fix installation for git-all package in development repository
 - [https://phabricator.apertis.org/T6683](T6683)	Drop or fix installation for gccbrig package in development repository
 - [https://phabricator.apertis.org/T6684](T6684)	Move dh-python package from target to development repository 
 - [https://phabricator.apertis.org/T6685](T6685)	Fix gstreamer1.0-gl package in target repository
 - [https://phabricator.apertis.org/T6686](T6686)	Move kernel-wedge package from target to development repository 
 - [https://phabricator.apertis.org/T6687](T6687)	Move kdump-tools package from target to development repository
 - [https://phabricator.apertis.org/T6688](T6688)	Fix libblockdev-btrfs2 in target repository
 - [https://phabricator.apertis.org/T6689](T6689)	Move skales package from target to development repository
 - [https://phabricator.apertis.org/T6690](T6690)	Fix mesa-vdpau-drivers package in target repository
 - [https://phabricator.apertis.org/T6691](T6691)	Fix mesa-va-drivers package in target repository
 - [https://phabricator.apertis.org/T6692](T6692)	Move makedumpfile package from target to development repository
 - [https://phabricator.apertis.org/T6693](T6693)	Move lsb-release package from target to development repository
 - [https://phabricator.apertis.org/T6694](T6694)	Fix firmware-linux package in target
 - [https://phabricator.apertis.org/T6695](T6695)	Remove remaining hotdoc dbgsym package from development repository
 - [https://phabricator.apertis.org/T6727](T6727)	FTBFS: Apertis v2020pre package build failures
 - [https://phabricator.apertis.org/T6768](T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [https://phabricator.apertis.org/T6773](T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [https://phabricator.apertis.org/T6783](T6783)	Kernel  trace on armhf board with attached screen
 - [https://phabricator.apertis.org/T6795](T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [https://phabricator.apertis.org/T6806](T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [https://phabricator.apertis.org/T6816](T6816)	apertis.org: Replace the slogan
 - [https://phabricator.apertis.org/T6834](T6834)	Apertis-flatdeb build issues.
 - [https://phabricator.apertis.org/T6841](T6841)	sdk-code-analysis-tools-splint: 4_sdk-code-analysis-tools-splint test failed
 - [https://phabricator.apertis.org/T6842](T6842)	sdk-debug-tools-gdb: 5_sdk-debug-tools-gdb test failed
 - [https://phabricator.apertis.org/T6843](T6843)	sdk-debug-tools-strace: 6_sdk-debug-tools-strace test failed
 - [https://phabricator.apertis.org/T6844](T6844)	sdk-debug-tools-valgrind: 7_sdk-debug-tools-valgrind test failed
 - [https://phabricator.apertis.org/T6855](T6855)	busybox wget does not work with https proxy
 - [https://phabricator.apertis.org/T6863](T6863)	qa-report-app: Creates duplicate tasks on Phabricator  
 - [https://phabricator.apertis.org/T6887](T6887)	ARM64 target does not reboot automatically
 - [https://phabricator.apertis.org/T6891](T6891)	apparmor-pulseaudio: 13_apparmor-pulseaudio test failed
 - [https://phabricator.apertis.org/T6903](T6903)	U-Boot boot counter is used for AMD64 & ARM64
 - [https://phabricator.apertis.org/T6904](T6904)	apertis-update-manager-usb-unplug test fails on ARM64 image  
 - [https://phabricator.apertis.org/T6961](T6961)	audio-backhandling feature fails 
 - [https://phabricator.apertis.org/T6968](T6968)	apparmor-session-lockdown-no-deny: 9_apparmor-session-lockdown-no-deny test failed
 - [https://phabricator.apertis.org/T6969](T6969)	apparmor-session-lockdown-no-deny: 15_apparmor-session-lockdown-no-deny test failed
 - [https://phabricator.apertis.org/T6970](T6970)	apparmor-tracker: 22_apparmor-tracker test failed
 - [https://phabricator.apertis.org/T7000](T7000)	DNS resolution does not work in Debos on some setups
 - [https://phabricator.apertis.org/T7012](T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [https://phabricator.apertis.org/T7016](T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [https://phabricator.apertis.org/T7018](T7018)	Chaiwala-logo doesn't come up on boot up
 - [https://phabricator.apertis.org/T7069](T7069)	Ext bootcounter: U-Boot error read/write on Arm64 (Raspberry Pi 3) due to missing EXT4_WRITE support

### Low (20)
 - [https://phabricator.apertis.org/T1809](T1809)	Upstream: linux-tools-generic should depend on lsb-release
 - [https://phabricator.apertis.org/T1924](T1924)	telepathy-ring: Review and fix SMS test
 - [https://phabricator.apertis.org/T1964](T1964)	Mildenhall compositor crops windows
 - [https://phabricator.apertis.org/T2142](T2142)	Power button appers to be disabled on target 
 - [https://phabricator.apertis.org/T2226](T2226)	Network search pop-up isn't coming up in wi-fi settings
 - [https://phabricator.apertis.org/T2367](T2367)	Videos are hidden when Eye is launched
 - [https://phabricator.apertis.org/T2483](T2483)	Video doesn't play when toggling from full screen to detail view
 - [https://phabricator.apertis.org/T2498](T2498)	Simulator screen is not in center but left aligned 
 - [https://phabricator.apertis.org/T2704](T2704)	The video player window is split into 2 frames in default view
 - [https://phabricator.apertis.org/T3161](T3161)	If 2 drawers are activated, the most recent one hides behind the older one, instead of coming on top of older one.
 - [https://phabricator.apertis.org/T3537](T3537)	cgroups-resource-control: test network-cgroup-prio-class failed
 - [https://phabricator.apertis.org/T3759](T3759)	Status bar is not getting updated with the current song/video being played 
 - [https://phabricator.apertis.org/T3955](T3955)	rhosydd-client crashes when displaying vehicle properties for mock backend
 - [https://phabricator.apertis.org/T3965](T3965)	Rhosydd service crashes when client exits on some special usecases (Refer description for it)
 - [https://phabricator.apertis.org/T4166](T4166)	On multiple re-entries from settings to eye the compositor hangs 
 - [https://phabricator.apertis.org/T4243](T4243)	Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll test case
 - [https://phabricator.apertis.org/T4296](T4296)	Segmentation fault is observed on closing the mildenhall-compositor
 - [https://phabricator.apertis.org/T4490](T4490)	webkit2gtk-drag-and-drop doesn't work with touch 
 - [https://phabricator.apertis.org/T5898](T5898)	traffic-control-basic test fails
 - [https://phabricator.apertis.org/T6065](T6065)	apt-get dist-upgrade fails on SDK

### Lowest (80)
 - [https://phabricator.apertis.org/T789](T789)	Remove unnecessary folks package dependencies for automated tests
 - [https://phabricator.apertis.org/T1556](T1556)	No connectivity Popup is not seen when the internet is disconnected.
 - [https://phabricator.apertis.org/T1960](T1960)	remove INSTALL, aclocal.m4 files from langtoft
 - [https://phabricator.apertis.org/T2028](T2028)	Documentation is not available from the main folder
 - [https://phabricator.apertis.org/T2224](T2224)	apparmor-libreoffice: libreoffice.normal.expected fails: ods_to_pdf: fail [Bugzilla bug #331] 
 - [https://phabricator.apertis.org/T2299](T2299)	Clutter_text_set_text API redraws entire clutterstage
 - [https://phabricator.apertis.org/T2317](T2317)	libgrassmoor: executes tracker-control binary 
 - [https://phabricator.apertis.org/T2318](T2318)	mildenhall-settings: does not generate localization files from source
 - [https://phabricator.apertis.org/T2475](T2475)	Theme ,any F node which is a child of an E node is not working for Apertis widgets.
 - [https://phabricator.apertis.org/T2781](T2781)	Horizontal scroll is not shown on GtkClutterLauncher
 - [https://phabricator.apertis.org/T2785](T2785)	The background HMI is blank on clicking the button for Power OFF
 - [https://phabricator.apertis.org/T2788](T2788)	Share links to facebook, twitter are nbt working in browser (GtkClutterLauncher)
 - [https://phabricator.apertis.org/T2790](T2790)	Background video is not played in some website with GtkClutterLauncher 
 - [https://phabricator.apertis.org/T2833](T2833)	Interaction with PulseAudio not allowed by its AppArmor profile
 - [https://phabricator.apertis.org/T2858](T2858)	shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes /var/root/.cache/dconf/ 
 - [https://phabricator.apertis.org/T2889](T2889)	Cannot open/view pdf documents in browser (GtkClutterLauncher)
 - [https://phabricator.apertis.org/T2890](T2890)	Zoom in feature does not work on google maps 
 - [https://phabricator.apertis.org/T2892](T2892)	Printing hotdoc webpage directly results in misformatted document
 - [https://phabricator.apertis.org/T2917](T2917)	Images for the video links are not shown in news.google.com on GtkClutterLauncher 
 - [https://phabricator.apertis.org/T2995](T2995)	Focus in launcher rollers broken because of copy/paste errors
 - [https://phabricator.apertis.org/T3008](T3008)	beep audio decoder gives errors continously
 - [https://phabricator.apertis.org/T3171](T3171)	Unusable header in Traprain section in Devhelp
 - [https://phabricator.apertis.org/T3174](T3174)	Clang package fails to install appropriate egg-info needed by hotdoc
 - [https://phabricator.apertis.org/T3219](T3219)	Canterbury messes up kerning when .desktop uses unicode chars
 - [https://phabricator.apertis.org/T3237](T3237)	make check fails on libbredon package for wayland warnings
 - [https://phabricator.apertis.org/T3280](T3280)	Cannot open links within website like yahoo.com
 - [https://phabricator.apertis.org/T3319](T3319)	mx6qsabrelite: linking issue with libgstimxeglvivsink.so and libgstimxvpu.so gstreamer plugins
 - [https://phabricator.apertis.org/T3332](T3332)	Compositor seems to hide the bottom menu of a webpage 
 - [https://phabricator.apertis.org/T3430](T3430)	Spacing issues between text and selection box in website like amazon
 - [https://phabricator.apertis.org/T3431](T3431)	Content on a webpage doesn't load in sync with the scroll bar
 - [https://phabricator.apertis.org/T3433](T3433)	Resizing the window causes page corruption 
 - [https://phabricator.apertis.org/T3506](T3506)	Confirm dialog status updated before selecting the confirm option YES/NO
 - [https://phabricator.apertis.org/T3517](T3517)	webview Y offset not considered to place, full screen video on youtube webpage
 - [https://phabricator.apertis.org/T3563](T3563)	GObject Generator link throws 404 error
 - [https://phabricator.apertis.org/T3564](T3564)	GLib, GIO Reference Manual links are incorrectly swapped 
 - [https://phabricator.apertis.org/T3580](T3580)	Canterbury entry-point launching hides global popups, but only sometimes
 - [https://phabricator.apertis.org/T3588](T3588)	<abstractions/chaiwala-base> gives privileges that not every app-bundle should have
 - [https://phabricator.apertis.org/T3631](T3631)	Segmentation fault when disposing test executable of mildenhall
 - [https://phabricator.apertis.org/T3647](T3647)	The web runtime doesn't set the related view when opening new windows
 - [https://phabricator.apertis.org/T3729](T3729)	ribchester: gnome-desktop-testing test times out
 - [https://phabricator.apertis.org/T3730](T3730)	canterbury: Most of the tests fail
 - [https://phabricator.apertis.org/T3763](T3763)	Compositor hides the other screens
 - [https://phabricator.apertis.org/T3771](T3771)	Roller problem in settings application 
 - [https://phabricator.apertis.org/T3797](T3797)	Variable roller is not working
 - [https://phabricator.apertis.org/T3798](T3798)	In mildenhall, URL history speller implementation is incomplete.
 - [https://phabricator.apertis.org/T3909](T3909)	MildenhallSelectionPopupItem doesn't take ownership when set properties
 - [https://phabricator.apertis.org/T3939](T3939)	libshoreham packaging bugs
 - [https://phabricator.apertis.org/T3940](T3940)	libmildenhall-0-0 contains files that would conflict with a future libmildenhall-0-1
 - [https://phabricator.apertis.org/T3969](T3969)	MildenhallSelPopupItem model should be changed to accept only gchar * instead of MildenhallSelPopupItemIconDetail for icons
 - [https://phabricator.apertis.org/T3971](T3971)	libbredon/seed uninstallable on target as they depend on libraries in :development
 - [https://phabricator.apertis.org/T3972](T3972)	webview-test should be shipped in libbredon-0-tests instead of libbredon-0-1
 - [https://phabricator.apertis.org/T3973](T3973)	bredon-0-launcher should be shipped in its own package, not in libbredon-0-1
 - [https://phabricator.apertis.org/T3991](T3991)	virtual keyboard is not showing for password input field of any webpage
 - [https://phabricator.apertis.org/T3992](T3992)	Steps like pattern is seen in the background in songs application
 - [https://phabricator.apertis.org/T3996](T3996)	Avoid unconstrained dbus AppArmor rules in frome
 - [https://phabricator.apertis.org/T4005](T4005)	Newport test fails on minimal images
 - [https://phabricator.apertis.org/T4009](T4009)	connman: patch "Use ProtectSystem=true" rejected upstream
 - [https://phabricator.apertis.org/T4010](T4010)	connman: patch "device: Don't report EALREADY" not accepted upstream
 - [https://phabricator.apertis.org/T4027](T4027)	webkit2GTK crash observed flicking on webview from other widget
 - [https://phabricator.apertis.org/T4031](T4031)	Mildenhall should install themes in the standard xdg data dirs
 - [https://phabricator.apertis.org/T4046](T4046)	Page rendering is not smooth in sites like www.yahoo.com
 - [https://phabricator.apertis.org/T4048](T4048)	HTML5 demo video's appear flipped when played on webkit2 based browser app
 - [https://phabricator.apertis.org/T4050](T4050)	Render theme buttons are not updating with respect to different zoom levels
 - [https://phabricator.apertis.org/T4052](T4052)	Rendering issue observed on websites like http://www.moneycontrol.com
 - [https://phabricator.apertis.org/T4089](T4089)	Crash observed on webruntime framework
 - [https://phabricator.apertis.org/T4110](T4110)	Crash observed on seed module when we accesing the D-Bus call method
 - [https://phabricator.apertis.org/T4142](T4142)	introspectable support for GObject property, signal and methods
 - [https://phabricator.apertis.org/T4244](T4244)	zoom feature is not working as expected 
 - [https://phabricator.apertis.org/T4348](T4348)	Inital Roller mappings are misaligned on the HMI 
 - [https://phabricator.apertis.org/T4383](T4383)	folks-metacontacts-antilinking: folks-metacontacts-antilinking_sh.service failed
 - [https://phabricator.apertis.org/T4386](T4386)	apparmor-tracker: AssertionError: False is not true
 - [https://phabricator.apertis.org/T4392](T4392)	apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed 
 - [https://phabricator.apertis.org/T4395](T4395)	tracker-indexing-local-storage: AssertionError: False is not true
 - [https://phabricator.apertis.org/T4419](T4419)	traprain: sadt: error: cannot find debian/tests/control
 - [https://phabricator.apertis.org/T4420](T4420)	canterbury: core-as-root and full-as-root tests failed
 - [https://phabricator.apertis.org/T4421](T4421)	ribchester: Job for generated-test-case-ribchester.service canceled
 - [https://phabricator.apertis.org/T5301](T5301)	libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test failed
 - [https://phabricator.apertis.org/T5754](T5754)	cgroups-resource-control: 3_cgroups-resource-control test failed
 - [https://phabricator.apertis.org/T5989](T5989)	frome: 6_frome test failed
 - [https://phabricator.apertis.org/T5990](T5990)	frome: 5_frome test failed

