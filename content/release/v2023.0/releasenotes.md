+++
date = "2023-03-09"
weight = 100

title = "v2023.0 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023.0** is the first **stable** release of the Apertis
v2023 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
 Apertis is committed to maintaining the v2023 release stream up to the end
of 2024.

This Apertis release is built on top of Debian Bullseye with several
customizations and the Linux kernel 6.1.x LTS series.

{{% notice warning %}}
After releasing v2023.0 some build errors were reported, for this reason the
Apertis team decided to publish a minor release update v2023.0-1 on top v2023.0.
This minor update contains low impact changes to fix the build issues.
{{% /notice %}}

Test results for the v2023.0 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023/20230222.0117/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023/20230222.0117/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023/20230222.0117/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023/20230222.0117/lxc)

Test results for the v2023.0-1 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023/20230322.1745/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023/20230322.1745/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023/20230322.1745/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023/20230322.1745/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - **2023 Q1: v2023.0**
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023.0 images](https://images.apertis.org/release/v2023/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0/amd64/fixedfunction/apertis_ostree_v2023-fixedfunction-amd64-uefi_v2023.0.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0/amd64/hmi/apertis_ostree_v2023-hmi-amd64-uefi_v2023.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023/v2023.0/amd64/basesdk/apertis_v2023-basesdk-amd64-sdk_v2023.0.ova) | [SDK](https://images.apertis.org/release/v2023/v2023.0/amd64/sdk/apertis_v2023-sdk-amd64-sdk_v2023.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0/armhf/fixedfunction/apertis_ostree_v2023-fixedfunction-armhf-uboot_v2023.0.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0/armhf/hmi/apertis_ostree_v2023-hmi-armhf-uboot_v2023.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-rpi64_v2023.0.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0/arm64/hmi/apertis_ostree_v2023-hmi-arm64-rpi64_v2023.0.img.gz)

| [Apertis v2023.0-1 images](https://images.apertis.org/release/v2023/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0-1/amd64/fixedfunction/apertis_ostree_v2023-fixedfunction-amd64-uefi_v2023.0-1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0-1/amd64/hmi/apertis_ostree_v2023-hmi-amd64-uefi_v2023.0-1.img.gz) | [base SDK](https://images.apertis.org/release/v2023/v2023.0-1/amd64/basesdk/apertis_v2023-basesdk-amd64-sdk_v2023.0-1.ova) | [SDK](https://images.apertis.org/release/v2023/v2023.0-1/amd64/sdk/apertis_v2023-sdk-amd64-sdk_v2023.0-1.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0-1/armhf/fixedfunction/apertis_ostree_v2023-fixedfunction-armhf-uboot_v2023.0-1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0-1/armhf/hmi/apertis_ostree_v2023-hmi-armhf-uboot_v2023.0-1.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0-1/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.0-1.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.0-1/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-rpi64_v2023.0-1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.0-1/arm64/hmi/apertis_ostree_v2023-hmi-arm64-rpi64_v2023.0-1.img.gz)



The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023 package list

The full list of packages available from the v2023 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2023](https://infrastructure.pages.apertis.org/dashboard/tsv/v2023.tsv)

#### Apertis v2023 repositories

    deb https://repositories.apertis.org/apertis/ v2023 target development sdk

## New features

### Completion of the GPL-3-free modernization

This release completes the modernization of the approach to
[licensing expectations compliance]({{< ref "license-expectations.md" >}})
started with the [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}) release.

The [v2022.0]({{< ref "release/v2022.0/releasenotes.md" >}}) release already addressed the licensing challenges in the
[TLS stack]({{< ref "tls-stack.md" >}}), [Coreutils]({{< ref "coreutils-replacement.md" >}}), and
[GnuPG]({{< ref "gnupg-replacement.md" >}}) and this release addresses all the smaller challenges identified in the
[GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}), in particular the move to `rust-findutils`, `libedit`
and GNU tools such as `cpio`, `grep`, `gzip`, `sed` and `tar` to cover the needs of the target images.

### Improved automated license compliance

With the goal of providing a more reliable copyright information Apertis has improved its automated license
compliance. Now for packages which require a not straight forward configuration for license whitelist and
override, a full copyright report can be enabled, providing a finer configuration.

### Support automated license compliance for Rust packages

The process described in [Automated License Compliance]({{< ref automated-license-compliance.md >}}) has been improved
to support Rust packages to provide a more reliable license report. Since Rust packages are statically linked, the
process now captures the license of all the build dependencies to generate the package metadata.

### New reference AMD 64 hardware selected

As described in [AMD 64 reference boards]({{< ref amd64.md >}}) Apertis has chosen **UP Squared 6000** as the
new reference board to build products. This decision was done after a long research to compare different alternatives since
**MinnowBoard** boards are no longer available. Work to fully support the new board in Apertis is currently in progress.

### Support for lintian validation to packaging CI

In an effort to allow developers to check that their changes follow Debian's best practices, Apertis added
a lintian step in its CI with a custom profile. This results in a better developer experience, specially for
those new in Apertis.

## Build and integration

### Build-test packages on OBS before merging

A new feature of the CI for packaging is the inclusion of OBS build test on branches
to be merged. Thanks to this addition, now developers can confirm that their changes
successfully build in OBS before being merged in the main branches.

### Improve developer experience while importing packages

The infrastructure around Apertis has been improved in order to make the importing of new
packages straightforward, by avoiding as much as possible any manual steps. With these
changes the effort invested to import new packages has been reduced drastically and the possibility
of errors in the process has been notably reduced.

### Aptly publisher support

Based on the concept document [Cloud-friendly APT repository publishing]({{< ref "apt-publisher.md" >}})
the Apertis project now supports **aptly** as publisher for its repositories which unlocks key features such as:
- Improved publishing workflow, specially during release periods
- Support for snapshots natively
- Support for cloud storage is also in progress and should be made available soon

Newer Apertis development repositories will be published using **aptly**.

### Dashboard improvements

[Dashboard](https://infrastructure.pages.apertis.org/dashboard/)
is the tool Apertis uses to have a single view of packages' health for the different supported releases.
Newer features has been added to make it more developer friendly such as:
- Support for different domains of issues
- Support for different priorities of issues
- Support for licensing issues

### Automated note importing in QA Report App

The QA report app is the tool used to collect and summarize the test results for all the Apertis images.
To improve the user experience for testers running manual tests and to reduce
the amount of information introduced, now the results notes are
automatically propagated
from previous submissions, giving the chance to the user to amend them to match
the current results.

### Improved QA Report App configuration management and deployment

A set of changes were introduced to make configuring the QA Report App more straightforward and consistent, also including an improved validation of configuration settings.
These improvements align with the use of **ansible** and **kubernetes** as a way to manage deployments and orchestration,
improving automation and reducing manual work. These changes will also benefit downstream distributions, which will find it easier to deploy new instances.

### QA Report App and OIDC authentication mechanism

As a way to provide more flexibility around authentication mechanisms, the QA Report App now uses
[OpenID Connect](https://openid.net/connect/) as the authentication mechanism, replacing the previous
GitLab OAuth mechanism.

Although the Apertis deployment still uses GitLab as the OIDC provider, this change will allow deployments to use
any OIDC provider that best fit their needs.

Relatedly, the QA Report App now also provides the support to always require login, allowing deployments to control
whether login is required to access the reports.

### Improved QA Report App support for weekly images

Weekly Apertis images are tested with both automated and manual test cases, as part of the QA process.
The results from these tests are submitted to the QA Report App, and now can be easily seen in the main page
which provides a separate section for each type of image: releases, weeklies and dailies.


### QA Report App with Gitlab issues support

As planned in [Moving to Gitlab issues]({{< ref "gitlab-issues.md" >}}) QA Report App has been improved to add
support for different bug tracking systems, including Phabricator and Gitlab issues. Thanks to this change
Apertis has moved to [Gitlab as bugtracker](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues) in order to be able to collect feedback from the community and being
more open.

### New IoT image support

A new reference image for IoT products is now supported based on
[Eclipse Kanto](https://projects.eclipse.org/projects/iot.kanto) which provides a modular software stack.
Thanks to this image now product teams have a shortcut to develop their IoT products based on Apertis.

### LAVA support for the UP Squared 6000 amd64 reference board

In relation to the support of the new amd64 reference board, the LAVA infrastructure now includes UP Squared 6000
boards. Thanks to this addition, now daily automated tests run in the new reference board, providing an improved
test coverage.

### Improved submission and monitoring of LAVA jobs

The CI in Apertis has been improved to have better control of the jobs submitted to LAVA. With the latest changes
the integration in more tight and simple. Thanks to this change now it is possible to get a finer grained information
about test failures in the Gitlab pipelines. Additionally it allows the use of short lived credential to access
private repositories from LAVA.

### Reusable CI pipeline to create Flatpak runtime and applications

To provide a better developer experience, Apertis now provides a set of reusable CI pipelines for Flatpak,
which makes it easier to create both runtimes and applications. Following the steps in
[Flatpak CI]({{< ref "flatpak.md#building-flatpaks-on-gitlab-ci" >}})
developers can take advantage of this feature.

### Enhanced QA site

With the goal of providing more coherent information the QA test cases and results are now provided by the
[QA Report App](https://qa.apertis.org), which was integrated with the previous [lavaphabbridge](https://lavaphabbridge.apertis.org). This results in a better user experience and reduces
the costs associated with maintaining multiple different applications/repositories to view test cases and track reports.

### Improved QA reports

Following the latest changes in relation with [QA report](https://qa.apertis.org) the new site provides better
information to differentiate automated from manual tests. Additionally, a more flexible configuration allows
the creation of separate issues per release when a test fails on LAVA.

Additionally the new version provides a section to show weekly images to highlight manual tests results.

### Improved QA Report App configuration management and deployment

A set of changes were introduced to make configuring the QA Report App more straightforward and consistent, also including an improved validation of configuration settings.
These improvements align with the use of **ansible** and **kubernetes** as a way to manage deployments and orchestration,
improving automation and reducing manual work. These changes will also benefit downstream distributions, which will find it easier to deploy new instances.

### QA Report App and OIDC authentication mechanism

As a way to provide more flexibility around authentication mechanisms, the QA Report App now uses
[OpenID Connect](https://openid.net/connect/) as the authentication mechanism, replacing the previous
GitLab OAuth mechanism.

Although the Apertis deployment still uses GitLab as the OIDC provider, this change will allow deployments to use
any OIDC provider that best fit their needs.

Relatedly, the QA Report App now also provides the support to always require login, allowing deployments to control
whether login is required to access the reports.

## Documentation and designs

### Thin proxies - REST APIs

This release includes a concept document [Thin proxies: REST APIS]({{ < ref "thin-proxies.md" >}})
with guidelines for developers to access
system APIs through high level REST APIs. Using this approach the complexity of
system APIs can be hidden and developers can chose the language/technology that better
fit their needs.

### Documentation refresh to match the switch to Flatpak

Continuing with the effort of updating documentation after switching to Flatpak, this
release also includes a reviewed version of several documents related to the Application
Framework to provide developers with the best practices to build their applications.

### Apertis test strategy

This release includes the new concept document [Apertis test strategy]({{< ref "test-strategy.md" >}})
advertising the test strategy Apertis applies to provide a high quality distribution and explaining
the different testing loops in the development and release workflow.

### New recommended tooling for SDK API documentation

Following best practices on SDK API documentation Apertis chose **gtk-doc** as the preferred tool. Thanks
to the information found in [Documenting Apertis interfaces]({{< ref documenting-interfaces.md >}}) developers
can easily integrate SDK API documentation to their packages which can later be accessed using **devhelp**.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No know breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #155](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/155) aum-offline-upgrade-branch: test failed
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #169](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/169) aum-out-of-space: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up FS corruption issue - part 2
- [Issue #218](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/218) agl-compositor not showing on UP SQUARED 6000 board setup of lilliput display
- [Issue #228](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/228) sdk-debos-image-building: test failed
- [Issue #270](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/270) Issue in playing the Video on HMI

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #217](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/217) AUM out of space tests fail on UP Squared 6000 board
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #247](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/247) tiny-container-user-basic: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #256](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/256) OBS: Backend doesn’t start immediately upon restart
- [Issue #257](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/257) sdk-performance-tools-gprof: test failed
- [Issue #260](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/260) tiny-container-user-folder-sharing: test failed
- [Issue #262](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/262) tiny-container-system-aa-enforcement: test failed
- [Issue #272](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/272) QA Report App does not handle task-per-release when using Phabricator
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #281](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/281) tiny-container-user-aa-enforcement: test failed

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #40](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/40) evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #93](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/93) "Error: GDBus.Error:org.freedesktop.DBus.Error.InvalidArgs" logs seen during bluez-phone test
