+++
date = "2019-06-25"
weight = 100

title = "V2019pre ReleaseNotes"

aliases = [
    "/old-wiki/V2019pre/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis v2019pre Release

Apertis a Debian derivative distribution geared towards the creation of
product-specific images for ARM (both the 32bit ARMv7 and 64-bit ARMv8
versions using the hardfloat ABI) and Intel x86-64 (64-bit) systems.

**Apertis v2019pre** is the first **development** release of the Apertis
v2019 release flow that will lead to the LTS **Apertis v2019.0** release
in September 2019.

This Apertis release is the first one to be based on top Debian Buster
with several customizations. Test results for the v2019pre release are
available in the [v2019pre test report]().

## Release flow

  - 2019 Q1: v2019dev0
  - 2019 Q2: **v2019pre**
  - 2019 Q3: v2019.0 + v2020dev0
  - 2019 Q4: v2019.1 + v2020pre
  - 2020 Q1: v2019.2 + v2020.0

### Release downloads

| [Apertis v2019pre images]() |
| ----------------------------------------------------------------------- |
| Intel 64-bit                                                            |
| ARM 32-bit (U-Boot)                                                     |
| ARM 64-bit (U-Boot)                                                     |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2019pre repositories

` $ deb `<https://repositories.apertis.org/apertis/>` v2019pre target helper-libs development sdk hmi`

## New features

### Automatic web-based online OTA updates using OSTree

In addition to the [automatic offline updates over USB Mass Storage
devices]( {{< ref "/release/v2019dev0/releasenotes.md#automatic-offline-updates-over-usb-mass-storage-devices-with-rollback" >}} )
the Apertis update agent now support polling for updates over the
network and retrieving the updated contents over HTTPS using OSTree.

### Persistent storage on the SDK

The Apertis SDK now provides a new mechanism to let developers preserve
their working environment across multiple SDK versions.

This enables developer to share their workspace among different SDK
images without manually copying and keeping it in sync across SDK
images. It also lets developers to preserve bits of the system
configuration so they don't have to go through the same manual setup
steps when moving from a SDK release to the next one.

Developers can use invoke the setup GUI from the *Applications* →
*System* → *Persistent Disk Setup* menu entry, launching the `psdk` tool
in interactive mode.

See the [psdk
documentation](https://gitlab.apertis.org/apertis/psdk/blob/master/README.md)
for a full walkthrough on how to use the feature.

### NFS boot

The artifacts needed to boot Apertis using TFTP/NFS are now part of the
official release artifacts, see for instance the [ones for the armhf
ARM 32bit
platform]().

Booting over TFTP/NFS is an invaluable development help while working on
lower level components and in particular when working on the kernel,
providing a much faster iteration cycle compared to deploying to a
bootable SD card.

### Portable toolchain

The Apertis toolchain is now also available as a pre-built downloadable
tarball ready to be unpacked on systems running third-party
distributions such as Ubuntu 16.04 Xenial.

Combined with the sysroots it allows developers to reliably and
efficiently cross-build binaries for Apertis without having to boot the
Apertis SDK.

### Raspberry Pi CM3 support in the kernel

The Apertis 4.19 kernel now ships the device tree description for the
Raspberry Pi Compute Module 3 as well. While the Raspberry Pi is not an
officially supported platform yet, this is a small step to enable
developers to run Apertis on the Raspberry Pi devices.

### Devroot improvements

In addition to the devroots targeting `amd64` (Intel x86-64) and `armhf`
(ARM 32bit hard-float) platforms, the v2019pre release includes a
devroot targeting `arm64` platform (ARM aarch64 64bit). Platforms
developer have now a reliable way to build projects and packages
targeting ARM 64bit systems from their Intel based workstations running
the Apertis SDK.

The devroots have also been made sensibly leaner, greatly reducing the
default set of packages shipped. Developers can use the `apt` package
manager to install only the dependencies they need without any unneeded
bloat.

## Build and integration

### GitLab-based packaging workflow

A new workflow has been set up to make contributing to Apertis much
easier.

Now any package on Apertis can be updated by submitting [merge
requests](https://docs.gitlab.com/ce/user/project/merge_requests/) to
its corresponding project on GitLab. The [Gitlab-based packaging
workflow]( {{< ref "component_guide.md" >}} )
guide walks developers through the steps needed, sensibly lowering the
learning curve for developers new to the Debian-based packaging tools.

Developers now have a coherent and reliable workflow that applies to all
the packages in the archive using the same [contribution
process]( {{< ref "contributions.md" >}} ) used
for all the other Apertis projects, while maintainers have finer grained
control on the downstream changes that get applied.

Currently the workflow is active for all the packages in the `:target`
repository, but support will be extended to all packages soon after this
release.

## Quality Assurance

### System update test automation

System updates are a critical component for devices deployed on field:
they need to be robust and reliable, as no operator may have the chance
to intervene in case of errors.

To better catch regressions as they are introduced in the system during
development a set of previously manual tests have been automated and are
now run daily on the devices in the Apertis LAVA laboratory.

### Improved manual test results submission and reporting

As a work in progress for this release, the [QA report
application](http://qa.apertis.org/) is being extended to
also handle the submission of results from the manual testing rounds and
to integrate such results in the automatically generated reports.

Moving away from the currently wiki-based reports to a dedicated
interface will simplify the work of testers and the structured database
will give more insight about the health of the project in the future by
providing the foundation for new visuals, charting and tracking of
historical trends.

## Design and documentation

### Guide to build images on the SDK

The Apertis SDK now ships all that is needed for developers to build the
reference Apertis image recipes and the sample image recipes.

The [recipe repository now provides a
tutorial](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/blob/apertis/v2019pre/README.md)
that guides platform developers through the process of building the
sample image recipes.

The sample image recipes provide a good starting point for developers
interested on building images based on the reference Apertis recipes, so
the tutorial provide the information needed to build a development
image, an image for production adn a dedicated SDK. The sample images
themselves are heavily commented and show the extension points that
developers can use to customize their images.

### OTA improvements in the System update and rollback design document

The [System update and rollback design
document]( {{< ref "system-updates-and-rollback.md" >}} )
has been update to cover online Over-the-Air (OTA) updates with more
details, describing how online OSTree updates are provided, what kind of
verification is done, how the process is triggered, what kind of hosting
infrastructure is needed and how it could be integrated with rollout
management suites.

### Devroot vs. sysroot

The developer portal now contains a [guide about devroots and
sysroots]( {{< ref "sysroots-and-devroots.md" >}} ),
describing what they are, what are their purposes, how they differ and
how/when they should be used. This has been a source of confusion in the
past and the new document should shed some light on their use-cases and
their different tradeoffs.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester` and `canterbury` are deprecated

<dd>

From the next release Canterbury and Ribchester are deprecated as is the
[Application
Layout]( {{< ref "application-layout.md" >}} )
document in favour of Flatpak.

</dl>

### Breaks

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-testcases-builder` and
`apertis-documentation-builder` Docker images.

### Apertis infrastructure tools

The [Apertis v2019 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2019)
provides packages for `git-phab`, `lqa`, Debos and deps, LAVA v2, OBS
2.7, `ostree-push` and `jenkins-job-builder` for Debian Buster:

` deb `<https://repositories.apertis.org/infrastructure-v2019/>` buster infrastructure`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (18)

  - ribchester: ribchester test failed

  - ribchester: core-as-root test failed

  - Package build fails for apertis:v2019dev0 on OBS

  - canterbury: 3_canterbury test failed

  - Terminal comes up inside the Launcher

  - boot-no-crashes: 3_boot-no-crashes test failed

  - Songs/Videos don't play on i.MX6 with Frampton on internal images

  - A2DP test is failing as part of the bluez-phone test

  - Video does not stream in WebKit on the i.MX6 internal images

  - ping command needs to be run with sudo permission

  - RBEI FTBFS: rustc

  - canterbury: 2_canterbury test failed

  - ribchester: 9_ribchester test failed

  - ribchester: 8_ribchester test failed

  - RBEI FTBFS: apitrace

  - Applications do not re-launch

  - Multimedia playback is broken on the internal i.MX6 images (internal
    3.14 kernel issue)

  - youtube Videos are not playing on upstream webkit2GTK

### Normal (156)

  - sanity-check: verify that a trivial PyGI and/or gjs GUI can run

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Crash when initialising egl on ARM target

  - Develop test case for out of screen events in Wayland images

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-tracker: underlying_tests failed

  - Fix Tracker testcase to not download media files from random HTTP
    user folders

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Ensure that the arch:all packages in the archive match the
    arch-specific versions

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - gupnp-services: browsing and introspection tests fail

  - dbus-installed-tests: service failed because a timeout was exceeded

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - SDK hangs when trying to execute bluez-hfp testcase

  - OSTree testsuite hangs the gouda Jenkins build slave

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on the internal
    mx6qsabrelite images with proprietary kernel

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - Apps like songs, artists etc are not offser correctly by the
    compositor window

  - Double tapping on any empty space in the songs app's screen results
    in the compositor window hinding the left ribbon

  - Back button is hidden by the compositor window in apps like songs,
    artists etc

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - Mildenhall compositor gets offset when we open an app like songs

  - cgroups-resource-control: cpu-shares test failed

  - tracker-indexing-local-storage: run-test test failed

  - apparmor-pulseaudio: pulseaudio.malicious.expected test failed

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    test failed

  - apparmor-tracker: run-test-tracker test failed

  - apparmor-tracker: tracker.malicious.expected test failed

  - apparmor-tracker: tracker.malicious.expected_underlying_tests test
    failed

  - apparmor-tracker: tracker.normal.expected test failed

  - apparmor-tracker: tracker.normal.expected_underlying_tests test
    failed

  - kernel panic occurs when trying to boot an image on i.mx6 revision
    1.2 board from LAVA

  - gupnp-services: test_service_introspection test failed

  - gupnp-services: test_service_browsing test failed

  - build failure in liblightwood-2 on 18.06 image

  - newport: newport test failed

  - lava: 12_apparmor-functional-demo test failed

  - apparmor-pulseaudio:
    pulseaudio.malicious.expected_underlying_tests test failed

  - didcot: didcot test failed

  - traprain: traprain test failed

  - "Setup Install to Target" option is not showing on eclipse

  - Ospacks ship a .gitignore file in the root directory

  - Ospacks ship an empty /script directory in the root directory

  - rhosydd: rhosydd test failed

  - rhosydd: integration test failed

  - LAVA-Phab bridge should mark test failures as bugs

  - build-snapshot: allow to build packages without \`autogen.sh\`
    script

  - Wi-Fi search button is missing in wifi application

  - LAVA-Phab bridge does not report the release version or the board
    used of the tested image

  - traprain: gnome-desktop-testing test failed

  - apparmor-geoclue: run-test-geoclue test failed

  - LAVA-Phab bridge should post a comment once tests stop failing

  - bluez-hfp testcase fails

  - ade tests failing on lava

  - apparmor-tumbler: run-test-sh test failed

  - \`kbd\` package has broken/problematic dependencies

  - lava: read-feedback test failed

  - lava: read-feedback test failed

  - Sample applications should not be part of the base SDK

  - Seed reference manual should not be part of devhelp

  - Media files should not be part of Base SDK

  - iptables-basic: test_iptables_list test failed

  - Debos crash when a recipes embbed recipes with ostree-commit and
    ostree-deploy

  - boot-no-crashes automated testcase needs to be included in the
    weekly test report

  - Both sysroot and devroot folder is exist in /opt path

  - The /boot mountpoint is not empty

  - System users are shipped in /usr/etc/passwd instead of /lib/passwd

  - apparmor-gstreamer1-0: run-test-sh test failed

  - apparmor-pulseaudio: run-test-pulseaudio test failed

  - Fix the RFS tiny images filesystem structure

  - apparmor-folks: 5_apparmor-folks test failed

  - apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed

  - apparmor-pulseaudio: 7_apparmor-pulseaudio test failed

  - apparmor-tracker: 10_apparmor-tracker test failed

  - cgroups-resource-control: 8_cgroups-resource-control test failed

  - folks-alias-persistence: 4_folks-alias-persistence test failed

  - Chaiwala logo does not come up on bootup

  - xmessage error pop up comes up when Guestadditions is enabled on the
    sdk's

  - No test-text is displayed inside the Xephyr window.

  - aa_get_complaints.sh script needs to be run with sudo permission

  - bluez-setup test fails

  - bluez-phone test fails on the basesdk

  - apparmor-functional-demo: 13_apparmor-functional-demo test failed

  - apparmor-geoclue: 11_apparmor-geoclue test failed

  - apparmor-pulseaudio: 8_apparmor-pulseaudio test failed

  - apparmor-tracker: 12_apparmor-tracker test failed

  - apparmor-tumbler: 9_apparmor-tumbler test failed

  - cgroups-resource-control: 6_cgroups-resource-control test failed

  - gettext-i18n: gettext-i18n test failed

  - apparmor-functional-demo: 12_apparmor-functional-demo test failed

  - apparmor-geoclue: 10_apparmor-geoclue test failed

  - apparmor-tracker: 11_apparmor-tracker test failed

  - apparmor-tumbler: 8_apparmor-tumbler test failed

  - apparmor-functional-demo: apparmor-functional-demo test failed

  - apparmor-geoclue: apparmor-geoclue test failed

  - apparmor-tracker: apparmor-tracker test failed

  - apparmor-utils: apparmor-utils test failed

  - gettext-i18n: 11_gettext-i18n test failed

  - tracker-indexing-local-storage: 14_tracker-indexing-local-storage
    test failed

  - GuestAdditions fails to launch full screen in the first attempt

  - cgroups-resource-control: 5_cgroups-resource-control test failed

  - gettext-i18n: 10_gettext-i18n test failed

  - tracker-indexing-local-storage: 13_tracker-indexing-local-storage
    test failed

  - tracker-indexing-local-storage: 17_tracker-indexing-local-storage
    test failed

  - dbus-installed-tests: trying to overwrite mktemp.1.gz

  - telepathy-ring-automated: Unable to locate package telepathy-phoenix

  - sdk-dbus-tools-bustle testcase is failing

  - apparmor-ofono test fails

  - apparmor-bluez-avrcp-volume test fails

  - evolution-sync-bluetooth test fails

  - eclipse-plugins-apertis-management package is missing

  - No audio/sound is heard while making a call in the bluez-hfp
    testcase

  - ofono-tests package is missing

  - connman-pan-tethering test fail

  - connman-pan-network-access test fails

  - connman-usb-tethering test fails

  - ifconfig command need to be run with sudo permission

  - libfolks-ofono25 package not found

  - newport: 7_newport test failed

  - newport: 6_newport test failed

  - rhosydd: 8_rhosydd test failed

  - rhosydd: 7_rhosydd test failed

  - traprain: 10_traprain test failed

  - traprain: 9_traprain test failed

  - eclipse-plugins-remote-debugging test fails

  - Thumb nails of songs are not seen on the SDK

  - The pacrunner package used for proxy autoconfiguration is not
    available

  - Auto-resizing of the SDK display in VirtualBox is known to be broken

  - Booting the SDK in VirtualBox appears to be broken with the 6.0.4
    VirtualBox Guest Extensions

  - webkit2gtk-event-handling-redesign test fails on the amd64 ostree
    images

  - simple-agent not found

  - kernel crash during the usb detaching

  - bluez-avrcp-volume test fails on the sdk and base sdk

  - bluez-hfp testcase fails on sdk

  - folks-inspect: command not found

  - gupnp-services: 13_gupnp-services test failed

  - apparmor-functional-demo: 13_apparmor-functional-demo test failed

  - apparmor-geoclue: 11_apparmor-geoclue test failed

  - Page scroll is lagging in Minibrowser on upstream webkit2GTK

  - Disable timesyncd on images and set fallback time servers for
    ConnMan

### Low (136)

  - Remove unnecessary folks package dependencies for automated tests

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - telepathy-ring: Review and fix SMS test

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - apparmor-folks: unable to link contacts to test unlinking

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - folks: random tests fail

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - connman: patch "Use ProtectSystem=true" rejected upstream

  - connman: patch "device: Don't report EALREADY""not accepted upstream

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-folks: Several tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - didcot: test_open_uri: assertion failed

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - webkit2gtk-drag-and-drop doesn't work with touch

  - folks-alias-persistence: folks-alias-persistence test failed

  - folks-extended-info: folks-extended-info test failed

  - folks-metacontacts: folks-metacontacts-unlinking test failed

  - folks-metacontacts: folks-metacontacts-linking test failed

  - folks-metacontacts-antilinking: folks-metacontacts-antilinking test
    failed

  - folks-search: folks-search-contacts test failed

  - apparmor-folks: run-test-folks test failed

  - apparmor-folks: R1.13b_folks-metacontacts-unlinking test failed

  - apparmor-folks: R1.13b_folks-metacontacts-linking test failed

  - apparmor-folks: R1.13b_folks-alias-persistence test failed

  - apparmor-folks: R6.4.2_folks-metacontacts-antilinking test failed

  - frome: frome test failed

  - frome: gnome-desktop-testing test failed

  - libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test
    failed

  - folks-telepathy: folks-retrieve-contacts-telepathy test failed

  - apparmor-folks: R1.13b_folks-retrieve-contacts-telepathy test
    failed

  - folks-metacontacts-unlinking: folks-metacontacts-unlinking test
    failed

  - folks-search-contacts: folks-search-contacts test failed

  - folks-metacontacts-linking: folks-metacontacts-linking test failed

  - folks: Unable to locate package telepathy-gabble

  - traffic-control-basic test fails

  - frome: 6_frome test failed

  - frome: 5_frome test failed

  - Apertis FTBFS tracker

  - frome: 5_frome test failed

  - frome: 6_frome test failed

  - apt-get dist-upgrade fails on SDK

  - frome: 6_frome test failed

  - frome: 5_frome test failed

### Lowest (2)

  - cgroups-resource-control: 3_cgroups-resource-control test failed

  - tracker-indexing-local-storage: 18_tracker-indexing-local-storage
    test failed
