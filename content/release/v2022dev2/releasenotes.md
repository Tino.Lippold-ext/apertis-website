+++
date = "2021-06-28"
weight = 100

title = "v2022dev2 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022dev2** is the third **development** release of the Apertis
v2022 stable release flow that will lead to the LTS **Apertis v2022.0**
release in March 2022.

This Apertis release is the first one on top of Debian Bullseye. It currently
ships with the Linux kernel 5.10.x LTS series but later releases in the the
v2022 channel will track newer kernel versions up to the next LTS scheduled at
the end of 2021.

Test results for the v2022dev2 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022dev2/20210621.2349)
  - [OSTree images](https://qa.apertis.org/report/v2022dev2/20210621.2349/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022dev2/20210621.2349/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022dev2/20210621.2349/lxc)

Due to the large effort required for rebasing to Bullseye, the QA requirements
for this release have been limited to basic features, focusing on ensuring full
functionality in the next quarter.

The goals for this release are:

   1. minimal image boot and provide a textual prompt
   2. target image show the Maynard shell at boot
   3. sdk images provide an XFCE user session

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - **2021 Q2: v2022dev2**
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022dev2 images](https://images.apertis.org/release/v2022dev2/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2022dev2/v2022dev2.0/amd64/minimal/apertis_ostree_v2022dev2-minimal-amd64-uefi_v2022dev2.0.img.gz) | [target](https://images.apertis.org/release/v2022dev2/v2022dev2.0/amd64/target/apertis_ostree_v2022dev2-target-amd64-uefi_v2022dev2.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022dev2/v2022dev2.0/amd64/basesdk/apertis_v2022dev2-basesdk-amd64-sdk_v2022dev2.0.ova) | [SDK](https://images.apertis.org/release/v2022dev2/v2022dev2.0/amd64/sdk/apertis_v2022dev2-sdk-amd64-sdk_v2022dev2.0.ova)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev2/v2022dev2.0/armhf/minimal/apertis_ostree_v2022dev2-minimal-armhf-uboot_v2022dev2.0.img.gz) | [target](https://images.apertis.org/release/v2022dev2/v2022dev2.0/armhf/target/apertis_ostree_v2022dev2-target-armhf-uboot_v2022dev2.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev2/v2022dev2.0/arm64/minimal/apertis_ostree_v2022dev2-minimal-arm64-uboot_v2022dev2.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2022dev2/v2022dev2.0/arm64/minimal/apertis_ostree_v2022dev2-minimal-arm64-rpi64_v2022dev2.0.img.gz) | [target](https://images.apertis.org/release/v2022dev2/v2022dev2.0/arm64/target/apertis_ostree_v2022dev2-target-arm64-rpi64_v2022dev2.0.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022dev2 repositories

    deb https://repositories.apertis.org/apertis/ v2022dev2 target development sdk hmi

## New features

### Rebase to Debian Bullseye

This release is the first one based on Debian Bullseye, bringing plenty of
upstream updates and features compared to the Buster baseline.

### WPA3 support in ConnMan

Apertis now ships support for WPA3/SAE in ConnMan. By basing the work on top of the
[patches from](https://review.tizen.org/git/?p=platform/upstream/connman.git;a=commitdiff;h=a48fa9fdffe415e9a6f703776b5db795e242ac23)
[the Tizen project](https://review.tizen.org/git/?p=platform/upstream/connman.git;a=commit;h=d04bfa0350781ebfb8cbb2e64fabdfb2f36cd302)
the Apertis team landed the
[implementation of WPA3-SAE authentication `wpa_supplicant`](https://git.kernel.org/pub/scm/network/connman/connman.git/commit/?id=a08873088de149e6aea771d46946e96fd35ab319)
to upstream ConnMan and the made it available in the Apertis `connman` package.

### Switch to PipeWire+WirePlumber by default

From this release the [Pipewire](https://pipewire.org/) multimedia manager and
the [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/) policy
manager replace PulseAudio as the default audio manager shipped on the
reference images, as described in the
[audio management]({{< ref "audio-management.md" >}}) concept document.

PipeWire grants more efficiency and flexibility compared to PulseAudio, with
better integration in container-based application frameworks like Flatpak,
while WirePlumber enable developers to define complex policies with ease.

Thanks to `pipewire-pulse` applications using the PulseAudio protocol will
automatically and transparently be able to interact with PipeWire with no
changes on the client side.

### Updated licensing compliant TLS stack

To match the [licensing expectations]({{< ref "license-expectations.md" >}})
previous releases shipped an old version of the GnuTLS stack, before part of it
switched to a LGPL-3-or-GPL-2 dual licensing scheme.

Following what was planned in the
[licensing document about the TLS stack]( {{< ref "tls-stack.md" >}} ) this
release ships with updated TLS stacks:

* OpenSSL is used when available, and linking to it from GPL programs is
  considered covered by the system libraries clause;
* GnuTLS is only linked from programs that can be considered GPL-2 as a whole,
  to avoid the LGPL-3 provisions in the stack.

### OVA VirtualBox appliance

To make the initial set up easier for users of the Apertis SDK with VirtualBox,
a full OVA appliance file is provided. Developers can now create their Apertis
virtual machine in Virtualbox with just a double click instead of having to
manually set all the right parameters before importing the actual disk image.

### Improved Flatpak demo

The [apertis-flatdeb-demo](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-demo/)
showcase has been severly improved by:

1. adding GTK to the runtime and to the SDK in order to run and build
   graphical applications
1. replacing the purely command-line demo app with a graphical one
1. improving the handling of different Apertis releases
1. updating the CI pipeline to build the demo flatpak for amd64, arm64 and
   armhf architectures
1. adding the [flatdeb-demo](https://qa.apertis.org/flatdeb-demo.html) testcase
   to our regular runs

### Robot Framework

Packages for the [Robot Framework]({{< ref "robotframework.md" >}}) are now
available in Apertis and exercising them is now
[part of our regular testing runs](https://qa.apertis.org/latest/robotframework.html).

## QA

### Check binary to source package mapping from the dashboard

To further ensure consistency across all the Apertis artifacts, the
[infrastructure dashboard](https://infrastructure.pages.apertis.org/dashboard/)
has now been taught to check that the version of the binary packages published
on our APT repositories match the version of the published source packages and
reflect what is available on GitLab and OBS.

### Updated per-release package listings

To help developers figure out what Apertis ships as part of a specific release,
we now
[list all the packages and their versions](https://infrastructure.pages.apertis.org/dashboard/tsv/index.html)
in a easy-to-consume tab-separated-values (TSV) format.

## Build and integration

### Preparing LAVA server for cloud deployment

The latest upstream LAVA release ships two welcome additions:

1. OpenID-Connect support for implementing Single-Sign-On authentication
1. server ↔ dispatcher communication over HTTPS for easier traversal of proxies

In this quarter the Apertis team has started to prepare for the upgrade,
focusing on improving the deployment story
[using Docker and Kubernetes](https://gitlab.collabora.com/lava/lava/container_registry).

### OpenID-Connect authentication for OBS

Support for OpenID-Connect to implement Single-Sign-On authentication has
been developed for OBS.

## Documentation and designs

### Apertis platform technical vision

The reference scenarios that drive the development of Apertis are now described in the
[Apertis Platform Technical Vision]({{< ref "overview.md" >}}) document:

* Fixed function devices
* HMI devices
* (Industrial) IoT
* SDK

### Device hardening guide

The new document about [device hardening]({{< ref "hardening.md" >}}) collects
all kinds of useful advice for teams that are in the process of securing their
products for widespread usage.

### Apertis development workflow

The various guides describing the Apertis development workflow have now been
reorganized in a more
[coherent and up-to-date workflow document]({{< ref "workflow-guide.md" >}})
that guides developers through all the layers and services that compose the
Apertis infrastructure.

### Security and Access Control

A new guide describes in detail the process of
[creating and maintaining AppArmor profiles]({{< ref "guides/apparmor.md" >}})
to secure services and applications.

### WirePlumber audio policies

The [Audio management]({{< ref "audio-management.md" >}}) document has been
extended with concrete examples to help developers write their own custom
policies using WirePlumber.

### GPL-3 delta assestment

Ensuring that Apertis matches the
[licensing expectations]({{< ref "license-expectations.md" >}}) is a big
undertaking and a few documents already addressed the biggest hurdles:

* [GNU coreutils]( {{< ref "coreutils-replacement.md" >}} )
* [the TLS stack]( {{< ref "tls-stack.md" >}} )
* [GnuPG]( {{< ref "gnupg-replacement.md" >}} )

The [GPL-3 delta assessment]({{< ref "gpl3_free_deltas.md" >}}) document now
also surveys and analyses the remaining problematic components, providing
a clear path forward for the project.

### Evaluate cloud-friendly APT repository publishing

To keep up with adoption and new workloads Apertis is always looking for ways
to improve its infrastructure to scale better and improve maintainability.

The document investigating and evaluating
[cloud-friendly APT publishers]({{< ref "apt-publisher.md" >}}) is the first
step to replace the aging `reprepro`-based implementation that has served
Apertis since its inception.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

#### GNU coreutils replaced by uutils-coreutils

From the next release the Rust-based
[uutils-coreutils package will replace GNU coreutils]({{< ref "coreutils-replacement.md" >}})
on minimal and target images to avoid issues due to the GPL-3 licensing of GNU
coreutils.

GNU coreutils will still be used when building packages and on the SDK images.

### Breaks

#### GPL-3 GnuPG on minimal and target images

The provisions in the [GnuPG replacement]({{< ref "gnupg-replacement.md" >}})
document are planned to be implemented in the next release: this release ships
the GPL-3 GnuPG tools on the minimal and target images.

#### Mildenhall HMI no longer available

From this release the Maynard shell on top of the AGL compositor replaces the
Mildenhall compositor and launcher.

Upstream toolkits like GTK are recommended to replace any usage of the
Mildenhall UI toolkit.

The existing Mildenhall-based applications can still be used through Flatpak,
using the v2021 runtime.

#### Canterbury application framework no longer available

Flatpak is now the reference application framework, replacing Canterbury/Ribchester.

#### PipeWire used by default instead of Pulseaudio

Audio management is now handled with PipeWire and WirePlumber on all images.

Pulseaudio will remain available in the package repositories, even if not installed by defrault.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev2-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High (5)
 - [T7781](https://phabricator.apertis.org/T7781)	aum-ota-api: test failed due to new bootcount test needing a newer uboot
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T7984](https://phabricator.apertis.org/T7984)	Rhosydd test shows status as incomplete on v2021 images
 - [T8024](https://phabricator.apertis.org/T8024)	robotframework test fails on v2022dev2 images
 - [T8029](https://phabricator.apertis.org/T8029)	Unable to apply few of the gitlab-rulez for projects hosted on Gitlab

### Needs Triage (61)
 - [T7898](https://phabricator.apertis.org/T7898)	Failed to start message logs seen on booting v2022dev2 on all the target boards
 - [T7899](https://phabricator.apertis.org/T7899)	Sanity-Check test fails on the v2022dev2 images
 - [T7904](https://phabricator.apertis.org/T7904)	Failed unmounting /var message appears when v2022dev2 AMD64 Minimal OSTree image is shutdown
 - [T7908](https://phabricator.apertis.org/T7908)	aum-api: test failed
 - [T7909](https://phabricator.apertis.org/T7909)	aum-offline-upgrade: test failed
 - [T7910](https://phabricator.apertis.org/T7910)	aum-offline-upgrade-signed: test failed
 - [T7911](https://phabricator.apertis.org/T7911)	aum-ota-auto: test failed
 - [T7912](https://phabricator.apertis.org/T7912)	aum-ota-out-of-space: test failed
 - [T7913](https://phabricator.apertis.org/T7913)	aum-ota-rollback-blacklist: test failed
 - [T7914](https://phabricator.apertis.org/T7914)	aum-ota-signed: test failed
 - [T7915](https://phabricator.apertis.org/T7915)	aum-power-cut: test failed
 - [T7916](https://phabricator.apertis.org/T7916)	connman: test failed
 - [T7917](https://phabricator.apertis.org/T7917)	sanity-check: test failed
 - [T7926](https://phabricator.apertis.org/T7926)	tiny-container-system-aa-enforcement: test failed
 - [T7927](https://phabricator.apertis.org/T7927)	tiny-container-system-basic: test failed
 - [T7928](https://phabricator.apertis.org/T7928)	tiny-container-system-connectivity-profile: test failed
 - [T7929](https://phabricator.apertis.org/T7929)	canterbury: test failed
 - [T7936](https://phabricator.apertis.org/T7936)	bluez-setup test fails in v2022dev2 : ofonod is not running
 - [T7937](https://phabricator.apertis.org/T7937)	bluez-phone test fails in v2022dev2 : gdbus: not found
 - [T7938](https://phabricator.apertis.org/T7938)	apparmor-bluez test fails in v2022dev2
 - [T7939](https://phabricator.apertis.org/T7939)	bluez-avrcp-volume test fails in v2022dev2
 - [T7940](https://phabricator.apertis.org/T7940)	bluez-hfp test fails in v2022dev2
 - [T7941](https://phabricator.apertis.org/T7941)	connman-pan-network-access test fails in v2022dev2
 - [T7943](https://phabricator.apertis.org/T7943)	connman-pan-tethering test fails in v2022dev2
 - [T7944](https://phabricator.apertis.org/T7944)	connman-usb-tethering test fails in v2022dev2
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails in v2022dev2
 - [T7946](https://phabricator.apertis.org/T7946)	webkit2gtk test fails in v2022dev2
 - [T7960](https://phabricator.apertis.org/T7960)	[Logs] /usr/sbin/plymouthd: not found on boot up logs seen on the v2022dev2 target images
 - [T7962](https://phabricator.apertis.org/T7962)	apertis-update-manager-rollback test fails on the v2022dev2 images
 - [T7963](https://phabricator.apertis.org/T7963)	apertis-update-manager-usb-unplug test fails on the v2022dev2 images
 - [T7964](https://phabricator.apertis.org/T7964)	assertion 'G_IS_DBUS_PROXY (proxy)' failed errors seeing on executing apertis-update-manager-ota-rollback test
 - [T7966](https://phabricator.apertis.org/T7966)	apertis-update-manager-amd64-rollback test fails on the v2022dev2 images
 - [T7970](https://phabricator.apertis.org/T7970)	tiny-container-user-aa-enforcement: test failed
 - [T7971](https://phabricator.apertis.org/T7971)	tiny-container-user-basic: test failed
 - [T7977](https://phabricator.apertis.org/T7977)	v2022dev2 Persistent SDK tests fail
 - [T7981](https://phabricator.apertis.org/T7981)	aum-rollback-blacklist: test failed
 - [T7998](https://phabricator.apertis.org/T7998)	Failed to mount VBox_GAs_6.1.18 for v2022dev2 SDK / Base SDK
 - [T7999](https://phabricator.apertis.org/T7999)	Restart & Shutdown buttons are greyed out in the v2022dev2 SDK / BaseSDK
 - [T8000](https://phabricator.apertis.org/T8000)	v2022dev2 SDK / Base SDK does not shutdown corretly
 - [T8004](https://phabricator.apertis.org/T8004)	libapparmor package not found error when executing sdk-ade-remote-debugging test on v2022dev2 SDK/Base SDK
 - [T8006](https://phabricator.apertis.org/T8006)	USB memory sticks are not detected on the v2022dev2 SDK/Base SDK
 - [T8007](https://phabricator.apertis.org/T8007)	AGL compositor test fails on the v2022dev2 SDK / Base SDK
 - [T8009](https://phabricator.apertis.org/T8009)	sdk-flatpak-build-helloworld-app test fails on v2022dev2 sdk / base sdk
 - [T8011](https://phabricator.apertis.org/T8011)	No audio is heard when playing the video in webkit2gtk-gstreamer1.0 test on v2022dev2 SDK
 - [T8026](https://phabricator.apertis.org/T8026)	Songs don't get populated automatically during first boot and after rebooting the target in v2022dev2 images
 - [T8028](https://phabricator.apertis.org/T8028)	Music, Pictures and Videos directory not linked to shared folders in v2022dev2 SDK
 - [T8045](https://phabricator.apertis.org/T8045)	tiny-container-system-device-sharing: test failed
 - [T8046](https://phabricator.apertis.org/T8046)	tiny-container-system-folder-sharing: test failed
 - [T8047](https://phabricator.apertis.org/T8047)	tiny-container-system-seccomp: test failed
 - [T8048](https://phabricator.apertis.org/T8048)	tiny-container-user-connectivity-profile: test failed
 - [T8049](https://phabricator.apertis.org/T8049)	tiny-container-user-device-sharing: test failed
 - [T8050](https://phabricator.apertis.org/T8050)	tiny-container-user-folder-sharing: test failed
 - [T8051](https://phabricator.apertis.org/T8051)	tiny-container-user-seccomp: test failed
 - [T8053](https://phabricator.apertis.org/T8053)	rfkill-toggle test fails in v2022dev2
 - [T8057](https://phabricator.apertis.org/T8057)	Failed to start Remove Sta�…ext4 Metadata Check Snapshots: Log is seeing in image boot process of v2022dev2
 - [T8060](https://phabricator.apertis.org/T8060)	hmi-audio-play-pause test : next or previous button not working on v2022dev2 images
 - [T8061](https://phabricator.apertis.org/T8061)	apparmor-basic-profiles: test failed
 - [T8062](https://phabricator.apertis.org/T8062)	apparmor-dbus: test failed
 - [T8063](https://phabricator.apertis.org/T8063)	aum-offline-upgrade-collection_id: test failed
 - [T8064](https://phabricator.apertis.org/T8064)	dbus-dos-reply-time: test failed
 - [T8065](https://phabricator.apertis.org/T8065)	aum-out-of-space: test failed

### Normal (66)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6727](https://phabricator.apertis.org/T6727)	FTBFS: Apertis v2020pre package build failures
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test fails on internal images
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7530](https://phabricator.apertis.org/T7530)	ADE can't download amd64 sysroot.
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7815](https://phabricator.apertis.org/T7815)	ci-license-scan fails to detect (L)GPL-3 code in util-linux
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7826](https://phabricator.apertis.org/T7826)	Kernel panic logs seen on apertis-update-manager-usb-unplug test on v2019/r-car with u-boot from v2021
 - [T7827](https://phabricator.apertis.org/T7827)	When creaing new test repositories `common-subtree.sh add` gets confused if the checked out branch does not exists in `tests/common`
 - [T7843](https://phabricator.apertis.org/T7843)	Package node-grunt-legacy-log fails to build in  OBS due to environment variables
 - [T7849](https://phabricator.apertis.org/T7849)	Error building package po4a in OBS
 - [T7852](https://phabricator.apertis.org/T7852)	v2022dev2: Investigate test failure TestGetSourceMount
 - [T7854](https://phabricator.apertis.org/T7854)	v2022dev2: FTBFS for package clisp in test streams.tst
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7985](https://phabricator.apertis.org/T7985)	Errors are encountered when trying to install the ssl-cert & postfix packages for Rhoyssd test
 - [T8039](https://phabricator.apertis.org/T8039)	scan-copyrights fails on packages with non ASCII information in package.json
 - [T8058](https://phabricator.apertis.org/T8058)	Rhosydd test fails to launch  rhosydd-speedo-backend.service
