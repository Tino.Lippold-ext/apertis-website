+++
date = "2022-03-18"
weight = 100

title = "v2023dev1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023dev1** is the second **development** of the Apertis
v2023 stable release flow that will lead to the LTS **Apertis v2023.0**
release in March 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations. It currently
ships with the Linux kernel 5.15.x LTS series but later releases in the
v2023 channel will track newer kernel versions up to the next LTS.

Test results for the v2023dev1 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023dev1/20220316.0416/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023dev1/20220316.0416/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023dev1/20220316.0416/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023dev1/20220316.0416/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - **2022 Q1: v2023dev1**
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023dev1.0 images](https://images.apertis.org/release/v2023dev1/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-amd64-uefi_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/hmi/apertis_ostree_v2023dev1-hmi-amd64-uefi_v2023dev1.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/basesdk/apertis_v2023dev1-basesdk-amd64-sdk_v2023dev1.0.ova) | [SDK](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/sdk/apertis_v2023dev1-sdk-amd64-sdk_v2023dev1.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/armhf/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-armhf-uboot_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/armhf/hmi/apertis_ostree_v2023dev1-hmi-armhf-uboot_v2023dev1.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-arm64-uboot_v2023dev1.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-arm64-rpi64_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/hmi/apertis_ostree_v2023dev1-hmi-arm64-rpi64_v2023dev1.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023dev1 repositories

    deb https://repositories.apertis.org/apertis/ v2023dev1 target development sdk


## New features

### Completion of the GPL-3-free modernization

This release completes the modernization of the approach to
[licensing expectations compliance]({{< ref "license-expectations.md" >}})
started with the
[v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}) release.

The [v2022.0]({{< ref "release/v2022.0/releasenotes.md" >}}) release already
addressed the licensing challenges in the
[TLS stack]({{< ref "tls-stack.md" >}}),
[Coreutils]({{< ref "coreutils-replacement.md" >}}), and
[GnuPG]({{< ref "gnupg-replacement.md" >}}) and this release addresses all the
smaller challenges identified in the
[GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}),
in particular the move to `rust-findutils` and `libedit`.

## Build and integration

### Build-test packages on OBS before merging

A new feature of the CI for packaging is the inclusion of OBS build test on branches
to be merged. Thanks to this addition, now developers can confirm that their changes
successfully build in OBS before being merged in the main branches.

### Improve developer experience while importing packages

The infrastructure around Apertis has been improved in order to make the importing of new
packages straightforward, by avoiding as much as possible any manual steps. With these
changes the effort invested to import new packages has been reduced drastically and the possibility
of errors in the process has been notably reduced.

### Automated note importing in QA Report App

The QA report app is the tool used to collect and summarize the test results for all the Apertis images.
To improve the user experience for testers running manual tests and to reduce
the amount of information introduced, now the results notes are
[automatically propagated](https://gitlab.apertis.org/infrastructure/qa-report-app/-/merge_requests/121)
from previous submissions, giving the chance to the user to amend them to match
the current results.

## Documentation and designs

### Thin proxies - REST APIs

This release includes a concept document [Thin proxies: REST APIS]({{ < ref "thin-proxies.md" >}})
with guidelines for developers to access
system APIs through high level REST APIs. Using this approach the complexity of
system APIs can be hidden and developers can chose the language/technology that better
fit their needs.

### Documentation refresh to match the switch to Flatpak

Continuing with the effort of updating documentation after switching to Flatpak, this
release also includes a reviewed version of several documents related to the Application
Framework to provide developers with the best practices to build their applications.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

#### GPL-3 replacements revamp on target images (HMI and FixedFunction)

The obsolete `findutils-gplv2` and `readline5` packages are not longer
available in the `target` packaging repositories.

The HMI and FixedFunction images now use the `rust-findutils` tools and the
API-compatible `libedit` to be compliant with the
[Apertis licensing expectations]({{< ref "license-expectations.md" >}})
while avoiding unmaintained forks of GPL-3 components.

See the [GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}) document
for further details.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/show/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High (22)
 - [T8456](https://phabricator.apertis.org/T8456)	aum-api: test failed
 - [T8470](https://phabricator.apertis.org/T8470)	secure-boot-imx6 test failed 
 - [T8530](https://phabricator.apertis.org/T8530)	evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
 - [T8545](https://phabricator.apertis.org/T8545)	BOM file generation pick default license
 - [T8547](https://phabricator.apertis.org/T8547)	sanity-check: test failed
 - [T8603](https://phabricator.apertis.org/T8603)	AUM fails to detect rollback on Lava for RPi64 board
 - [T8604](https://phabricator.apertis.org/T8604)	AUM tests fails on Lava for RPi64 board
 - [T8660](https://phabricator.apertis.org/T8660)	Random FS issues on OSTree images
 - [T8689](https://phabricator.apertis.org/T8689)	Branching for v2023dev2 disabled CI for several projects
 - [T8729](https://phabricator.apertis.org/T8729)	"Segmentation fault" observed during enabling BT and Wifi dongle
 - [T8749](https://phabricator.apertis.org/T8749)	aum-offline-upgrade-signed: test failed
 - [T8757](https://phabricator.apertis.org/T8757)	aum-ota-api: test failed
 - [T8762](https://phabricator.apertis.org/T8762)	aum-ota-signed: test failed
 - [T8763](https://phabricator.apertis.org/T8763)	aum-out-of-space: test failed
 - [T8764](https://phabricator.apertis.org/T8764)	dbus-dos-reply-time: test failed
 - [T8765](https://phabricator.apertis.org/T8765)	aum-ota-auto: test failed
 - [T8772](https://phabricator.apertis.org/T8772)	aum-power-cut: test failed
 - [T8789](https://phabricator.apertis.org/T8789)	API test
 - [T8790](https://phabricator.apertis.org/T8790)	aum-ota-rollback-blacklist: test failed
 - [T8803](https://phabricator.apertis.org/T8803)	QA Report App: Pipeline fails to test MR
 - [T8822](https://phabricator.apertis.org/T8822)	aum-offline-upgrade: test failed
 - [T8823](https://phabricator.apertis.org/T8823)	connman: test failed

### Normal (41)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7852](https://phabricator.apertis.org/T7852)	Investigate test failure TestGetSourceMount
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails 
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8504](https://phabricator.apertis.org/T8504)	LAVA/Phab bridge timeouts
 - [T8516](https://phabricator.apertis.org/T8516)	apparmor-pipewire: test failed
 - [T8524](https://phabricator.apertis.org/T8524)	grilo: test failed
 - [T8572](https://phabricator.apertis.org/T8572)	Missing warning on coreutils overwrite
 - [T8613](https://phabricator.apertis.org/T8613)	apparmor-functional-demo: test failed
 - [T8622](https://phabricator.apertis.org/T8622)	Manual testcase results should not have any hyperlink in the LavaPhabridge report page
 - [T8629](https://phabricator.apertis.org/T8629)	frome: test failed
 - [T8634](https://phabricator.apertis.org/T8634)	Failed to start Access poi…server : logs seen in v2023dev1 Amd64 boot logs
 - [T8667](https://phabricator.apertis.org/T8667)	cgroups-resource-control: test failed
 - [T8668](https://phabricator.apertis.org/T8668)	Test apparmor-chaiwala-system does not work properly on OSTree images
 - [T8683](https://phabricator.apertis.org/T8683)	"firmware: failed to load" logs seen during boot
 - [T8748](https://phabricator.apertis.org/T8748)	sdk-docker: test failed

