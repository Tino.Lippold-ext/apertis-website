+++
date = "2022-11-15"
weight = 100

title = "v2021.7 Release notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021.7** is the eighth **stable** release of the Apertis v2021
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2021 release stream up to the end
of 2022.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 5.10.x LTS series.

Test results for the v2021.7 release are available in the following test
reports:

  - [APT     images](https://qa.apertis.org/report/v2021/20221110.1454/apt)
  - [OSTree  images](https://qa.apertis.org/report/v2021/20221110.1454/ostree)
  - [NFS  artifacts](https://qa.apertis.org/report/v2021/20221110.1454/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2021/20221110.1454/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2021.7.

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - 2020 Q2: v2021dev2
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - **2022 Q4: v2021.7**


### Release downloads

| [Apertis v2021.7 images](https://images.apertis.org/release/v2021/v2021.7/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2021/v2021.7/amd64/minimal/apertis_v2021-minimal-amd64-uefi_v2021.7.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.7/amd64/target/apertis_v2021-target-amd64-uefi_v2021.7.img.gz) | [base SDK](https://images.apertis.org/release/v2021/v2021.7/amd64/basesdk/apertis_v2021-basesdk-amd64-sdk_v2021.7.vdi.gz) | [SDK](https://images.apertis.org/release/v2021/v2021.7/amd64/sdk/apertis_v2021-sdk-amd64-sdk_v2021.7.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.7/armhf/minimal/apertis_v2021-minimal-armhf-uboot_v2021.7.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.7/arm64/minimal/apertis_v2021-minimal-arm64-uboot_v2021.7.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2021/v2021.7/arm64/minimal/apertis_v2021-minimal-arm64-rpi64_v2021.7.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.7/arm64/target/apertis_v2021-target-arm64-rpi64_v2021.7.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021 package list

The full list of packages available from the v2021 APT repositories is available here in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2021](https://infrastructure.pages.apertis.org/dashboard/tsv/)


#### Apertis v2021 repositories

    deb https://repositories.apertis.org/apertis/ v2021 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2021-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Buster and the latest
LTS Linux kernel on the 5.10.x series.

In this release minimal support for the new [UP Squared 6000 board]({{< ref "amd64.md" >}}) has been
included.

## Regressions

## Deprecations and ABI/API breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[apertis-base](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-base),
[apertis-image-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-image-builder),
[apertis-package-source-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-package-source-builder),
[apertis-flatdeb-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-flatdeb-builder),
[apertis-documentation-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-documentation-builder),
and [apertis-testcases-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure repository](https://build.collabora.co.uk/project/show/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure


### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #205](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/205) devhelp page shows empty data on v2023dev3 SDK
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #214](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/214) iMX.6: u-boot has a misconfigured mmc order in v2023pre
- [Issue #217](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/217) AUM out of space tests fail on UP Squared 6000 board
- [Issue #218](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/218) agl-compositor not showing on UP SQUARED 6000 board setup of lilliput display

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #8](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/8) arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #11](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/11) traprain: 7_traprain test failed
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #17](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/17) SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #24](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/24) Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners
- [Issue #26](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/26) newport: test failed
- [Issue #28](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/28) Investigate test failure TestGetSourceMount
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #46](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/46) frome: test failed
- [Issue #58](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/58) The apertis-flatdeb-mildenhall pipeline is flaky
- [Issue #62](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/62) apparmor-ofono: test failed
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #119](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/119) tiny-container-user-connectivity-profile: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #172](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/172) apparmor-session-lockdown-no-deny: test failed
- [Issue #173](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/173) apparmor-tumbler: test failed
- [Issue #179](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/179) gupnp-services: test failed
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #27](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/27) Failed logs are seen when we do a system update using apertis hawkbit agent on Bosch IOT
