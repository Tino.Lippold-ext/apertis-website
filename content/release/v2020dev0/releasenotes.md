+++
date = "2019-12-18"
weight = 100

title = "V2020dev0 ReleaseNotes"

aliases = [
    "/old-wiki/V2020dev0/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis v2020dev0 Release

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2020dev0** is the first **development** release of the
Apertis v2020 stable release flow that will lead to the LTS **Apertis
v2020.0** release in March 2020.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2020dev0 release are available in the following
test reports:

  - [APT
    images](https://qa.apertis.org/report/v2020dev0/20191003.1)
  - [OSTree
    images](https://qa.apertis.org/report/v2020dev0/20191003.1/ostree)
  - [NFS
    artifacts](https://qa.apertis.org/report/v2020dev0/20191003.0/nfs)
  - [LXC
    containers](https://qa.apertis.org/report/v2020dev0/20191003.0/lxc)

## Release flow

  - 2019 Q3: **v2020dev0**
  - 2019 Q4: v2020pre
  - 2020 Q1: v2020.0
  - 2020 Q2: v2020.1
  - 2020 Q3: v2020.2
  - 2020 Q4: v2020.3
  - 2021 Q1: v2020.4
  - 2021 Q2: v2020.5
  - 2021 Q3: v2020.6
  - 2021 Q4: v2020.7

### Release downloads

| [Apertis v2020dev0 images]() |
| ------------------------------------------------------------------------- |
| Intel 64-bit                                                              |
| ARM 32-bit (U-Boot)                                                       |
| ARM 64-bit (U-Boot)                                                       |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2020dev0 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` v2020dev0 target development sdk hmi`

## New features

### System update authentication checks using Ed25519 signatures

The OSTree updates in this release are now signed to prove their trusted
provenance. The updater on the devices now checks for the signature
against its configurable set of trusted keys before processing the
updates, refusing to install anything that comes from untrusted sources.

While this is usually done with OpenPGP keys and signatures, such
approach is not suitable for Apertis since modern versions of GnuPG
[switched to the GPL-3
license](https://lists.gnupg.org/pipermail/gnupg-announce/2007q3/000255.html)
which conflicts with the [Apertis Open Source license
expectations]( {{< ref "license-expectations.md" >}} ).

In addition, the use of OpenPGP is suboptimal for OSTree: the OpenPGP
standard is large and leads to complex implementations, covering
features that address use-cases like the web of trust that are not
relevant for the system update verification use-case.

To address that, the upstream OSTree code has now been [reworked to
optionally drop its reliance on
GnuPG](https://github.com/ostreedev/ostree/pull/1889), with work being
in progress to land the modularization of the signature verification
mechanism with an alternative backend based on the [Ed25519
cryptosystem](https://ed25519.cr.yp.to/) with the [libsodium
implementation](https://libsodium.org/), which brings the following
benefits:

  - State-of-the-art security
  - Much simpler implementation than GnuPG
  - Small signatures
  - Small keys
  - Fast signature verification
  - Very fast signing

As a technology preview, the Ed25519 backend is already available and
enabled in Apertis showcasing the whole workflow by signing updates
during the image building pipeline on the Apertis server and verifying
those signatures on target device.

## Build and integration

### GitLab-based packaging workflow for multiple parallel releases

The [packaging workflow introduced in the previous
quarter]( {{< ref "/release/v2019pre/releasenotes.md#gitlab-based-packaging-workflow" >}} )
has been extended to handle multiple release streams, dealing with the
`v2019` and `v2020dev0` streams at the same time.

Git branches are used to manage each streams, allowing developers to
control with ease which changes are aimed at each stream.

Once source uploads are built the [GitLab CI
environments](https://docs.gitlab.com/ce/ci/environments) are used
to keep track of the OBS projects they get uploaded to.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle no new deprecations have been issued. Obsolete
or problematic APIs are marked with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

No breaking changes have been introduced in this release.

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2020 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2020)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

` deb `<https://repositories.apertis.org/infrastructure-v2020/>` buster infrastructure`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

  - Opening the Mildenhall songs and video application too soon can lead
    to the thumbnails not being rendered, see  for more details. To work
    around the issue until is fixed, wait a few seconds after boot
    before opening the affected applications.

### High (4)

  - RBEI FTBFS: apitrace

  - The gnu-efi package contains files licensed under the BSD-4-Clause
    license assigned to Intel

  - GL broken in Xvfb and on the SDK due to the removal of the libelf in
    Mesa, causes mutter to FTBFS

  - Thumbnails for songs/videos are not shown when opening the app too
    soon

### Needs Triage (2)

  - AUM automatic power cut test should be reworked to reduce the speed
    of delta read

  - Not available DNS in LAVA tests for Amd64 after reboot

### Normal (136)

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Crash when initialising egl on ARM target

  - Develop test case for out of screen events in Wayland images

  - Test apps are failing in Liblightwood with the use of GTest

  - Fix Tracker testcase to not download media files from random HTTP
    user folders

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Ensure that the arch:all packages in the archive match the
    arch-specific versions

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - gupnp-services: browsing and introspection tests fail

  - dbus-installed-tests: service failed because a timeout was exceeded

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - SDK hangs when trying to execute bluez-hfp testcase

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on the internal
    mx6qsabrelite images with proprietary kernel

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - cgroups-resource-control: cpu-shares test failed

  - tracker-indexing-local-storage: run-test test failed

  - apparmor-tracker: tracker.normal.expected test failed

  - kernel panic occurs when trying to boot an image on i.mx6 revision
    1.2 board from LAVA

  - gupnp-services: test_service_introspection test failed

  - gupnp-services: test_service_browsing test failed

  - build failure in liblightwood-2 on 18.06 image

  - """Setup Install to Target"" option is not showing on eclipse "

  - Ospacks ship a .gitignore file in the root directory

  - rhosydd: integration test failed

  - build-snapshot: allow to build packages without \`autogen.sh\`
    script

  - Wi-Fi search button is missing in wifi application

  - traprain: gnome-desktop-testing test failed

  - apparmor-geoclue: run-test-geoclue test failed

  - bluez-hfp testcase fails

  - \`kbd\` package has broken/problematic dependencies

  - Debos crash when a recipes embbed recipes with ostree-commit and
    ostree-deploy

  - Both sysroot and devroot folder is exist in /opt path

  - The /boot mountpoint is not empty

  - System users are shipped in /usr/etc/passwd instead of /lib/passwd

  - apparmor-gstreamer1-0: run-test-sh test failed

  - Fix the RFS tiny images filesystem structure

  - apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed

  - cgroups-resource-control: 8_cgroups-resource-control test failed

  - folks-alias-persistence: 4_folks-alias-persistence test failed

  - No test-text is displayed inside the Xephyr window.

  - aa_get_complaints.sh script needs to be run with sudo permission

  - apparmor-geoclue: 11_apparmor-geoclue test failed

  - apparmor-tumbler: 9_apparmor-tumbler test failed

  - cgroups-resource-control: 6_cgroups-resource-control test failed

  - gettext-i18n: gettext-i18n test failed

  - apparmor-geoclue: 10_apparmor-geoclue test failed

  - apparmor-tumbler: 8_apparmor-tumbler test failed

  - apparmor-geoclue: apparmor-geoclue test failed

  - apparmor-utils: apparmor-utils test failed

  - gettext-i18n: 11_gettext-i18n test failed

  - tracker-indexing-local-storage: 14_tracker-indexing-local-storage
    test failed

  - canterbury: 3_canterbury test failed

  - Terminal comes up inside the Launcher

  - gettext-i18n: 10_gettext-i18n test failed

  - tracker-indexing-local-storage: 13_tracker-indexing-local-storage
    test failed

  - tracker-indexing-local-storage: 17_tracker-indexing-local-storage
    test failed

  - dbus-installed-tests: trying to overwrite mktemp.1.gz

  - Songs/Videos don't play on i.MX6 with Frampton on internal images

  - sdk-dbus-tools-bustle testcase is failing

  - apparmor-ofono test fails

  - apparmor-bluez-avrcp-volume test fails

  - evolution-sync-bluetooth test fails

  - eclipse-plugins-apertis-management package is missing

  - A2DP test is failing as part of the bluez-phone test

  - No audio/sound is heard while making a call in the bluez-hfp
    testcase

  - Video does not stream in WebKit on the i.MX6 internal images

  - ofono-tests package is missing

  - connman-pan-tethering test fail

  - connman-pan-network-access test fails

  - connman-usb-tethering test fails

  - ifconfig command need to be run with sudo permission

  - libfolks-ofono25 package not found

  - canterbury: 2_canterbury test failed

  - newport: 7_newport test failed

  - newport: 6_newport test failed

  - rhosydd: 8_rhosydd test failed

  - rhosydd: 7_rhosydd test failed

  - traprain: 10_traprain test failed

  - traprain: 9_traprain test failed

  - eclipse-plugins-remote-debugging test fails

  - Thumb nails of songs are not seen on the SDK

  - The pacrunner package used for proxy autoconfiguration is not
    available

  - Auto-resizing of the SDK display in VirtualBox is known to be broken

  - Booting the SDK in VirtualBox appears to be broken with the 6.0.4
    VirtualBox Guest Extensions

  - webkit2gtk-event-handling-redesign test fails on the amd64 ostree
    images

  - simple-agent not found

  - kernel crash during the usb detaching

  - bluez-avrcp-volume test fails on the sdk and base sdk

  - bluez-hfp testcase fails on sdk

  - folks-inspect: command not found

  - Multimedia playback is broken on the internal i.MX6 images (internal
    3.14 ADIT kernel issue)

  - gupnp-services: 13_gupnp-services test failed

  - apparmor-geoclue: 11_apparmor-geoclue test failed

  - youtube Videos are not playing on upstream webkit2GTK

  - Page scroll is lagging in Minibrowser on upstream webkit2GTK

  - Disable timesyncd on images and set fallback time servers for
    ConnMan

  - traprain: 7_traprain test failed

  - frome: 5_frome test failed

  - frome: 6_frome test failed

  - qa-report-app: Tests results are not available for the modules job

  - gitlab-to-obs: Handle packages changing component across releases

  - apparmor-session-lockdown-no-deny test fails on all platforms

  - AppArmor ubercache support is no longer enabled after 18.12

  - ldconfig: Warning comes up when we do an apt-get upgrade on the
    i.MX6

  - Generated lavaphabbridge error report email provides wrong link for
    full report link

  - gettext-i18n: 9_gettext-i18n test failed

  - tracker-indexing-local-storage: 16_tracker-indexing-local-storage
    test failed

  - gupnp-services: 11_gupnp-services test failed

  - Failed to parse .netrc file

  - apparmor-geoclue: 8_apparmor-geoclue test failed

  - apparmor-pulseaudio: 14_apparmor-pulseaudio test failed

  - apparmor-tracker: 21_apparmor-tracker test failed

  - tracker-indexing-local-storage: 20_tracker-indexing-local-storage
    test failed

  - ADE sometimes fails to install sysroot for armhf because the sysroot
    index file is corrupt

  - "SDK results for ""sdk-\*"" tests are not being listed in automated
    test reports"

  - apparmor-geoclue: 7_apparmor-geoclue test failed

  - apparmor-tracker: 20_apparmor-tracker test failed

  - tracker-indexing-local-storage: 19_tracker-indexing-local-storage
    test failed

  - systemctl list-units --failed --no-legend --no-pager --plain

  - Mismatch between downloaded sysroot (apertis v2019 - 20190925.0
    (arm64)) and expected one

### Low (101)

  - Remove unnecessary folks package dependencies for automated tests

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - telepathy-ring: Review and fix SMS test

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - webkit2gtk-drag-and-drop doesn't work with touch

  - libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test
    failed

  - traffic-control-basic test fails

  - frome: 6_frome test failed

  - frome: 5_frome test failed

  - Apertis FTBFS tracker

  - apt-get dist-upgrade fails on SDK

### Lowest (2)

  - cgroups-resource-control: 3_cgroups-resource-control test failed

  - tracker-indexing-local-storage: 18_tracker-indexing-local-storage
    test failed
