+++
date = "2023-06-01"
weight = 100

title = "v2022.5 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.5* is the sixth **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.5 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022/20230517.0219/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022/20230517.0219/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022/20230517.0219/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022/20230517.0219/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.3
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - **2023 Q2: v2022.5**
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.5 images](https://images.apertis.org/release/v2022/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.5/amd64/fixedfunction/apertis_ostree_v2022-fixedfunction-amd64-uefi_v2022.5.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.5/amd64/hmi/apertis_ostree_v2022-hmi-amd64-uefi_v2022.5.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.5/amd64/basesdk/apertis_v2022-basesdk-amd64-sdk_v2022.5.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.5/amd64/sdk/apertis_v2022-sdk-amd64-sdk_v2022.5.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.5/armhf/fixedfunction/apertis_ostree_v2022-fixedfunction-armhf-uboot_v2022.5.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.5/armhf/hmi/apertis_ostree_v2022-hmi-armhf-uboot_v2022.5.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.5/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-uboot_v2022.5.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.5/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-rpi64_v2022.5.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.5/arm64/hmi/apertis_ostree_v2022-hmi-arm64-rpi64_v2022.5.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022.5 package list

The full list of packages available from the v2022.5 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2022.5](https://infrastructure.pages.apertis.org/dashboard/tsv/)

#### Apertis v2022.5 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 5.15.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.com/project/show/apertis:infrastructure:v2022)
provides packages for the required versions in Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up FS corruption issue - part 2
- [Issue #218](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/218) agl-compositor not showing on UP SQUARED 6000 board setup of lilliput display
- [Issue #289](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/289) Difference in license data observation between scan copyright and fossology tool data
- [Issue #301](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/301) Wrong default bootcmd setting in u-boot enviroment for r-car boards
- [Issue #314](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/314) QA Report App: Invalid links to images on Test Report pages
- [Issue #319](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/319) Kernel failure observed on rebooting UpSquared board
- [Issue #325](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/325) Not Tested status for manual testcases should be changed to N/A, for Fixed function ARM64 (rpi64) images
- [Issue #328](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/328) Automated iptables-nmap test executes successfuly even `nmap` debian package not exists on apertis repository

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #256](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/256) OBS: Backend doesn’t start immediately upon restart
- [Issue #260](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/260) tiny-container-user-folder-sharing: test failed
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #291](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/291) tiny-container-system-folder-sharing: test failed
- [Issue #293](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/293) sdk-performance-tools-gprof: test failed
- [Issue #302](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/302) U-boot does not detect correct r-car board variant
- [Issue #307](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/307) Sample applications are pointing by default to apertis/v2021 on all SDK's
- [Issue #326](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/326) OBS: The .asc files from the freetype sources are missing from the internal OBS "published" repositories
- [Issue #331](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/331) Frequent 504 Errors repeatedly causing pipelines to be marked as failed

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #40](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/40) evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #270](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/270) Issue in playing the Video on HMI armhf