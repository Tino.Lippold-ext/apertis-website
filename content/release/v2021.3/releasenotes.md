+++
date = "2021-11-2"
weight = 100

title = "v2021.3 Release notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021.3** is the fourth **stable** release of the Apertis v2021
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2021 release stream up to the end
of 2022.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 5.4.x LTS series.

Test results for the v2021.3 release are available in the following test
reports:

  - [APT     images](https://qa.apertis.org/report/v2021/20211124.0215/apt)
  - [OSTree  images](https://qa.apertis.org/report/v2021/20211124.0215/ostree)
  - [NFS  artifacts](https://qa.apertis.org/report/v2021/20211124.0215/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2021/20211124.0215/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2021.7.

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - 2020 Q2: v2021dev2
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - **2021 Q4: v2021.3**
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7


### Release downloads

| [Apertis v2021.3 images](https://images.apertis.org/release/v2021/v2021.3/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2021/v2021.3/amd64/minimal/apertis_v2021-minimal-amd64-uefi_v2021.3.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.3/amd64/target/apertis_v2021-target-amd64-uefi_v2021.3.img.gz) | [base SDK](https://images.apertis.org/release/v2021/v2021.3/amd64/basesdk/apertis_v2021-basesdk-amd64-sdk_v2021.3.vdi.gz) | [SDK](https://images.apertis.org/release/v2021/v2021.3/amd64/sdk/apertis_v2021-sdk-amd64-sdk_v2021.3.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.3/armhf/minimal/apertis_v2021-minimal-armhf-uboot_v2021.3.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.3/arm64/minimal/apertis_v2021-minimal-arm64-uboot_v2021.3.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2021/v2021.3/arm64/minimal/apertis_v2021-minimal-arm64-rpi64_v2021.3.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.3/arm64/target/apertis_v2021-target-arm64-rpi64_v2021.3.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021 package list

The full list of packages available from the v2021 APT repositories is available here in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2021](https://infrastructure.pages.apertis.org/dashboard/tsv/)


#### Apertis v2021 repositories

    deb https://repositories.apertis.org/apertis/ v2021 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2021-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Buster and the latest
LTS Linux kernel on the 5.4.x series.

As announced, latest source code scan highlighted that the version of Nettle used
on Apertis target images for the v2020 and v2021 releases is already
subject to the relicensing to LGPL-3 or GPL-2 and it is no longer LGPL-
2.1 licensed as the metadata from the original Debian package sources
indicated.

In order to fix this issue the changes to the [TLS stack]( {{< ref "tls-stack" >}} )
applied in v2022 were backported to v2021.3 and new versions of the affected
packages are included in this release.
Under the scope of these changes glib-networking was updated to 2.66 to properly
support OpenSSL backend.

## Regressions

## Deprecations and ABI/API breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[apertis-base](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-base),
[apertis-image-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-image-builder),
[apertis-package-source-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-package-source-builder),
[apertis-flatdeb-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-flatdeb-builder),
[apertis-documentation-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-documentation-builder),
and [apertis-testcases-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure


### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### Normal (64)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails 
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7827](https://phabricator.apertis.org/T7827)	When creaing new test repositories `common-subtree.sh add` gets confused if the checked out branch does not exists in `tests/common`
 - [T7843](https://phabricator.apertis.org/T7843)	Package node-grunt-legacy-log fails to build in  OBS due to environment variables
 - [T7852](https://phabricator.apertis.org/T7852)	v2022dev2: Investigate test failure TestGetSourceMount
 - [T7854](https://phabricator.apertis.org/T7854)	v2022dev2: FTBFS for package clisp in test streams.tst
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7904](https://phabricator.apertis.org/T7904)	Failed unmounting /var message appears when v2022dev2 AMD64 Minimal OSTree image is shutdown 
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails in v2022dev2
 - [T7964](https://phabricator.apertis.org/T7964)	assertion 'G_IS_DBUS_PROXY (proxy)' failed errors seeing on executing apertis-update-manager-ota-rollback test
 - [T7977](https://phabricator.apertis.org/T7977)	v2022dev2 Persistent SDK tests fail
 - [T8029](https://phabricator.apertis.org/T8029)	Unable to apply few of the gitlab-rulez for projects hosted on Gitlab
 - [T8058](https://phabricator.apertis.org/T8058)	Rhosydd test fails to launch  rhosydd-speedo-backend.service
 - [T8069](https://phabricator.apertis.org/T8069)	Sound/Audio is not heard during the very first time when running the webkit2gtk-gstreamer1.0 test 
 - [T8074](https://phabricator.apertis.org/T8074)	Error wifi: Message recipient disconnected from message bus without replying observed on v2022dev3 images
 - [T8101](https://phabricator.apertis.org/T8101)	sound: ASoC: failed to init link HiFi: logs seen in v2020 image bootup
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8255](https://phabricator.apertis.org/T8255)	glib-gio-fs: test failed
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8377](https://phabricator.apertis.org/T8377)	gupnp-services: test failed
