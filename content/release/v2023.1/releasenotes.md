+++
date = "2023-06-08"
weight = 100

title = "v2023.1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023.1** is the second **stable** release of the Apertis
v2023 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
 Apertis is committed to maintaining the v2023 release stream up to the end
of 2024.

This Apertis release is built on top of Debian Bullseye with several
customizations and the Linux kernel 6.1.x LTS series.

Test results for the v2023.1 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023/20230524.1145/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023/20230524.1145/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023/20230524.1145/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023/20230524.1145/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - **2023 Q2: v2023.1**
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023.1 images](https://images.apertis.org/release/v2023/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023/v2023.1/amd64/fixedfunction/apertis_ostree_v2023-fixedfunction-amd64-uefi_v2023.1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.1/amd64/hmi/apertis_ostree_v2023-hmi-amd64-uefi_v2023.1.img.gz) | [base SDK](https://images.apertis.org/release/v2023/v2023.1/amd64/basesdk/apertis_v2023-basesdk-amd64-sdk_v2023.1.ova) | [SDK](https://images.apertis.org/release/v2023/v2023.1/amd64/sdk/apertis_v2023-sdk-amd64-sdk_v2023.1.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.1/armhf/fixedfunction/apertis_ostree_v2023-fixedfunction-armhf-uboot_v2023.1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.1/armhf/hmi/apertis_ostree_v2023-hmi-armhf-uboot_v2023.1.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.1/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.1.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.1/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-rpi64_v2023.1.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.1/arm64/hmi/apertis_ostree_v2023-hmi-arm64-rpi64_v2023.1.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023 package list

The full list of packages available from the v2023 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2023](https://infrastructure.pages.apertis.org/dashboard/tsv/v2023.tsv)

#### Apertis v2023 repositories

    deb https://repositories.apertis.org/apertis/ v2023 target development sdk

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 6.1.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #328](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/328) Automated iptables-nmap test executes successfuly even `nmap` debian package not exists on apertis repository
- [Issue #336](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/336) "lcov" source package: entry "Multi-Arch: foreign" missing in debian/control file
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly
- [Issue #341](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/341) sdk-debos-image-building: test failed
- [Issue #345](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/345) Touch functionality is not working on LILLIPUT Display

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #39](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/39) grilo: test failed
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #260](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/260) tiny-container-user-folder-sharing: test failed
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #278](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/278) chksum report fail! logs are seen when enabling WiFi dongle on the R-Car boards
- [Issue #326](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/326) OBS: The .asc files from the freetype sources are missing from the internal OBS "published" repositories
- [Issue #329](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/329) ade-commands: test failed
- [Issue #331](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/331) Frequent 504 Errors repeatedly causing pipelines to be marked as failed
- [Issue #333](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/333) tiny-container-user-aa-enforcement: test failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #346](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/346) tiny-container-system-device-sharing: test failed

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page

