+++
date = "2022-12-07"
weight = 100

title = "v2023pre Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023pre** is the **preview** release of the Apertis
v2023 stable release flow that will lead to the LTS **Apertis v2023.0**
release in March 2023.

This Apertis release is built on top of Debian Bullseye with several
customizations. It currently ships with the Linux kernel 5.18.x series
but later releases in the v2023 channel will track newer kernel versions
up to the next LTS.

Test results for the v2023pre release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023pre/20221123.0019/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023pre/20221123.0019/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023pre/20221123.0019/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023pre/20221123.0019/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - **2022 Q4: v2023pre**
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023pre.0 images](https://images.apertis.org/release/v2023pre/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023pre/v2023pre.0/amd64/fixedfunction/apertis_ostree_v2023pre-fixedfunction-amd64-uefi_v2023pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2023pre/v2023pre.0/amd64/hmi/apertis_ostree_v2023pre-hmi-amd64-uefi_v2023pre.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023pre/v2023pre.0/amd64/basesdk/apertis_v2023pre-basesdk-amd64-sdk_v2023pre.0.ova) | [SDK](https://images.apertis.org/release/v2023pre/v2023pre.0/amd64/sdk/apertis_v2023pre-sdk-amd64-sdk_v2023pre.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023pre/v2023pre.0/armhf/fixedfunction/apertis_ostree_v2023pre-fixedfunction-armhf-uboot_v2023pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2023pre/v2023pre.0/armhf/hmi/apertis_ostree_v2023pre-hmi-armhf-uboot_v2023pre.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023pre/v2023pre.0/arm64/fixedfunction/apertis_ostree_v2023pre-fixedfunction-arm64-uboot_v2023pre.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023pre/v2023pre.0/arm64/fixedfunction/apertis_ostree_v2023pre-fixedfunction-arm64-rpi64_v2023pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2023pre/v2023pre.0/arm64/hmi/apertis_ostree_v2023pre-hmi-arm64-rpi64_v2023pre.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023pre package list

The full list of packages available from the v2023pre APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2023pre](https://infrastructure.pages.apertis.org/dashboard/tsv/)

#### Apertis v2023pre repositories

    deb https://repositories.apertis.org/apertis/ v2023pre target development sdk

## New features

No new features since this is a preview release.

## Build and integration

### Improved QA Report App configuration management and deployment

A set of changes were introduced to make configuring the QA Report App more straightforward and consistent, also including an improved validation of configuration settings.
These improvements align with the use of **ansible** and **kubernetes** as a way to manage deployments and orchestration,
improving automation and reducing manual work. These changes will also benefit downstream distributions, which will find it easier to deploy new instances.

### QA Report App and OIDC authentication mechanism

As a way to provide more flexibility around authentication mechanisms, the QA Report App now uses
[OpenID Connect](https://openid.net/connect/) as the authentication mechanism, replacing the previous
GitLab OAuth mechanism.

Although the Apertis deployment still uses GitLab as the OIDC provider, this change will allow deployments to use
any OIDC provider that best fit their needs.

Relatedly, the QA Report App now also provides the support to always require login, allowing deployments to control
whether login is required to access the reports.

### Improved QA Report App support for weekly images

Weekly Apertis images are tested with both automated and manual test cases, as part of the QA process.
The results from these tests are submitted to the QA Report App, and now can be easily seen in the main page
which provides a separate section for each type of image: releases, weeklies and dailies.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No know breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023pre-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #169](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/169) aum-out-of-space: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #213](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/213) dbus-installed-tests: test failed
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up FS corruption issue - part 2
- [Issue #223](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/223) Launching an Application on agl-compositor takes more time
- [Issue #224](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/224) SD card timeout issues in RPi4
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #8](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/8) arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #17](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/17) SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #26](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/26) newport: test failed
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #39](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/39) grilo: test failed
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #47](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/47) Failed to start Access poi…server : logs seen in v2023 amd64/armhf boot logs
- [Issue #62](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/62) apparmor-ofono: test failed
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #119](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/119) tiny-container-user-connectivity-profile: test failed
- [Issue #193](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/193) flatpak-run-demo-cli-app: test failed
- [Issue #210](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/210) tiny-container-system-folder-sharing: test failed

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK