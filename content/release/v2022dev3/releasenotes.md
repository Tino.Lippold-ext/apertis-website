+++
date = "2021-09-15"
weight = 100

title = "v2022dev3 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022dev3** is the third **development** release of the Apertis
v2022 stable release flow that will lead to the LTS **Apertis v2022.0**
release in March 2022.

This Apertis release is based on Debian Bullseye. It currently
ships with the Linux kernel 5.10.x LTS series but later releases in the
v2022 channel will track newer kernel versions up to the next LTS scheduled at
the end of 2021.

Test results for the v2022dev3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022dev3/20210915.0019/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022dev3/20210915.0019/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022dev3/20210915.0019/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022dev3/20210915.0019/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - **2021 Q3: v2022dev3**
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022dev3 images](https://images.apertis.org/release/v2022dev3/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022dev3/v2022dev3.0/amd64/fixedfunction/apertis_ostree_v2022dev3-fixedfunction-amd64-uefi_v2022dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2022dev3/v2022dev3.0/amd64/hmi/apertis_ostree_v2022dev3-hmi-amd64-uefi_v2022dev3.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022dev3/v2022dev3.0/amd64/basesdk/apertis_v2022dev3-basesdk-amd64-sdk_v2022dev3.0.ova) | [SDK](https://images.apertis.org/release/v2022dev3/v2022dev3.0/amd64/sdk/apertis_v2022dev3-sdk-amd64-sdk_v2022dev3.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022dev3/v2022dev3.0/armhf/fixedfunction/apertis_ostree_v2022dev3-fixedfunction-armhf-uboot_v2022dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2022dev3/v2022dev3.0/armhf/hmi/apertis_ostree_v2022dev3-hmi-armhf-uboot_v2022dev3.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022dev3/v2022dev3.0/arm64/fixedfunction/apertis_ostree_v2022dev3-fixedfunction-arm64-uboot_v2022dev3.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022dev3/v2022dev3.0/arm64/fixedfunction/apertis_ostree_v2022dev3-fixedfunction-arm64-rpi64_v2022dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2022dev3/v2022dev3.0/arm64/hmi/apertis_ostree_v2022dev3-hmi-arm64-rpi64_v2022dev3.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022dev3 repositories

    deb https://repositories.apertis.org/apertis/ v2022dev3 target development sdk

## New features

### Updates from Debian Bullseye

This release is the second one based on Debian Bullseye, bringing plenty of
upstream updates and fixes.

### GNU coreutils replaced by uutils-coreutils

From this release the Rust-based
[uutils-coreutils]({{< ref "coreutils-replacement.md" >}})
replaces GNU coreutils on Fixed Function and HMI images to avoid
GPL-3 licensing issues that would conflict with the
[Apertis licensing expectations]({{< ref "license-expectations.md" >}}).

This means that from this release the basic userland covered by
coreutils is now provided by a modern and maintained implementation
rather than an old fork.

GNU coreutils is still used when building packages and on the SDK images
to maximise compatibility where the GPL-3 licensing is not a concern.

### GPL-3 GnuPG replaced by sequoia-gpgv

Another step toward the modernization of the changes required to match the
[Apertis licensing expectations]({{< ref "license-expectations.md" >}})
has been landed in this release.

The Fixed Function and HMI images no longer ship neither the current GPL-3
GnuPG nor the old GPL-2 fork that has been used so far, replacing the
required subset of their functionality with a minimal compatibility shim
using the Rust-based [sequoia-gpgv]({{< ref "gnupg-replacement.md" >}}).

The modern GPL-3 GnuPG is used for all the other use-cases that are not
subject to the [Apertis licensing expectations]({{< ref "license-expectations.md" >}}),
like build environments and the SDK images.

### Fixed Function and HMI images to replace minimal and target

Based on the [Apertis Platform Technical Vision]({{< ref "overview.md" >}}) the
set of reference images is now composed of:
- Fixed Function 
- HMI
- BaseSDK
- SDK

This gives a clearer purpose to each image type, providing a better guide to
developers looking at the reference images for their product recipes.

The Fixed Function image is meant to represent images that focus on a single,
specific purpose and are built monolithic ally, with all the components
subject to the same lifecycle. The reference use-case for Fixed Function image
has been selected to be as simple as possible while still being meaningful,
and the choice fell on serving a static web page over HTTP.
The narrow goal of the Fixed Function gives the opportunity to further
optimize it compared to the fuzzy, broad definition of the `minimal` image
in previous releases.

The HMI images focus instead on more elaborate and featureful use-cases: they
boot up in a graphical UI provided by the
[AGL compositor framework]({{< ref "application-framework.md#compositor-libweston" >}})
with the Maynard shell and use the
[Flatpak application framework]({{< ref "application-framework.md#application-runtime-flatpak" >}})
to handle dynamically evolving workloads with life cycles decoupled from the
one for the base operating system.

### Improved Raspberry Pi support

Users of Raspberry Pi will find a better support in Apertis including:

- Support for RPi 400 model.
- Graphical interface now booting without input devices attached.
- Fixed USB support.
- Documentation to bring up [Raspberry Pi 4]({{< ref rpi4_setup.md  >}}).

### Technology preview: iMX8MN Variscite Symphony support 

With this new release the initial support for 
[iMX8MN Variscite Symphony]({{< ref imx8mn_var_symphony_setup.md >}})
has been included, enabling Apertis users to develop their products on top
of this hardware.

### Docker available in the SDK

Having reproducible work environments is a must in today's world to ensure to
get the best from collaboration across the community. For this reason Apertis
now includes Docker in its SDK which has proven to be perfect tool to support
it.

Thanks to Docker it is easy to create an image and share it for a specific
purpose. As an example, Apertis already uses Docker in its infrastructure
for different purposes as described in
[Apertis Docker images](#apertis-docker-images)

### WPA3 support in ConnMan

Following the work from previous release, Apertis now has improved the support
for WPA3/SAE in ConnMan. As part of it
[support for WPA3-Personal transition mode](https://git.kernel.org/pub/scm/network/connman/connman.git/commit/?id=9a54df53361a12458532dfc06156a2b2e468ea0d)
has been added.

## QA

### Improved handling of multiple releases in in the QA report app

With the new release of Apertis the test cases have diverged dramatically from
previous ones. Therefore, QA Report App was enhanced to render the status report
from different release independently, avoiding several previous hacks.

### Improved automatic license compliance

During this release Apertis the process described in
[Automated License Compliance]({{< ref "automated-license-compliance.md" >}})
has been improved to ensure most packages ship license information and any
possible issue could be detected during BOM file generation.

### LAVA upgrade and deployment with Docker/Kubernetes

LAVA, the validation harness used by Apertis to execute automated
tests on the reference hardware platforms, has been upgraded to the
2021.05 release.

Unlike previous releases, this time the whole application has been
deployed with Docker containers, with a significant simplification
of the setup. As a preview, integration with Kubernetes has also
been made available in the test environment, while production still
relies on Docker Compose.

Two important highlights from the new upstream release are:

1. OpenID-Connect support for implementing Single-Sign-On authentication (not currently enabled)
1. server ↔ dispatcher communication over HTTPS for easier traversal of proxies

### Technology preview: RPi4 and iMX8MN Variscite Symphony on LAVA

With the improved support of Raspberry Pi 4 and iMX8MN Variscite Symphony on
Apertis, the next step is to include them in the harness tests performed on
LAVA.

### Improve Bluetooth test robustness

In this new release Bluetooth test were improved by adding support for multiple
pairings consecutively, preventing some test freezes waiting for the remote
device to be discovered and paired.

## Build and integration

### OpenID-Connect authentication for OBS

Support for OpenID-Connect to implement Single-Sign-On authentication has
been [developed for OBS](https://gitlab.collabora.com/obs/open-build-service-debian/-/merge_requests/21).

### Authorization mediator for the Apertis infrastructure

Apertis now supports
[AuthZ mediator](https://gitlab.apertis.org/infrastructure/authz-mediator)
which intercepts the OpenID-Connect authentication flow, extract group
membership information and apply it automatically to ensure that users
have access to the right groups as soon as they access the Apertis services.

Thanks to to this enhancement downstream distributions can easily share Apertis
infrastructure across different product teams.

### Improve Gitlab workflow to derive release from the merge request

Since Apertis supports several releases the workflow in Gitlab has been
improved to make it transparent the choice of the images to be used during
build. This is achieved by deriving the release from the merge request target
branch, which avoids extra configurations and potential errors.

### Improve robustness when fetching large build logs

Apertis development process uses Gitlab and OBS as their pillars. However, in
order to have a single point for all the information, Gitlab is used to collect
build logs from OBS. This process was improved to overcome limitations when
facing large build logs, making information more coherent.

## Documentation and designs

### Lifetime of documents

As Apertis is continuously evolving documentation should also evolve to reflect
that. The lifetime of documents provides a workflow to handle the different
stages of a document as well as making sure it is still up to date.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

#### Apertis Development Environment for application bundles

[The next-gen Apertis application framework]({{< ref "application-framework.md" >}})
is based on Flatpak making the [Apertis Development Environment]({{< ref "ade.md" >}})
only suitable for managing `sysroots` and native applications.

### Breaks

No known breaking changes are part of this release.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev3-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High (27)
 - [T7781](https://phabricator.apertis.org/T7781)	aum-ota-api: test failed due to new bootcount test needing a newer uboot
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T7928](https://phabricator.apertis.org/T7928)	tiny-container-system-connectivity-profile: test failed
 - [T7963](https://phabricator.apertis.org/T7963)	apertis-update-manager-usb-unplug test fails on the v2022dev2 images
 - [T7966](https://phabricator.apertis.org/T7966)	apertis-update-manager-rollback test fails on the v2022dev3 fixed-function images 
 - [T7984](https://phabricator.apertis.org/T7984)	Rhosydd test shows status as incomplete on v2021 images
 - [T8009](https://phabricator.apertis.org/T8009)	sdk-flatpak-build-helloworld-app test fails on v2022dev2 sdk / base sdk
 - [T8048](https://phabricator.apertis.org/T8048)	tiny-container-user-connectivity-profile: test failed
 - [T8064](https://phabricator.apertis.org/T8064)	dbus-dos-reply-time: test failed
 - [T8076](https://phabricator.apertis.org/T8076)	ade-commands: test failed
 - [T8108](https://phabricator.apertis.org/T8108)	Connman pan network access test fails on v2022dev3 images
 - [T8109](https://phabricator.apertis.org/T8109)	Connman pan tethering test fails on v2022dev3 images
 - [T8113](https://phabricator.apertis.org/T8113)	Package gcc-10 does not provide license scan report
 - [T8121](https://phabricator.apertis.org/T8121)	Rythmbox icon is not clearly visible on the agl-compositor 
 - [T8132](https://phabricator.apertis.org/T8132)	sdk-flatpak-demo: test failed
 - [T8139](https://phabricator.apertis.org/T8139)	bluez-hfp test fails on v2022dev3 images
 - [T8144](https://phabricator.apertis.org/T8144)	ConnMan: incomplete WPA3 support: connect failed
 - [T8168](https://phabricator.apertis.org/T8168)	Apertis flatdeb-mildenhall runtime is not published
 - [T8178](https://phabricator.apertis.org/T8178)	hmi-audio-skip-start-end : skip start and skip end button not working on v2022dev3 images using Rythmbox
 - [T8179](https://phabricator.apertis.org/T8179)	root:Project is not configured : error when executing sdk-ade-remote-debugging test on v2022dev3 SDK/Base SDK
 - [T8182](https://phabricator.apertis.org/T8182)	gstreamer1-0-decode: test failed
 - [T8215](https://phabricator.apertis.org/T8215)	apparmor-functional-demo: test failed
 - [T8229](https://phabricator.apertis.org/T8229)	aum-offline-upgrade: test failed
 - [T8230](https://phabricator.apertis.org/T8230)	aum-offline-upgrade-signed: test failed
 - [T8238](https://phabricator.apertis.org/T8238)	LXC tests fail because rust-coreutils `touch`  returns 0 on failure
 - [T8253](https://phabricator.apertis.org/T8253)	apparmor-pipewire: test failed
 - [T8261](https://phabricator.apertis.org/T8261)	connman: test failed

### Normal (74)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found 
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link 
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6727](https://phabricator.apertis.org/T6727)	FTBFS: Apertis v2020pre package build failures
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails 
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7827](https://phabricator.apertis.org/T7827)	When creaing new test repositories `common-subtree.sh add` gets confused if the checked out branch does not exists in `tests/common`
 - [T7843](https://phabricator.apertis.org/T7843)	Package node-grunt-legacy-log fails to build in  OBS due to environment variables
 - [T7852](https://phabricator.apertis.org/T7852)	v2022dev2: Investigate test failure TestGetSourceMount
 - [T7854](https://phabricator.apertis.org/T7854)	v2022dev2: FTBFS for package clisp in test streams.tst
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7904](https://phabricator.apertis.org/T7904)	Failed unmounting /var message appears when v2022dev2 AMD64 Minimal OSTree image is shutdown 
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails in v2022dev2
 - [T7964](https://phabricator.apertis.org/T7964)	assertion 'G_IS_DBUS_PROXY (proxy)' failed errors seeing on executing apertis-update-manager-ota-rollback test
 - [T7977](https://phabricator.apertis.org/T7977)	v2022dev2 Persistent SDK tests fail
 - [T8029](https://phabricator.apertis.org/T8029)	Unable to apply few of the gitlab-rulez for projects hosted on Gitlab
 - [T8058](https://phabricator.apertis.org/T8058)	Rhosydd test fails to launch  rhosydd-speedo-backend.service
 - [T8069](https://phabricator.apertis.org/T8069)	Sound/Audio is not heard during the very first time when running the webkit2gtk-gstreamer1.0 test on SDK/Base SDK images
 - [T8074](https://phabricator.apertis.org/T8074)	Error wifi: Message recipient disconnected from message bus without replying observed on v2022dev3 images
 - [T8101](https://phabricator.apertis.org/T8101)	sound: ASoC: failed to init link HiFi: logs seen in v2020 image bootup
 - [T8153](https://phabricator.apertis.org/T8153)	ci-license-scan on package libuv1
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8239](https://phabricator.apertis.org/T8239)	tiny-container-system-aa-enforcement: test failed
 - [T8240](https://phabricator.apertis.org/T8240)	tiny-container-user-aa-enforcement: test failed
 - [T8241](https://phabricator.apertis.org/T8241)	tiny-container-user-basic: test failed
 - [T8251](https://phabricator.apertis.org/T8251)	gupnp-services: test failed
 - [T8254](https://phabricator.apertis.org/T8254)	cgroups-resource-control: test failed
 - [T8255](https://phabricator.apertis.org/T8255)	glib-gio-fs: test failed
 - [T8275](https://phabricator.apertis.org/T8275)	iptables-nmap shows incomplete state in QA report

