+++
date = "2020-06-29"
weight = 100

title = "V2021dev2 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021dev2** is the third **development** release of the
Apertis v2021 stable release flow that will lead to the LTS **Apertis
v2021.0** release in March 2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2021dev2 release are available in the following
test reports:

  - [APT
    images](https://qa.apertis.org/report/v2021dev2/20200624.0118)
  - [OSTree
    images](https://qa.apertis.org/report/v2021dev2/20200624.0118/ostree)
  - [NFS
    artifacts](https://qa.apertis.org/report/v2021dev2/20200624.0117/nfs)
  - [LXC
    containers](https://qa.apertis.org/report/v2021dev2/20200624.0/lxc)

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - **2020 Q2: v2021dev2**
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7

### Release downloads

| [Apertis v2021dev2 images](https://images.apertis.org/release/v2021dev2/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2021dev2/v2021dev2.0/amd64/minimal/apertis_v2021dev2-minimal-amd64-uefi_v2021dev2.0.img.gz) | [target](https://images.apertis.org/release/v2021dev2/v2021dev2.0/amd64/target/apertis_v2021dev2-target-amd64-uefi_v2021dev2.0.img.gz) | [base SDK](https://images.apertis.org/release/v2021dev2/v2021dev2.0/amd64/basesdk/apertis_v2021dev2-basesdk-amd64-sdk_v2021dev2.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2021dev2/v2021dev2.0/amd64/sdk/apertis_v2021dev2-sdk-amd64-sdk_v2021dev2.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev2/v2021dev2.0/armhf/minimal/apertis_v2021dev2-minimal-armhf-uboot_v2021dev2.0.img.gz) | [target](https://images.apertis.org/release/v2021dev2/v2021dev2.0/armhf/target/apertis_v2021dev2-target-armhf-uboot_v2021dev2.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev2/v2021dev2.0/arm64/minimal/apertis_v2021dev2-minimal-arm64-uboot_v2021dev2.0.img.gz)

The Intel `minimal` and `target` images are tested on the [reference hardware
(MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ), but they can
run on any UEFI-based x86-64 system. The `sdk` image is [tested under
VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021dev2 repositories

    deb https://repositories.apertis.org/apertis/ v2021dev2 target development sdk hmi

## New features

### Signed metadata verification for offline updates

Apertis uses OSTree bundles for offline updates and verifies the authentication
information stored in the contained commit before deploying it on the system.

However, the metadata which defines whether the update is appropriate for the
current device is stored outside of the signed commit, so a separate signature
has now been introduced to ensure that the authenticity of the all the data
processed can be asserted.

### Mildenhall flatpak runtime

A new Flatpak runtime [shipping the Mildenhall
libraries](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-mildenhall)
is now available and the [HelloWorld sample
application](https://gitlab.apertis.org/sample-applications/helloworld-app) has
now been ported to it.

This provides a migration path for applications from the deprecated Canterbury
application framework to the new Flatpak-based solution, as described in the
[Application Framework]( {{< ref "application-framework.md" >}} )
design document.

### Encrypted offline updates technology preview

To add another layer of protection, offline update bundles can now be encrypted
to make their contents inaccessible from unathorized parties.

Keys need to be stored securely on devices using solutions like trusted
computing keyrings, but even when secure boot and trusted computing are not
available encryption can help to discourage attackers.

## Build and integration

### GitLabCI-based image building pipeline

The work started during the
[v2021dev1 cycle]( {{< ref "/release/v2021dev1/releasenotes.md#gitlabci-based-image-building-pipeline-technology-preview" >}} )
to move the image generation to GitLab CI has been completed with the
official switch. The v2021dev2 reference images are now generated from the
GitLab-CI pipeline and the Jenkins pipeline has been dropped.

The new GitLab-CI pipeline has proven to scale much better, leading to faster
build times. It is now enabled even for secondary branches and every commit is
fully tested before being merged to the main branch.

### The feedback from OBS builds is now integrated with GitLab CI uploads

When uploading packages the [GitLab-to-OBS packaging
pipeline](https://gitlab.apertis.org/infrastructure/ci-package-builder) now
shows the live build log from OBS and propagates the final build status to
the GitLab pipeline.

Developers can find and monitor all the information about their changes in a
single integrated place, tracking issues and regressions more easily.

### Move binary artifacts extraction for tests to GitLab CI

With the overarching goal of converging to GitLab and dropping Jenkins, the
pipeline that extracts binary artifacts and stores them in the testcase git
repositories has been converted to GitLab CI, making it more flexible and
easier to use and test.

### Emulated OBS package builds on Intel x86-64 proof-of-concept

One of the main limitations on making Apertis scale easily on a cloud deployment
is the requirement for native ARM builders for OBS, since not all cloud
providers have ARM machines available.

To address this scenario we started investigating using emulated builders that
use the QEMU user mode emulation to build ARM/ARM64 packages on Intel x86-64 machines.

The [OBS Docker Compose recipes have been
extended](https://gitlab.collabora.com/obs/docker-obs/-/tree/collabora-emulated-builds)
to demonstrate the setup.

A set of packages chosen because their language/runtime diversity has been
built with successful results, and the next step is to test this approach
at scale.

### HTTPS proxy for Open Build Service builders

The connection flow between the OBS builders and the server is quite complex:
builders register themselves to the server, but then the server opens separate
connections to each builder, under the assumption they have a directly
reachable IP. So far this has been handled by setting up dedicated virtual
networks but on some deployments this is not a viable solution.

The [OBS proxy](https://gitlab.apertis.org/infrastructure/obs-proxy) avoids
these issues by tunnelling the data over a single HTTP connection from the
workers to the server. With the OBS proxy it is now possible to host builders
behind a restrictive HTTPS proxy with ease.

## Documentation and designs

### Website reorganization

The contents on www.apertis.org have been massively reorganized to make them
much easier to find and consume.

The information on the website is being categorised making it easier for
users to find relevant content. To improve searches and clarity, each document
is being evaluated to determine its relevancy to recent releases, with the
contents having been updated or removed accordingly.

The look and feel of the documentation has also see some small general
improvements.

### Trusted zones with OP-TEE

Trusted computing is a topic of growing importance as it enables new way to
ensure the security of products and of the data they host.

The [Integration of OP-TEE in
Apertis]( {{< ref "op-tee.md" >}} ) document describes
the purpose and benefits of trusted computing technologies like OP-TEE and
defines how OP-TEE can be integrated in Apertis.

### OSS compliance

Generating a bill-of-materials (BOM) listing all the licenses involved in
generating an image is an important step to ensure that every product is in
full compliance with the requirements of those licenses.

The [Automated License
Compliance]( {{< ref "automated-license-compliance.md" >}} )
document proposes a way to collect and store the information needed to generate
such BOMs with minimal human guidance.

### Integrated site search on www.apertis.org

Finding contents on the www.apertis.org website is now much easier as it now
provides integrated search functionality.

With the move away from the wiki the search functionality was lost, and we
now have re-introduced it using client-side JavaScript and pre-computed indexes.

## QA

### Project-wide dashboard

To improve the monitoring of the overall project health, a [new
dashboard](https://infrastructure.pages.apertis.org/dashboard/) has been made
available.

It currently keeps track of packages with pending updates from upstream but it
is based on a extensible pipeline that will allow us to progressively add more
data sources and checks to always ensure that everything is under control.

### Automated OTA updates testcases 

The tests exercising the Over-the-Air update functionality have been manually
executed in previous releases. They were quite time consuming, so we put some
effort to fully automate them in this release.

The [`aum-ota-*`](https://qa.apertis.org/report/v2021dev2/20200618.0117/ostree#aum-ota-api)
tests now automatically exercise online updates on every build, ensuring that
regressions are caught in a timely manner.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

#### Image builds now happen on GitLab-CI

The [GitLab-CI
pipeline](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/pipelines)
is now producing the reference images, and the old Jenkins pipeline has been
dismissed.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev2-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev2-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-package-sources/v2021dev2-package-source-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-testcasess/v2021dev2-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (3)
 - [T7011](https://phabricator.apertis.org/T7011)	No audio is heard after rooting audio to jack and performing a reboot
 - [T7056](https://phabricator.apertis.org/T7056)	FAILED: Error running command: ['ssh-copy-id', 'user@127.0.0.1' errors are shown when running the sdk-persistent testcases
 - [T7139](https://phabricator.apertis.org/T7139)	Incomplete description of the contribution process into Apertis

### Normal (169)
 - [T2043](https://phabricator.apertis.org/T2043)	property changed signal in org.bluez.Network1 is not emiting when PAN disconnects
 - [T2853](https://phabricator.apertis.org/T2853)	GStreamer playbin prioritises imxeglvivsink over clutterautovideosink
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3121](https://phabricator.apertis.org/T3121)	Test apps are failing in Liblightwood with the use of GTest
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3217](https://phabricator.apertis.org/T3217)	VirtualBox display freezes when creating multiple notifications at once and interacting (hover and click) with them
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3291](https://phabricator.apertis.org/T3291)	tracker tests: Error creating thumbnails: No poster key found in metadata
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T3970](https://phabricator.apertis.org/T3970)	Ensure that the arch:all packages in the archive match the arch-specific versions
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4242](https://phabricator.apertis.org/T4242)	gnutls depends on old abandoned package gmp4 due to licensing reasons
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4394](https://phabricator.apertis.org/T4394)	gupnp-services: browsing and introspection tests fail
 - [T4409](https://phabricator.apertis.org/T4409)	tracker-indexing-mass-storage test case fails
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4568](https://phabricator.apertis.org/T4568)	Ribchester mount unit depends on Btrfs
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T4755](https://phabricator.apertis.org/T4755)	Seek/Pause option does not work correctly on YouTube
 - [T5053](https://phabricator.apertis.org/T5053)	Canterbury needs to explicitly killed to relaunch the Mildenhall compositor 
 - [T5128](https://phabricator.apertis.org/T5128)	cgroups-resource-control: cpu-shares test failed
 - [T5153](https://phabricator.apertis.org/T5153)	tracker-indexing-local-storage: test failed
 - [T5210](https://phabricator.apertis.org/T5210)	apparmor-tracker: tracker.normal.expected test failed
 - [T5256](https://phabricator.apertis.org/T5256)	gupnp-services: test_service_introspection test failed
 - [T5284](https://phabricator.apertis.org/T5284)	gupnp-services: test_service_browsing test failed
 - [T5335](https://phabricator.apertis.org/T5335)	"Setup Install to Target" option is not showing on eclipse 
 - [T5351](https://phabricator.apertis.org/T5351)	rhosydd: integration test failed
 - [T5468](https://phabricator.apertis.org/T5468)	build-snapshot: allow to build packages without `autogen.sh` script
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5498](https://phabricator.apertis.org/T5498)	traprain: gnome-desktop-testing test failed
 - [T5576](https://phabricator.apertis.org/T5576)	bluez-hfp testcase fails
 - [T5611](https://phabricator.apertis.org/T5611)	`kbd` package has broken/problematic dependencies
 - [T5739](https://phabricator.apertis.org/T5739)	Debos crash when a recipes embbed recipes with ostree-commit and ostree-deploy
 - [T5747](https://phabricator.apertis.org/T5747)	The /boot mountpoint is not empty
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5757](https://phabricator.apertis.org/T5757)	apparmor-gstreamer1-0: run-test-sh test failed
 - [T5763](https://phabricator.apertis.org/T5763)	Fix the RFS tiny images filesystem structure
 - [T5765](https://phabricator.apertis.org/T5765)	apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed
 - [T5769](https://phabricator.apertis.org/T5769)	cgroups-resource-control: 8_cgroups-resource-control test failed
 - [T5770](https://phabricator.apertis.org/T5770)	folks-alias-persistence: 4_folks-alias-persistence test failed
 - [T5818](https://phabricator.apertis.org/T5818)	apparmor-tumbler: 9_apparmor-tumbler test failed
 - [T5819](https://phabricator.apertis.org/T5819)	cgroups-resource-control: 6_cgroups-resource-control test failed
 - [T5821](https://phabricator.apertis.org/T5821)	gettext-i18n: gettext-i18n test failed
 - [T5833](https://phabricator.apertis.org/T5833)	apparmor-tumbler: 8_apparmor-tumbler test failed
 - [T5837](https://phabricator.apertis.org/T5837)	apparmor-utils: apparmor-utils test failed
 - [T5838](https://phabricator.apertis.org/T5838)	gettext-i18n: 11_gettext-i18n test failed
 - [T5848](https://phabricator.apertis.org/T5848)	canterbury: 3_canterbury test failed
 - [T5852](https://phabricator.apertis.org/T5852)	Terminal comes up inside the Launcher
 - [T5857](https://phabricator.apertis.org/T5857)	gettext-i18n: 10_gettext-i18n test failed
 - [T5861](https://phabricator.apertis.org/T5861)	dbus-installed-tests: trying to overwrite mktemp.1.gz
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5902](https://phabricator.apertis.org/T5902)	A2DP test is failing as part of the bluez-phone test
 - [T5905](https://phabricator.apertis.org/T5905)	No audio/sound is heard while making a call in the bluez-hfp testcase
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images 
 - [T5913](https://phabricator.apertis.org/T5913)	ofono-tests package is missing 
 - [T5929](https://phabricator.apertis.org/T5929)	connman-pan-tethering test fail 
 - [T5930](https://phabricator.apertis.org/T5930)	connman-pan-network-access test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T5933](https://phabricator.apertis.org/T5933)	ifconfig command need to be run with sudo permission
 - [T5935](https://phabricator.apertis.org/T5935)	libfolks-ofono25 package not found
 - [T5988](https://phabricator.apertis.org/T5988)	canterbury: 2_canterbury test failed
 - [T5991](https://phabricator.apertis.org/T5991)	newport: 7_newport test failed
 - [T5992](https://phabricator.apertis.org/T5992)	newport: 6_newport test failed
 - [T5993](https://phabricator.apertis.org/T5993)	rhosydd: 8_rhosydd test failed
 - [T5994](https://phabricator.apertis.org/T5994)	rhosydd: 7_rhosydd test failed
 - [T5997](https://phabricator.apertis.org/T5997)	traprain: 10_traprain test failed
 - [T5998](https://phabricator.apertis.org/T5998)	traprain: 9_traprain test failed
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6003](https://phabricator.apertis.org/T6003)	Thumb nails of songs are not seen on the SDK
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6011](https://phabricator.apertis.org/T6011)	Booting the SDK in VirtualBox appears to be broken with the 6.0.4 VirtualBox Guest Extensions
 - [T6012](https://phabricator.apertis.org/T6012)	webkit2gtk-event-handling-redesign test fails on the amd64 ostree images
 - [T6015](https://phabricator.apertis.org/T6015)	simple-agent not found
 - [T6017](https://phabricator.apertis.org/T6017)	bluez-avrcp-volume test fails on the sdk and base sdk
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found 
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue) 
 - [T6057](https://phabricator.apertis.org/T6057)	gupnp-services: 13_gupnp-services test failed
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6130](https://phabricator.apertis.org/T6130)	frome: 5_frome test failed
 - [T6131](https://phabricator.apertis.org/T6131)	frome: 6_frome test failed
 - [T6231](https://phabricator.apertis.org/T6231)	gitlab-to-obs: Handle packages changing component across releases 
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6277](https://phabricator.apertis.org/T6277)	ldconfig: Warning comes up when we do an apt-get upgrade on the i.MX6 
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link 
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: 9_gettext-i18n test failed
 - [T6296](https://phabricator.apertis.org/T6296)	gupnp-services: 11_gupnp-services test failed
 - [T6307](https://phabricator.apertis.org/T6307)	apparmor-pulseaudio: 14_apparmor-pulseaudio test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6350](https://phabricator.apertis.org/T6350)	sdk-debug-tools-gdb: 4_sdk-debug-tools-gdb test failed
 - [T6351](https://phabricator.apertis.org/T6351)	sdk-debug-tools-strace: 5_sdk-debug-tools-strace test failed
 - [T6352](https://phabricator.apertis.org/T6352)	sdk-debug-tools-valgrind: 6_sdk-debug-tools-valgrind test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6369](https://phabricator.apertis.org/T6369)	apparmor-gstreamer1-0: 12_apparmor-gstreamer1-0 test failed
 - [T6444](https://phabricator.apertis.org/T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6466](https://phabricator.apertis.org/T6466)	Thumbnails for songs/videos are not shown when opening the app too soon
 - [T6614](https://phabricator.apertis.org/T6614)	aum-update-rollback-tests/armhf: Rollback situation is not reproduced on public armhf target and internal images
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6652](https://phabricator.apertis.org/T6652)	tests: apparmor-tracker, apparmor-geoclue: absent malicious library
 - [T6656](https://phabricator.apertis.org/T6656)	apparmor-geoclue: 8_apparmor-geoclue test failed
 - [T6657](https://phabricator.apertis.org/T6657)	apparmor-tracker: 21_apparmor-tracker test failed
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6666](https://phabricator.apertis.org/T6666)	apparmor-geoclue: 7_apparmor-geoclue test failed
 - [T6667](https://phabricator.apertis.org/T6667)	apparmor-tracker: 20_apparmor-tracker test failed
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms 
 - [T6670](https://phabricator.apertis.org/T6670)	Remove or move git-mediawiki package from the development repo
 - [T6680](https://phabricator.apertis.org/T6680)	Drop mkdocs package from development repository
 - [T6681](https://phabricator.apertis.org/T6681)	Fix btrfs packages in the development repository
 - [T6682](https://phabricator.apertis.org/T6682)	Drop or fix installation for git-all package in development repository
 - [T6683](https://phabricator.apertis.org/T6683)	Drop or fix installation for gccbrig package in development repository
 - [T6684](https://phabricator.apertis.org/T6684)	Move dh-python package from target to development repository 
 - [T6685](https://phabricator.apertis.org/T6685)	Fix gstreamer1.0-gl package in target repository
 - [T6686](https://phabricator.apertis.org/T6686)	Move kernel-wedge package from target to development repository 
 - [T6687](https://phabricator.apertis.org/T6687)	Move kdump-tools package from target to development repository
 - [T6688](https://phabricator.apertis.org/T6688)	Fix libblockdev-btrfs2 in target repository
 - [T6689](https://phabricator.apertis.org/T6689)	Move skales package from target to development repository
 - [T6690](https://phabricator.apertis.org/T6690)	Fix mesa-vdpau-drivers package in target repository
 - [T6691](https://phabricator.apertis.org/T6691)	Fix mesa-va-drivers package in target repository
 - [T6692](https://phabricator.apertis.org/T6692)	Move makedumpfile package from target to development repository
 - [T6693](https://phabricator.apertis.org/T6693)	Move lsb-release package from target to development repository
 - [T6694](https://phabricator.apertis.org/T6694)	Fix firmware-linux package in target
 - [T6695](https://phabricator.apertis.org/T6695)	Remove remaining hotdoc dbgsym package from development repository
 - [T6727](https://phabricator.apertis.org/T6727)	FTBFS: Apertis v2020pre package build failures
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6816](https://phabricator.apertis.org/T6816)	apertis.org: Replace the slogan
 - [T6834](https://phabricator.apertis.org/T6834)	Apertis-flatdeb build issues.
 - [T6841](https://phabricator.apertis.org/T6841)	sdk-code-analysis-tools-splint: 4_sdk-code-analysis-tools-splint test failed
 - [T6842](https://phabricator.apertis.org/T6842)	sdk-debug-tools-gdb: 5_sdk-debug-tools-gdb test failed
 - [T6843](https://phabricator.apertis.org/T6843)	sdk-debug-tools-strace: 6_sdk-debug-tools-strace test failed
 - [T6844](https://phabricator.apertis.org/T6844)	sdk-debug-tools-valgrind: 7_sdk-debug-tools-valgrind test failed
 - [T6855](https://phabricator.apertis.org/T6855)	busybox wget does not work with https proxy
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6891](https://phabricator.apertis.org/T6891)	apparmor-pulseaudio: 13_apparmor-pulseaudio test failed
 - [T6903](https://phabricator.apertis.org/T6903)	U-Boot boot counter is used for AMD64 & ARM64
 - [T6904](https://phabricator.apertis.org/T6904)	apertis-update-manager-usb-unplug test fails on ARM64 image  
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails 
 - [T6968](https://phabricator.apertis.org/T6968)	apparmor-session-lockdown-no-deny: 9_apparmor-session-lockdown-no-deny test failed
 - [T6969](https://phabricator.apertis.org/T6969)	apparmor-session-lockdown-no-deny: 15_apparmor-session-lockdown-no-deny test failed
 - [T6970](https://phabricator.apertis.org/T6970)	apparmor-tracker: 22_apparmor-tracker test failed
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [T7018](https://phabricator.apertis.org/T7018)	Chaiwala-logo doesn't come up on boot up
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny: test failed linux-mx6_3.14.79-0co1.
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7161](https://phabricator.apertis.org/T7161)	tracker-indexing-local-storage: test failed
 - [T7201](https://phabricator.apertis.org/T7201)	gitlab CI reporting of OBS build status fails under modest/heavy load
 - [T7203](https://phabricator.apertis.org/T7203)	Sometimes Connman is unable to establish a Wi-Fi connection on minnowboard
 - [T7204](https://phabricator.apertis.org/T7204)	Bluez pairing operation is failing from time to time
 - [T7207](https://phabricator.apertis.org/T7207)	PBAP Phonebook Access PSE profile fails from time to time
 - [T7223](https://phabricator.apertis.org/T7223)	didcot: test failed
 - [T7224](https://phabricator.apertis.org/T7224)	OBS logging job fails in Gitlab CI pipeline 

### Low (21)
 - [T1809](https://phabricator.apertis.org/T1809)	Upstream: linux-tools-generic should depend on lsb-release
 - [T1924](https://phabricator.apertis.org/T1924)	telepathy-ring: Review and fix SMS test
 - [T1964](https://phabricator.apertis.org/T1964)	Mildenhall compositor crops windows
 - [T2142](https://phabricator.apertis.org/T2142)	Power button appers to be disabled on target 
 - [T2226](https://phabricator.apertis.org/T2226)	Network search pop-up isn't coming up in wi-fi settings
 - [T2367](https://phabricator.apertis.org/T2367)	Videos are hidden when Eye is launched
 - [T2483](https://phabricator.apertis.org/T2483)	Video doesn't play when toggling from full screen to detail view
 - [T2498](https://phabricator.apertis.org/T2498)	Simulator screen is not in center but left aligned 
 - [T2704](https://phabricator.apertis.org/T2704)	The video player window is split into 2 frames in default view
 - [T3161](https://phabricator.apertis.org/T3161)	If 2 drawers are activated, the most recent one hides behind the older one, instead of coming on top of older one.
 - [T3537](https://phabricator.apertis.org/T3537)	cgroups-resource-control: test network-cgroup-prio-class failed
 - [T3759](https://phabricator.apertis.org/T3759)	Status bar is not getting updated with the current song/video being played 
 - [T3955](https://phabricator.apertis.org/T3955)	rhosydd-client crashes when displaying vehicle properties for mock backend
 - [T3965](https://phabricator.apertis.org/T3965)	Rhosydd service crashes when client exits on some special usecases (Refer description for it)
 - [T4166](https://phabricator.apertis.org/T4166)	On multiple re-entries from settings to eye the compositor hangs 
 - [T4243](https://phabricator.apertis.org/T4243)	Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll test case
 - [T4296](https://phabricator.apertis.org/T4296)	Segmentation fault is observed on closing the mildenhall-compositor
 - [T4490](https://phabricator.apertis.org/T4490)	webkit2gtk-drag-and-drop doesn't work with touch 
 - [T5898](https://phabricator.apertis.org/T5898)	traffic-control-basic test fails
 - [T6065](https://phabricator.apertis.org/T6065)	apt-get dist-upgrade fails on SDK
 - [T7225](https://phabricator.apertis.org/T7225)	webkit2gtk-ac-3d-rendering test fails

### Lowest (80)
 - [T789](https://phabricator.apertis.org/T789)	Remove unnecessary folks package dependencies for automated tests
 - [T1556](https://phabricator.apertis.org/T1556)	No connectivity Popup is not seen when the internet is disconnected.
 - [T1960](https://phabricator.apertis.org/T1960)	remove INSTALL, aclocal.m4 files from langtoft
 - [T2028](https://phabricator.apertis.org/T2028)	Documentation is not available from the main folder
 - [T2224](https://phabricator.apertis.org/T2224)	apparmor-libreoffice: libreoffice.normal.expected fails: ods_to_pdf: fail [Bugzilla bug #331] 
 - [T2299](https://phabricator.apertis.org/T2299)	Clutter_text_set_text API redraws entire clutterstage
 - [T2317](https://phabricator.apertis.org/T2317)	libgrassmoor: executes tracker-control binary 
 - [T2318](https://phabricator.apertis.org/T2318)	mildenhall-settings: does not generate localization files from source
 - [T2475](https://phabricator.apertis.org/T2475)	Theme ,any F node which is a child of an E node is not working for Apertis widgets.
 - [T2781](https://phabricator.apertis.org/T2781)	Horizontal scroll is not shown on GtkClutterLauncher
 - [T2785](https://phabricator.apertis.org/T2785)	The background HMI is blank on clicking the button for Power OFF
 - [T2788](https://phabricator.apertis.org/T2788)	Share links to facebook, twitter are nbt working in browser (GtkClutterLauncher)
 - [T2790](https://phabricator.apertis.org/T2790)	Background video is not played in some website with GtkClutterLauncher 
 - [T2833](https://phabricator.apertis.org/T2833)	Interaction with PulseAudio not allowed by its AppArmor profile
 - [T2858](https://phabricator.apertis.org/T2858)	shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes /var/root/.cache/dconf/ 
 - [T2889](https://phabricator.apertis.org/T2889)	Cannot open/view pdf documents in browser (GtkClutterLauncher)
 - [T2890](https://phabricator.apertis.org/T2890)	Zoom in feature does not work on google maps 
 - [T2892](https://phabricator.apertis.org/T2892)	Printing hotdoc webpage directly results in misformatted document
 - [T2917](https://phabricator.apertis.org/T2917)	Images for the video links are not shown in news.google.com on GtkClutterLauncher 
 - [T2995](https://phabricator.apertis.org/T2995)	Focus in launcher rollers broken because of copy/paste errors
 - [T3008](https://phabricator.apertis.org/T3008)	beep audio decoder gives errors continously
 - [T3171](https://phabricator.apertis.org/T3171)	Unusable header in Traprain section in Devhelp
 - [T3174](https://phabricator.apertis.org/T3174)	Clang package fails to install appropriate egg-info needed by hotdoc
 - [T3219](https://phabricator.apertis.org/T3219)	Canterbury messes up kerning when .desktop uses unicode chars
 - [T3237](https://phabricator.apertis.org/T3237)	make check fails on libbredon package for wayland warnings
 - [T3280](https://phabricator.apertis.org/T3280)	Cannot open links within website like yahoo.com
 - [T3319](https://phabricator.apertis.org/T3319)	mx6qsabrelite: linking issue with libgstimxeglvivsink.so and libgstimxvpu.so gstreamer plugins
 - [T3332](https://phabricator.apertis.org/T3332)	Compositor seems to hide the bottom menu of a webpage 
 - [T3430](https://phabricator.apertis.org/T3430)	Spacing issues between text and selection box in website like amazon
 - [T3431](https://phabricator.apertis.org/T3431)	Content on a webpage doesn't load in sync with the scroll bar
 - [T3433](https://phabricator.apertis.org/T3433)	Resizing the window causes page corruption 
 - [T3506](https://phabricator.apertis.org/T3506)	Confirm dialog status updated before selecting the confirm option YES/NO
 - [T3517](https://phabricator.apertis.org/T3517)	webview Y offset not considered to place, full screen video on youtube webpage
 - [T3563](https://phabricator.apertis.org/T3563)	GObject Generator link throws 404 error
 - [T3564](https://phabricator.apertis.org/T3564)	GLib, GIO Reference Manual links are incorrectly swapped 
 - [T3580](https://phabricator.apertis.org/T3580)	Canterbury entry-point launching hides global popups, but only sometimes
 - [T3588](https://phabricator.apertis.org/T3588)	<abstractions/chaiwala-base> gives privileges that not every app-bundle should have
 - [T3631](https://phabricator.apertis.org/T3631)	Segmentation fault when disposing test executable of mildenhall
 - [T3647](https://phabricator.apertis.org/T3647)	The web runtime doesn't set the related view when opening new windows
 - [T3729](https://phabricator.apertis.org/T3729)	ribchester: gnome-desktop-testing test times out
 - [T3730](https://phabricator.apertis.org/T3730)	canterbury: Most of the tests fail
 - [T3763](https://phabricator.apertis.org/T3763)	Compositor hides the other screens
 - [T3771](https://phabricator.apertis.org/T3771)	Roller problem in settings application 
 - [T3797](https://phabricator.apertis.org/T3797)	Variable roller is not working
 - [T3798](https://phabricator.apertis.org/T3798)	In mildenhall, URL history speller implementation is incomplete.
 - [T3909](https://phabricator.apertis.org/T3909)	MildenhallSelectionPopupItem doesn't take ownership when set properties
 - [T3939](https://phabricator.apertis.org/T3939)	libshoreham packaging bugs
 - [T3940](https://phabricator.apertis.org/T3940)	libmildenhall-0-0 contains files that would conflict with a future libmildenhall-0-1
 - [T3969](https://phabricator.apertis.org/T3969)	MildenhallSelPopupItem model should be changed to accept only gchar * instead of MildenhallSelPopupItemIconDetail for icons
 - [T3971](https://phabricator.apertis.org/T3971)	libbredon/seed uninstallable on target as they depend on libraries in :development
 - [T3972](https://phabricator.apertis.org/T3972)	webview-test should be shipped in libbredon-0-tests instead of libbredon-0-1
 - [T3973](https://phabricator.apertis.org/T3973)	bredon-0-launcher should be shipped in its own package, not in libbredon-0-1
 - [T3991](https://phabricator.apertis.org/T3991)	virtual keyboard is not showing for password input field of any webpage
 - [T3992](https://phabricator.apertis.org/T3992)	Steps like pattern is seen in the background in songs application
 - [T3996](https://phabricator.apertis.org/T3996)	Avoid unconstrained dbus AppArmor rules in frome
 - [T4005](https://phabricator.apertis.org/T4005)	Newport test fails on minimal images
 - [T4009](https://phabricator.apertis.org/T4009)	connman: patch "Use ProtectSystem=true" rejected upstream
 - [T4010](https://phabricator.apertis.org/T4010)	connman: patch "device: Don't report EALREADY" not accepted upstream
 - [T4027](https://phabricator.apertis.org/T4027)	webkit2GTK crash observed flicking on webview from other widget
 - [T4031](https://phabricator.apertis.org/T4031)	Mildenhall should install themes in the standard xdg data dirs
 - [T4046](https://phabricator.apertis.org/T4046)	Page rendering is not smooth in sites like www.yahoo.com
 - [T4048](https://phabricator.apertis.org/T4048)	HTML5 demo video's appear flipped when played on webkit2 based browser app
 - [T4050](https://phabricator.apertis.org/T4050)	Render theme buttons are not updating with respect to different zoom levels
 - [T4052](https://phabricator.apertis.org/T4052)	Rendering issue observed on websites like http://www.moneycontrol.com
 - [T4089](https://phabricator.apertis.org/T4089)	Crash observed on webruntime framework
 - [T4110](https://phabricator.apertis.org/T4110)	Crash observed on seed module when we accesing the D-Bus call method
 - [T4142](https://phabricator.apertis.org/T4142)	introspectable support for GObject property, signal and methods
 - [T4244](https://phabricator.apertis.org/T4244)	zoom feature is not working as expected 
 - [T4348](https://phabricator.apertis.org/T4348)	Inital Roller mappings are misaligned on the HMI 
 - [T4383](https://phabricator.apertis.org/T4383)	folks-metacontacts-antilinking: folks-metacontacts-antilinking_sh.service failed
 - [T4386](https://phabricator.apertis.org/T4386)	apparmor-tracker: AssertionError: False is not true
 - [T4392](https://phabricator.apertis.org/T4392)	apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed 
 - [T4395](https://phabricator.apertis.org/T4395)	tracker-indexing-local-storage: AssertionError: False is not true
 - [T4419](https://phabricator.apertis.org/T4419)	traprain: sadt: error: cannot find debian/tests/control
 - [T4420](https://phabricator.apertis.org/T4420)	canterbury: core-as-root and full-as-root tests failed
 - [T4421](https://phabricator.apertis.org/T4421)	ribchester: Job for generated-test-case-ribchester.service canceled
 - [T5301](https://phabricator.apertis.org/T5301)	libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test failed
 - [T5754](https://phabricator.apertis.org/T5754)	cgroups-resource-control: 3_cgroups-resource-control test failed
 - [T5989](https://phabricator.apertis.org/T5989)	frome: 6_frome test failed
 - [T5990](https://phabricator.apertis.org/T5990)	frome: 5_frome test failed

