+++
date = "2020-03-20"
weight = 100

title = "V2021dev1 ReleaseNotes"

aliases = [
    "/old-wiki/V2021dev1/ReleaseNotes"
]
+++

# Apertis v2021dev1 Release

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021dev1** is the second  **development** release of the
Apertis v2021 stable release flow that will lead to the LTS **Apertis
v2021.0** release in March 2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2021dev1 release are available in the following
test reports:

  - [APT
    images](https://qa.apertis.org/report/v2021dev1/20200312.0)
  - [OSTree
    images](https://qa.apertis.org/report/v2021dev1/20200312.0/ostree)
  - [NFS
    artifacts](https://qa.apertis.org/report/v2021dev1/20200312.0/nfs)
  - [LXC
    containers](https://qa.apertis.org/report/v2021dev1/20200312.0/lxc)

## Release flow

  - 2019 Q4: v2021dev0
  - **2020 Q1: v2021dev1**
  - 2020 Q2: v2021dev2
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7

### Release downloads

| [Apertis v2021dev1 images](https://images.apertis.org/release/v2021dev1/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2021dev1/v2021dev1.0/amd64/minimal/apertis_v2021dev1-minimal-amd64-uefi_v2021dev1.0.img.gz) | [target](https://images.apertis.org/release/v2021dev1/v2021dev1.0/amd64/target/apertis_v2021dev1-target-amd64-uefi_v2021dev1.0.img.gz) | [base SDK](https://images.apertis.org/release/v2021dev1/v2021dev1.0/amd64/basesdk/apertis_v2021dev1-basesdk-amd64-sdk_v2021dev1.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2021dev1/v2021dev1.0/amd64/sdk/apertis_v2021dev1-sdk-amd64-sdk_v2021dev1.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev1/v2021dev1.0/armhf/minimal/apertis_v2021dev1-minimal-armhf-uboot_v2021dev1.0.img.gz) | [target](https://images.apertis.org/release/v2021dev1/v2021dev1.0/armhf/target/apertis_v2021dev1-target-armhf-uboot_v2021dev1.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev1/v2021dev1.0/arm64/minimal/apertis_v2021dev1-minimal-arm64-uboot_v2021dev1.0.img.gz)

The Intel `minimal` and `target` images are tested on the [reference hardware
(MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ), but they can
run on any UEFI-based x86-64 system. The `sdk` image is [tested under
VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021dev1 repositories

    deb https://repositories.apertis.org/apertis/ v2021dev1 target development sdk hmi

## New features

### Secure boot on i.MX6 SABRE Lite technology preview

A first iteration of trusted boot on the i.MX6 SABRE Lite boards is now part of
the minimal and target ARM 32-bit OSTree images.

Based on the [High Assurance Boot v4
(HABv4)](https://community.nxp.com/docs/DOC-105193) functionality embedded in
the the boot ROM, the current implementation verifies the trust chain of the
bootloader, the kernel, the dtb and the initramfs. At the moment no checks
are done after that to validate the root filesystem and its contents, including
kernel modules loaded on-demand.

The `u-boot` shipped in the [`mx6qsabrelite-uboot` installer
image](https://images.apertis.org/release/v2021dev1/v2021dev1.0/installer/mx6qsabrelite-uboot/)
and the FIT images containing the kernel, dtb and initramfs on the minimal and
target `armhf` images are all signed with the [Apertis development
keys](https://gitlab.apertis.org/infrastructure/apertis-imx-srk).

Extensive documentation is available covering the
[implementation]( {{< ref "secure-boot.md#apertis-secure-boot-implementation-steps" >}} )
and the steps to be taken to [prepare a
device]( {{< ref "secure-boot.md#sabrelite-secure-boot-preparation" >}} )
for testing the secure boot functionality.

### Touch input on the reference i.MX6 SABRE Lite setup

The v2021dev1 release ships the latest 5.4.13 kernel and enables the
Egalax driver to be able to handle input from the reference LVDS
touchscreen for the i.MX6 SABRE Lite boards when using the HMI on the
target images.

## Build and integration

### Docker image generation on GitLab CI

The Apertis Docker images are now being [produced from GitLab CI
pipelines](https://gitlab.apertis.org/infrastructure/apertis-docker-images/-/merge_requests/87)
rather than from Jenkins.

The more integrated solution offered by GitLab provides better access controls,
easier maintenance of the published images, and the ability to run the pipeline
on development branches and merge request, before hitting the main branches.

### GitLabCI-based image building pipeline technology preview

This release marks the beginning of the conversion of the big Jenkins-based
image build pipeline to GitLab CI with the goals of converging to a single,
easy to use Continuous Integration solution and to be able to transparently
use autoscaled cloud workers instead of dedicated machines.

The implementation is based on a set of [YAML
templates](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/tree/87c3caf3)
for the [image building pipeline
itself](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/blob/87c3caf3/.gitlab-ci.yml)
combined with a [new backend for
Debos/Fakemachine](https://github.com/go-debos/fakemachine/tree/wip/uml) based
on [User Mode Linux](http://user-mode-linux.sourceforge.net/) to get the needed
performance even on cloud workers where KVM-based nested virtualization is
not available.

The official switch of the main pipeline is expected to happen during the
v2021dev2 release cycle.

### Flatpak runtime and application build pipeline technology preview

The GitLab CI infrastructure has been the basis for the first iteration of the
[pipeline building a Flatpak runtime and Flatpak
application](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-demo)
based on Apertis.

As described in the [Application
Framework]( {{< ref "application-framework.md#high-level-implementation-plan-for-the-nextgeneration-apertis-application-framework" >}} )
document, Apertis plans to adopt Flatpak as the reference application
deployment mechanism.

## Documentation

### www.apertis.org website refresh

The main Apertis website [www.apertis.org](https://www.apertis.org) got
converted from Mediawiki to a static website usng the Hugo rendering engine
and GitLab CI pipelines.

Being a static website, [www.apertis.org](https://www.apertis.org) is now
much faster and more secure than before.

A visual refresh has been also part of the transition, making the website
more modern and pleasant. Some contents have also been reorganized and
revamped, in an ongoing effort to keep them up to date and to make them easier
to navigate.

### Long term reproducibility

A new document addresses the need to [reproduce builds in the long
term]( {{< ref "long-term-reproducibility.md" >}} ),
helping product teams during the whole lifecycle of their products.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

#### FIT support is mandatory for the ARM 32-bit OSTree images

The secure boot implementation of the i.MX6 SABRE Lite boards requires the
kernel, initramfs, and DTB to be packed in a [FIT
image](https://gitlab.denx.de/u-boot/u-boot/-/blob/master/doc/uImage.FIT/source_file_format.txt).

Unfortunately versions of `u-boot` prior to 2018.11+dfsg-2co2 as shipped by
Apertis lack the support for FIT images, so they are unable to boot the
new v2021dev1 ARM 32-bit images.

To update the copy of `u-boot` on the SPI onboard flash memory of your i.MX6
SABRE Lite board with a version which is able to boot the new images it is
sufficient to boot the [`mx6qsabrelite-uboot` installer
image](https://images.apertis.org/release/v2021dev1/v2021dev1.0/installer/mx6qsabrelite-uboot/)
on the device.

#### Docker images are now hosted on the Apertis GitLab Docker registry

With the switch to GitLab CI for the generation of the Apertis Docker images
they are now published on the registry integrated in the Apertis GitLab
instance, `registry.gitlab.apertis.org`.

Images for older releases are still available on the
`docker-registry.apertis.org` registry, but newer releases will only
publish their Docker images at `registry.gitlab.apertis.org`.

Image references such as
`docker-registry.apertis.org/apertis/apertis-v2021dev1-image-builder` will need
to be updated to point at
`registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev1-image-builder`.

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

  deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at:

   https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues


