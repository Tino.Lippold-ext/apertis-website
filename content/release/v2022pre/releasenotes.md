+++
date = "2021-09-15"
weight = 100

title = "v2022pre Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022pre** is the **preview** release of the Apertis
v2022 stable release flow that will lead to the LTS **Apertis v2022.0**
release in March 2022.

This Apertis release is based on Debian Bullseye. It currently
ships with the Linux kernel 5.15.x LTS series but later releases in the
v2022 channel will track newer kernel versions up to the next LTS scheduled at
the end of 2021.

Test results for the v2022pre release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022pre/20211201.1511/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022pre/20211201.1511/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022pre/20211201.1511/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022pre/20211201.1511/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - **2021 Q4: v2022pre**
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022dev3 images](https://images.apertis.org/release/v2022pre/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022pre/v2022pre.0/amd64/fixedfunction/apertis_ostree_v2022pre-fixedfunction-amd64-uefi_v2022pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2022pre/v2022pre.0/amd64/hmi/apertis_ostree_v2022pre-hmi-amd64-uefi_v2022pre.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022pre/v2022pre.0/amd64/basesdk/apertis_v2022pre-basesdk-amd64-sdk_v2022pre.0.ova) | [SDK](https://images.apertis.org/release/v2022pre/v2022pre.0/amd64/sdk/apertis_v2022pre-sdk-amd64-sdk_v2022pre.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022pre/v2022pre.0/armhf/fixedfunction/apertis_ostree_v2022pre-fixedfunction-armhf-uboot_v2022pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2022pre/v2022pre.0/armhf/hmi/apertis_ostree_v2022pre-hmi-armhf-uboot_v2022pre.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022pre/v2022pre.0/arm64/fixedfunction/apertis_ostree_v2022pre-fixedfunction-arm64-uboot_v2022pre.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022pre/v2022pre.0/arm64/fixedfunction/apertis_ostree_v2022pre-fixedfunction-arm64-rpi64_v2022pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2022pre/v2022pre.0/arm64/hmi/apertis_ostree_v2022pre-hmi-arm64-rpi64_v2022pre.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022pre repositories

    deb https://repositories.apertis.org/apertis/ v2022pre target development sdk

## New features

### Updates from Debian Bullseye

This release is the second one based on Debian Bullseye, bringing plenty of
upstream updates and fixes.

### The reference Flatpak Runtime for Apertis

Starting from the Apertis demo introduced in the v2022dev2 release, the runtime has been refined and polished to provide the [reference `org.apertis` runtime](https://gitlab.apertis.org/infrastructure/apertis-flatpak-runtime) that application authors can use to distribute their Apertis-based applications on top of any Flatpak-supporting system.

Two variants are provided:
- `org.apertis.headless.Platform` and `org.apertis.headless.Sdk`: A basic
  runtime with some common libraries that headless applications may use.
- `org.apertis.hmi.Platform` and `org.apertis.hmi.Sdk`: A larger runtime for
  graphical applications, based on the headless one.

The ["Creating Flatpak Runtimes and Applications]({{< ref "guides/flatpak.md" >}}) guide covers in detail how the runtimes are built, how custom runtimes can be created and extended and how to build applications using them.

### Apply and enforce Ed25519 signatures for Apertis Flatpak packages and runtimes

After the introduction of Ed25519 signatures in OSTree in the v2020dev0 release, Apertis has now extended the support of Ed25519 signatures to Flatpak.

This adds a modern signature mechanism that is compliant with the [Apertis Licensing Expectations]({{< ref "license-expectations.md" >}}) to ensure that all the Apertis Flatpak applications and runtimes deployed on a system come from a trusted source.

### Audio policy to mute song app when switching to video app

Improvements in Wireplumber now allows system integrator and developer to support
additional use cases, such as mute audio from a song application when switching to
a video application.

A demonstrative policy is now bundled in the Apertis images, providing a starting point for teams interested in defining how their products should manage their audio streams.

### Improved license compliance checks

The introduction of the license checks and reporting in the v2022dev1 release did start the effort to ensure that all packages in the `target` component had the appropriate source licensing metadata.

With the steady progress over the quarters thanks to the patient and accurate work of the Apertis maintainers in reviewing the many inconsistencies identified, this effort now comes to completion, significantly improving the quality of the bill-of-materials produced by the image building pipelines.

### Improved availability of packaging tools for developers

The scrips and tools used by developers when performing packaging activities have been reorganized and are now shipped as part of the SDK, improving the development experience.

### BSH SMM S2 PRO board support

Designed by BSH, the [SystemMaster S2 Pro]({{< ref "imx8mn_bsh_smm_s2pro_setup.md"  >}}) is an add-on board which provides
input and output interfaces to a dedicated carrier board. Apertis users can now
run a headless system using the `fixedfunction` image, which includes hardware 
support for wifi, bluetooth and audio. Check the [setup notes]({{< ref imx8mn_bsh_smm_s2pro_setup.md  >}}) for more info.

## Build and integration

### Scalable monitoring of LAVA jobs from GitLab pipelines

Apertis relies heavily on [LAVA](https://lavasoftware.org/) to continuously test our deployable artifacts. Every night hundreds of LAVA jobs get submitted to the devices in our LAVA farm from the daily image building pipelines; until now, the submission was done with a fire-and-forget approach since the standard GitLab runners do not cope efficiently with monitoring jobs managed by a downstream scheduler like LAVA.

The introduction of a [custom GitLab runner dedicated to monitoring LAVA jobs](https://gitlab.collabora.com/lava/lava-gitlab-runner) enables the image building pipelines to keep track of all the submitted jobs efficiently.

Developers can now see the results for each jobs and retrieve the logs directly in the same GitLab pipeline that submitted them, helping them to keep track of the impact of their changes in a close feedback loop.

### Upstream Linux LTS version tracking from the packaging dashboard

The [Apertis dashboard](https://infrastructure.pages.apertis.org/dashboard/) is the tool that the project uses to stay up-to-date with its upstream distribution, Debian.

This works well for the near totality of our packages, with the Linux kernel being the major exception: accordingly to the [Apertis release flow]({{< ref "release-flow.md" >}}) for each new stable release we track the latest upstream LTS version, which means that we are rarely in-sync with the version shipped in Debian.

The dashboard is now able to find available updates directly from upstream in those cases, helping the Apertis maintainer to ensure we always ship the most up-to-date fixes to our users.

### Tracking consumption of APT snapshots for long-term reproducibility

The approach Apertis took for [Long-Term Reproducibility]({{< ref "long-term-reproducibility.md" >}}) involves frequent snapshots of the APT repositories.

While snapshots themselves are rather cheap thanks to the design of the APT repository format, their accumulation over time quickly becomes a substantial cost.

The Apertis dashboard has now a dedicated section to [track the storage consumption of the snapshots over time](https://infrastructure.pages.apertis.org/dashboard/storage.html), to give maintainer visibility on which releases are consuming the most space, which snapshots carry the most weight and how much space can be reclaimed by their deletion.

### Raspberry Pi images tested on LAVA

The Apertis images for Rasberry Pi are now included in the tests run on LAVA paving the way to the official suport of this board.

## Documentation and designs

### Export control

Apertis targets a global community developing global products, and this necessarily makes it interact with the legislation regulating the export of goods, software and technology. 

The [Export controls]({{< ref "export-controls.md">}}) concept document tries to address the requirements and challenges that teams targeting global markets will face, providing a high level  overview of the tools and workflows that Apertis can provide to make such compliance processes easier for them.

### Virtual keyboard

The Apertis team has surveyed the [On-Screen Virtual Keyboard]({{< ref "on-screen-keyboard.md">}}) state-of-the-art in the Wayland ecosystem to evaluate the viable options.

Due to licensing challenges and the in-flux state of the specifications no immediate winner emerged from the evaluation, but a strategy has been defined to address this need in the near future.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaking changes are part of this release.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022pre-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High (22)
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T7984](https://phabricator.apertis.org/T7984)	Rhosydd test shows status as incomplete on v2021 images
 - [T8009](https://phabricator.apertis.org/T8009)	Flatpak: sdk-flatpak-build-helloworld-app test fails on v2022dev2 sdk / base sdk
 - [T8076](https://phabricator.apertis.org/T8076)	ade-commands: test failed
 - [T8132](https://phabricator.apertis.org/T8132)	sdk-flatpak-demo: test failed
 - [T8179](https://phabricator.apertis.org/T8179)	ADE: root:Project is not configured : error when executing sdk-ade-remote-debugging test on v2022dev3 SDK/Base SDK
 - [T8288](https://phabricator.apertis.org/T8288)	HMI Videos app closing automatically on armhf v2022 images
 - [T8360](https://phabricator.apertis.org/T8360)	Arm64 hmi ostree image not available for offline system-update tests on v2022
 - [T8363](https://phabricator.apertis.org/T8363)	Videos are not loaded in Videos app of SDK/target of v2022pre
 - [T8364](https://phabricator.apertis.org/T8364)	aum-ota-rollback-blacklist: test failed
 - [T8382](https://phabricator.apertis.org/T8382)	Connman and other tests show as incomplete on v2020 for ARM
 - [T8399](https://phabricator.apertis.org/T8399)	aum-offline-upgrade-branch: test failed
 - [T8409](https://phabricator.apertis.org/T8409)	aum-rollback-blacklist: test failed
 - [T8434](https://phabricator.apertis.org/T8434)	Switch between different graphical applications doesn't work
 - [T8436](https://phabricator.apertis.org/T8436)	apparmor-pipewire: test failed
 - [T8440](https://phabricator.apertis.org/T8440)	agl-compositor : "PulseAudio Volume Control", application icon not visible, shows a blank screen on v2021.3rc1
 - [T8445](https://phabricator.apertis.org/T8445)	apparmor-functional-demo: test failed
 - [T8448](https://phabricator.apertis.org/T8448)	U-Boot 2021.10 fails to boot ostree images
 - [T8449](https://phabricator.apertis.org/T8449)	dbus-dos-reply-time: test failed
 - [T8454](https://phabricator.apertis.org/T8454)	aum-offline-upgrade: test failed
 - [T8456](https://phabricator.apertis.org/T8456)	aum-api: test failed
 - [T8464](https://phabricator.apertis.org/T8464)	aum-ota-auto: test failed

### Normal (68)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails 
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7827](https://phabricator.apertis.org/T7827)	When creaing new test repositories `common-subtree.sh add` gets confused if the checked out branch does not exists in `tests/common`
 - [T7843](https://phabricator.apertis.org/T7843)	Package node-grunt-legacy-log fails to build in  OBS due to environment variables
 - [T7852](https://phabricator.apertis.org/T7852)	v2022dev2: Investigate test failure TestGetSourceMount
 - [T7854](https://phabricator.apertis.org/T7854)	v2022dev2: FTBFS for package clisp in test streams.tst
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7904](https://phabricator.apertis.org/T7904)	Failed unmounting /var message appears when v2022dev2 AMD64 Minimal OSTree image is shutdown 
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails in v2022dev2
 - [T7964](https://phabricator.apertis.org/T7964)	assertion 'G_IS_DBUS_PROXY (proxy)' failed errors seeing on executing apertis-update-manager-ota-rollback test
 - [T7977](https://phabricator.apertis.org/T7977)	v2022dev2 Persistent SDK tests fail
 - [T8029](https://phabricator.apertis.org/T8029)	Unable to apply few of the gitlab-rulez for projects hosted on Gitlab
 - [T8058](https://phabricator.apertis.org/T8058)	Rhosydd test fails to launch  rhosydd-speedo-backend.service
 - [T8069](https://phabricator.apertis.org/T8069)	Sound/Audio is not heard during the very first time when running the webkit2gtk-gstreamer1.0 test 
 - [T8074](https://phabricator.apertis.org/T8074)	Error wifi: Message recipient disconnected from message bus without replying observed on v2022dev3 images
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8255](https://phabricator.apertis.org/T8255)	glib-gio-fs: test failed
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8377](https://phabricator.apertis.org/T8377)	gupnp-services: test failed
 - [T8411](https://phabricator.apertis.org/T8411)	tiny-container-system-connectivity-profile: test failed
 - [T8412](https://phabricator.apertis.org/T8412)	tiny-container-user-connectivity-profile: test failed
 - [T8423](https://phabricator.apertis.org/T8423)	apparmor-gstreamer1-0: test failed
 - [T8435](https://phabricator.apertis.org/T8435)	"Add 'reserved-memory' node failed: FDT_ERR_EXISTS" log during boot
