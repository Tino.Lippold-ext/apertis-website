+++
date = "2022-09-05"
weight = 100

title = "v2022.2 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.2** is the third **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.2 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022/20220831.0116/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022/20220831.0116/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022/20220831.0116/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022/20220831.0116/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - **2022 Q3: v2022.2**
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.2 images](https://images.apertis.org/release/v2022/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.2/amd64/fixedfunction/apertis_ostree_v2022-fixedfunction-amd64-uefi_v2022.2.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.2/amd64/hmi/apertis_ostree_v2022-hmi-amd64-uefi_v2022.2.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.2/amd64/basesdk/apertis_v2022-basesdk-amd64-sdk_v2022.2.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.2/amd64/sdk/apertis_v2022-sdk-amd64-sdk_v2022.2.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.2/armhf/fixedfunction/apertis_ostree_v2022-fixedfunction-armhf-uboot_v2022.2.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.2/armhf/hmi/apertis_ostree_v2022-hmi-armhf-uboot_v2022.2.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.2/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-uboot_v2022.2.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.2/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-rpi64_v2022.2.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.2/arm64/hmi/apertis_ostree_v2022-hmi-arm64-rpi64_v2022.2.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022.2 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 5.15.x series.

In this release the support for the new [UP Squared 6000 board]({{< ref "amd64.md" >}}) has been
included to replace the not longer available MinnowBoard Turbot E3826/E3845. Additionally,
LAVA support has also been added in order to run automated tests using the new reference board.

Finally, in an effort to allow developers to check that their changes follow Debian's best practices,
Apertis added a lintian step in its CI with a custom profile. This results in a better developer
experience, specially for those new in Apertis.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.com/project/show/apertis:infrastructure:v2022)
provides packages for the required versions in Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #70](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/70) Follow up FS corruption issue
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #129](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/129) aum-ota-rollback-blacklist: test failed
- [Issue #130](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/130) aum-rollback-blacklist: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #151](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/151) sdk-debos-image-building: test failed
- [Issue #155](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/155) aum-offline-upgrade-branch: test failed
- [Issue #178](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/178) sanity-check: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #8](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/8) arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #17](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/17) SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #26](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/26) newport: test failed
- [Issue #28](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/28) Investigate test failure TestGetSourceMount
- [Issue #30](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/30) Error building package ruby-redis on OBS
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #48](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/48) Test apparmor-chaiwala-system does not work properly on OSTree images
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #62](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/62) apparmor-ofono: test failed
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #119](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/119) tiny-container-user-connectivity-profile: test failed
- [Issue #138](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/138) gstreamer1-0-decode: test failed
- [Issue #153](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/153) apparmor-pipewire: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #162](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/162) phab-gitlab-bridge duplicate bugs on Phabricator

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #40](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/40) evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
- [Issue #93](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/93) "Error: GDBus.Error:org.freedesktop.DBus.Error.InvalidArgs" logs seen during bluez-phone test
