+++
date = "2022-11-28"
weight = 100

title = "v2022.3 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.3** is the fourth **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022/20221116.1415/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022/20221116.1415/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022/20221116.1415/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022/20221116.1415/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.3
  - **2022 Q4: v2022.3**
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.3 images](https://images.apertis.org/release/v2022/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.3/amd64/fixedfunction/apertis_ostree_v2022-fixedfunction-amd64-uefi_v2022.3.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.3/amd64/hmi/apertis_ostree_v2022-hmi-amd64-uefi_v2022.3.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.3/amd64/basesdk/apertis_v2022-basesdk-amd64-sdk_v2022.3.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.3/amd64/sdk/apertis_v2022-sdk-amd64-sdk_v2022.3.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.3/armhf/fixedfunction/apertis_ostree_v2022-fixedfunction-armhf-uboot_v2022.3.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.3/armhf/hmi/apertis_ostree_v2022-hmi-armhf-uboot_v2022.3.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.3/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-uboot_v2022.3.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.3/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-rpi64_v2022.3.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.3/arm64/hmi/apertis_ostree_v2022-hmi-arm64-rpi64_v2022.3.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022.3 package list

The full list of packages available from the v2022.3 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2022.3](https://infrastructure.pages.apertis.org/dashboard/tsv/)

#### Apertis v2022.3 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 5.15.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.com/project/show/apertis:infrastructure:v2022)
provides packages for the required versions in Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #126](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/126) aum-offline-upgrade-signed: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #155](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/155) aum-offline-upgrade-branch: test failed
- [Issue #169](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/169) aum-out-of-space: test failed
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #203](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/203) aum-ota-rollback-blacklist: test failed
- [Issue #204](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/204) aum-rollback-blacklist: test failed
- [Issue #205](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/205) devhelp page shows empty data on v2023dev3 SDK
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #214](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/214) iMX.6: u-boot has a misconfigured mmc order in v2023pre
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up FS corruption issue - part 2
- [Issue #217](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/217) AUM out of space tests fail on UP Squared 6000 board
- [Issue #218](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/218) agl-compositor not showing on UP SQUARED 6000 board setup of lilliput display

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #8](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/8) arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #17](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/17) SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #26](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/26) newport: test failed
- [Issue #30](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/30) Error building package ruby-redis on OBS
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #48](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/48) Test apparmor-chaiwala-system does not work properly on OSTree images
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #62](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/62) apparmor-ofono: test failed
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #119](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/119) tiny-container-user-connectivity-profile: test failed
- [Issue #154](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/154) sdk-cross-compilation: test failed
- [Issue #193](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/193) flatpak-run-demo-cli-app: test failed
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #210](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/210) tiny-container-system-folder-sharing: test failed
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #40](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/40) evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
- [Issue #93](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/93) "Error: GDBus.Error:org.freedesktop.DBus.Error.InvalidArgs" logs seen during bluez-phone test
