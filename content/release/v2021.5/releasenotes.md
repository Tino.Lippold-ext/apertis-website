+++
date = "2022-06-03"
weight = 100

title = "v2021.5 Release notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021.5** is the sixth **stable** release of the Apertis v2021
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2021 release stream up to the end
of 2022.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 5.10.x LTS series.

Test results for the v2021.5 release are available in the following test
reports:

  - [APT     images](https://qa.apertis.org/report/v2021/20220525.1900/apt)
  - [OSTree  images](https://qa.apertis.org/report/v2021/20220525.1900/ostree)
  - [NFS  artifacts](https://qa.apertis.org/report/v2021/20220525.1900/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2021/20220525.1900/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2021.7.

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - 2020 Q2: v2021dev2
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - **2022 Q2: v2021.5**
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7


### Release downloads

| [Apertis v2021.5 images](https://images.apertis.org/release/v2021/v2021.5/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2021/v2021.5/amd64/minimal/apertis_v2021-minimal-amd64-uefi_v2021.5.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.5/amd64/target/apertis_v2021-target-amd64-uefi_v2021.5.img.gz) | [base SDK](https://images.apertis.org/release/v2021/v2021.5/amd64/basesdk/apertis_v2021-basesdk-amd64-sdk_v2021.5.vdi.gz) | [SDK](https://images.apertis.org/release/v2021/v2021.5/amd64/sdk/apertis_v2021-sdk-amd64-sdk_v2021.5.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.5/armhf/minimal/apertis_v2021-minimal-armhf-uboot_v2021.5.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2021/v2021.5/arm64/minimal/apertis_v2021-minimal-arm64-uboot_v2021.5.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2021/v2021.5/arm64/minimal/apertis_v2021-minimal-arm64-rpi64_v2021.5.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.5/arm64/target/apertis_v2021-target-arm64-rpi64_v2021.5.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021 package list

The full list of packages available from the v2021 APT repositories is available here in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2021](https://infrastructure.pages.apertis.org/dashboard/tsv/)


#### Apertis v2021 repositories

    deb https://repositories.apertis.org/apertis/ v2021 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2021-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Buster and the latest
LTS Linux kernel on the 5.10.x series.

## Regressions

## Deprecations and ABI/API breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[apertis-base](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-base),
[apertis-image-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-image-builder),
[apertis-package-source-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-package-source-builder),
[apertis-flatdeb-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-flatdeb-builder),
[apertis-documentation-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-documentation-builder),
and [apertis-testcases-builder](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure repository](https://build.collabora.co.uk/project/show/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure


### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### Normal (39)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7852](https://phabricator.apertis.org/T7852)	Investigate test failure TestGetSourceMount
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8504](https://phabricator.apertis.org/T8504)	LAVA/Phab bridge timeouts
 - [T8524](https://phabricator.apertis.org/T8524)	grilo: test failed
 - [T8572](https://phabricator.apertis.org/T8572)	Missing warning on coreutils overwrite
 - [T8622](https://phabricator.apertis.org/T8622)	Manual testcase results should not have any hyperlink in the LavaPhabridge report page
 - [T8629](https://phabricator.apertis.org/T8629)	frome: test failed
 - [T8668](https://phabricator.apertis.org/T8668)	Test apparmor-chaiwala-system does not work properly on OSTree images
 - [T8683](https://phabricator.apertis.org/T8683)	"firmware: failed to load" logs seen during boot
 - [T8824](https://phabricator.apertis.org/T8824)	The apertis-flatdeb-mildenhall pipeline is flaky
 - [T8841](https://phabricator.apertis.org/T8841)	apparmor-ofono: test failed
 - [T8860](https://phabricator.apertis.org/T8860)	Copyrights in the image licenses.json file contain entries with newlines
 - [T8874](https://phabricator.apertis.org/T8874)	The firefox-esr package is being failed on OBS
 - [T8875](https://phabricator.apertis.org/T8875)	Terminal at xfce desktop corrupted after debos call

