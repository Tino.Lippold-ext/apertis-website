+++
date = "2019-04-30"
weight = 100

title = "V2019dev0 ReleaseNotes"

aliases = [
    "/old-wiki/V2019dev0/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis v2019dev0 Release

Apertis a Debian derivative distribution geared towards the creation of
product-specific images for ARM (both the 32bit ARMv7 and 64-bit ARMv8
versions using the hardfloat ABI) and Intel x86-64 (64-bit) systems.

**Apertis v2019dev0** is the first **development** release of the
Apertis v2019 release flow that will lead to the LTS **Apertis v2019.0**
release in September 2019.

This Apertis release is the first one to be based on top Debian Buster
with several customizations. Test results for the v2019dev0 release are
available in the [v2019dev0 test report]().

### Release downloads

| [Apertis v2019dev0 images]() |
| ------------------------------------------------------------------------- |
| Intel 64-bit                                                              |
| ARM 32-bit (U-Boot)                                                       |
| ARM 64-bit (U-Boot)                                                       |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2019dev0 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` v2019dev0 target helper-libs development sdk hmi`

## Highlights

### Yearly release flow

From now on, Apertis has moved to a yearly-based release schedule to
provide a more stable and well defined base for product development.

Every quarter a new development release is published, leading to a
yearly stable release released in Q1 with a default support period of 7
quarters.

To transition to the new release flow the first two cycles have been
shortened, with a reduced number of development releases along the
following schedule:

  - 2019 Q1: v2019dev0
  - 2019 Q2: v2019pre
  - 2019 Q3: v2019.0 + v2020dev0
  - 2019 Q4: v2019.1 + v2020pre
  - 2020 Q1: v2019.2 + v2020.0

The stable releases are intended to both be based on the most recent
mainline kernel LTS release and the current Debian stable release. Since
Debian releases roughly once every two years, that means that there will
typically be two Apertis product releases based on a single Debian
stable release. With Linux doing an LTS release on a yearly basis, each
Apertis product release will be based on a different (and then current)
Linux kernel release.

### Rebase to Debian 10 "Buster"

Past releases of Apertis were based on the Ubuntu 16.04 (Xenial) LTS
release but with this release Apertis has rebased directly on top of the
upcoming [Debian 10 "Buster"](https://wiki.debian.org/DebianBuster)
release.

Debian now offers a longer support period comparable to what Ubuntu
provides, and projects like the [Civil Infrastructure Platform
partnership](https://www.cip-project.org/announcement/2018/06/19/civil-infrastructure-platform-announces-collaboration-with-the-debian-lts-initiative-and-welcomes-cybertrust-as-a-new-member)
aim at extending such period even further.

With the rebase the kernel has been upgraded to the Linux kernel 4.19 to
bring new features and hardware support, covered by the [Super Long Term
Support
effort](https://www.linuxfoundation.org/press-release/2019/02/civil-infrastructure-platform-announces-new-super-long-term-support-kernel-that-advances-automation-machine-learning-and-artificial-intelligence/)
as well.

## New features

### Generic U-Boot images for ARM 32bit

The ARM 32bit images targeting the i.MX6 SABRE Lite boards now share the
same generic recipe as the ARM 64bit images and can be booted on any ARM
32bit device that provides the needed U-Boot support.

The new images no longer ship the bootloader and instead assume that
U-Boot is installed on-board flash storage.

Current devices thus need to be prepared before being able to boot the
`v2019dev0` images, see the [Change to storage location of U-Boot on
NXP SABRE
Lite](#change-to-storage-location-of-u-boot-on-nxp-sabre-lite)
section for more details on the migration.

### Automatic offline updates over USB Mass Storage devices with rollback

A [technology preview in the 18.12
release]( {{< ref "/release/18.12/releasenotes.md#offline-updates-over-usb-mass-storage-devices" >}} ),
the OSTree-based automatic updater is now fully part of the release.

Software updates for the base OS can be deployed on USB keys that, once
plugged to the device, automatically trigger the update process. Also
included is the ability to automatically roll back to the previous
version if booting the new one fails for any reason.

During this cycle the testcase covering the update process have been
automated and are part of the daily LAVA runs, ensuring that no
regressions are introduced during development.

## Build and integration

### Improved reusability of the image recipes

A new [`recipe`
action](https://godoc.org/github.com/go-debos/debos/actions#hdr-Recipe_Action)
has been introduce in Debos, allowing for recipes to include other
recipes.

Product-specific recipes can now use the newly introduced feature to
include more generic recipes like the Apertis reference recipes, and in
the future the same mechanism will be used to reduce the duplication in
the Apertis reference recipes themselves.

## Quality Assurance

### Automated test report generation

The test results from the daily automated LAVA runs are now collected in
a database and a web application [renders reports for each
run](https://qa.apertis.org/), sending notifications to
Mattermost and Phabricator.

Previously the generation of reports required a significant effort as it
was entirely manual, so it was done only once a week. Now results for
every run of the image building pipeline are captured automatically and
made available to developers, significantly improving the feedback loop
for developers and managers.

## Design and documentation

### Sample image recipes

A set of sample image recipes are now provide with the main reference
one to provide a smoother learning experience to newcomers and also
showcase how product teams can re-use the reference recipes for their
needs.

### Vehicle API documentation improvements

The internals of the service implementing the [Sensors and
actuators]( {{< ref "sensors-and-actuators.md" >}} )
design, Rhosydd, have been reworked to provide up-to-date, automatically
generated [documentation for its D-Bus
API](),
complementing the documentation for the C libraries.

The *Sensors and actuators* has been then updated to [describe the
currently implemented
API]( {{< ref "sensors-and-actuators.md#approach" >}} )
rather than the initial draft.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester` and `canterbury` are deprecated

<dd>

From the next release Canterbury and Ribchester are deprecated as is the
[Application
Layout]( {{< ref "application-layout.md" >}} )
document in favour of Flatpak.

</dl>

### Breaks

#### Apertis infrastructure tools repository

<dl>

<dt>

The Apertis infrastructure repositories replaces the Apertis tools
repository

<dd>

The repository containing the extra packages needed on the Apertis
infrastructure hosts is now managed differently and is now available via
a new APT source line:

` deb `<https://repositories.apertis.org/infrastructure-v2019/>` buster infrastructure`

The new repository now target a specific Apertis yearly release to
decouple the tools used to build each of the, preventing new updates to
cause regressions when building earlier Apertis releases.

See also the "[Apertis infrastructure
tools](#apertis-infrastructure-tools)" section for more
details about the contents of the repository.

</dl>

#### Change to storage location of U-Boot on NXP SABRE Lite

<dl>

<dt>

U-Boot moved from SD Card to serial EEPROM

<dd>

The decision has been made to boot the SABRE Lite from U-Boot stored in
the EEPROM rather than from the SD Card via a chainloader in the EEPROM.
This has the advantage of allowing the SABRE Lite to boot from a common
armhf SD Card image, rather than one tailored specifically for the SABRE
Lite and aligns with the boot process used on ARM64 targets.

A custom image (called `uboot-v2019dev0-installer-mx6qsabrelite.img.gz`)
is supplied with the v2019dev0 release that will flash U-Boot to the
EEPROM on SABRE Lite. This should be run on SABRE Lite boards that will
be used with v2019dev0.

To enable earlier versions of Apertis to be run, boot to the v2019dev0
U-Boot prompt and run `bmode mmc0`. This will cause the SABRE Lite to
boot from the SD Card until the board is power cycled.

</dl>

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-testcases-builder` and
`apertis-documentation-builder` Docker images.

### Apertis infrastructure tools

The [Apertis v2019 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2019)
provides packages for `git-phab`, `lqa`, Debos and deps, LAVA v2, OBS
2.7, `ostree-push` and `jenkins-job-builder` for Debian Buster:

` deb `<https://repositories.apertis.org/infrastructure-v2019/>` buster infrastructure`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

  - All the AppArmor profiles need to be updated to cope with the
    requirements of the new base libraries. As a result many AppArmor
    profiles are not enforced for this release, but should be switched
    again to the enforced mode in the next release.
  - Auto-resizing of the SDK display in VirtualBox is known to be broken
    in some versions of VirtualBox (specifically `6.0.4_Debian`).
  - Booting the SDK in VirtualBox appears to be broken with the `6.0.4`
    VirtualBox Guest Extensions: using the `5.x` Guest Extensions even
    under VirtualBox 6.0 is recommended. This has been tested with the
    `5.1.34` guest additions. Guest additions can can be retrieved from
    the [VirtualBox download
    site](http://download.virtualbox.org/virtualbox/).
  - The `pacrunner` package used for proxy autoconfiguration is not
    available in this release due to a conflict with the available
    version of the mozjs JS interpreter

### High (15)

  - ribchester: ribchester test failed

  - ribchester: core-as-root test failed

  - Installation of keyboard-configuration breaks SDK installation

  - bluez-phone test fails

  - apparmor-dbus: 6_apparmor-dbus test failed

  - dbus-installed-tests: 15_dbus-installed-tests test failed

  - Package build fails for apertis:v2019dev0 on OBS due to Internet
    connectivity not being available

  - ade-commands: 2_ade-commands test failed

  - apparmor-dbus: 5_apparmor-dbus test failed

  - frome: Unable to find a source package for frome

  - canterbury: 3_canterbury test failed

  - Terminal comes up inside the Launcher

  - Applications are not seen when mildenhall launcher is re-launched

  - Songs/Video's dont play on clicking the same

  - i.MX6 goes into BOOTP broadcast mode on rebooting

### Normal (121)

  - sanity-check: verify that a trivial PyGI and/or gjs GUI can run

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Crash when initialising egl on ARM target

  - Develop test case for out of screen events in Wayland images

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-tracker: underlying_tests failed

  - Fix Tracker testcase to not download media files from random HTTP
    user folders

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Ensure that the arch:all packages in the archive match the
    arch-specific versions

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - gupnp-services: browsing and introspection tests fail

  - dbus-installed-tests: service failed because a timeout was exceeded

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - SDK hangs when trying to execute bluez-hfp testcase

  - OSTree testsuite hangs the gouda Jenkins build slave

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on the internal
    mx6qsabrelite images with proprietary kernel

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - Apps like songs, artists etc are not offser correctly by the
    compositor window

  - Double tapping on any empty space in the songs app's screen results
    in the compositor window hinding the left ribbon

  - Back button is hidden by the compositor window in apps like songs,
    artists etc

  - Ensure that the uploaded linux package version uniquely identifies a
    single commit

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - Mildenhall compositor gets offset when we open an app like songs

  - cgroups-resource-control: cpu-shares test failed

  - tracker-indexing-local-storage: run-test test failed

  - apparmor-pulseaudio: pulseaudio.malicious.expected test failed

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    test failed

  - apparmor-tracker: run-test-tracker test failed

  - apparmor-tracker: tracker.malicious.expected test failed

  - apparmor-tracker: tracker.malicious.expected_underlying_tests test
    failed

  - apparmor-tracker: tracker.normal.expected test failed

  - apparmor-tracker: tracker.normal.expected_underlying_tests test
    failed

  - kernel panic occurs when trying to boot an image on i.mx6 revision
    1.2 board from LAVA

  - gupnp-services: test_service_introspection test failed

  - gupnp-services: test_service_browsing test failed

  - build failure in liblightwood-2 on 18.06 image

  - newport: newport test failed

  - lava: 12_apparmor-functional-demo test failed

  - apparmor-pulseaudio:
    pulseaudio.malicious.expected_underlying_tests test failed

  - didcot: didcot test failed

  - traprain: traprain test failed

  - """Setup Install to Target"" option is not showing on eclipse "

  - Ospacks ship a .gitignore file in the root directory

  - Ospacks ship an empty /script directory in the root directory

  - rhosydd: rhosydd test failed

  - rhosydd: integration test failed

  - LAVA-Phab bridge should mark test failures as bugs

  - build-snapshot: allow to build packages without \`autogen.sh\`
    script

  - Wi-Fi search button is missing in wifi application

  - LAVA-Phab bridge does not report the release version or the board
    used of the tested image

  - traprain: gnome-desktop-testing test failed

  - apparmor-geoclue: run-test-geoclue test failed

  - LAVA-Phab bridge should post a comment once tests stop failing

  - bluez-hfp testcase fails

  - ade tests failing on lava

  - apparmor-tumbler: run-test-sh test failed

  - Default environment for U-Boot should be written

  - \`kbd\` package has broken/problematic dependencies

  - lava: read-feedback test failed

  - lava: read-feedback test failed

  - Sample applications should not be part of the base SDK

  - Seed reference manual should not be part of devhelp

  - Media files should not be part of Base SDK

  - iptables-basic: test_iptables_list test failed

  - Debos crash when a recipes embbed recipes with ostree-commit and
    ostree-deploy

  - boot-no-crashes automated testcase needs to be included in the
    weekly test report

  - Both sysroot and devroot folder is exist in /opt path

  - The /boot mountpoint is not empty

  - System users are shipped in /usr/etc/passwd instead of /lib/passwd

  - apparmor-gstreamer1-0: run-test-sh test failed

  - apparmor-pulseaudio: run-test-pulseaudio test failed

  - Fix the RFS tiny images filesystem structure

  - apparmor-folks: 5_apparmor-folks test failed

  - apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed

  - apparmor-pulseaudio: 7_apparmor-pulseaudio test failed

  - apparmor-tracker: 10_apparmor-tracker test failed

  - cgroups-resource-control: 8_cgroups-resource-control test failed

  - folks-alias-persistence: 4_folks-alias-persistence test failed

  - Chaiwala logo does not come up on bootup

  - xmessage error pop up comes up when Guestadditions is enabled on the
    sdk's

  - No test-text is displayed inside the Xephyr window.

  - Atheros/Qualcomm based bluetooth dongles are not getting detected

  - aa_get_complaints.sh script needs to be run with sudo permission

  - bluez-setup test fails

  - bluez-phone test fails on the basesdk

  - apparmor-functional-demo: 13_apparmor-functional-demo test failed

  - apparmor-geoclue: 11_apparmor-geoclue test failed

  - apparmor-pulseaudio: 8_apparmor-pulseaudio test failed

  - apparmor-tracker: 12_apparmor-tracker test failed

  - apparmor-tumbler: 9_apparmor-tumbler test failed

  - cgroups-resource-control: 6_cgroups-resource-control test failed

  - gettext-i18n: gettext-i18n test failed

  - apparmor-functional-demo: 12_apparmor-functional-demo test failed

  - apparmor-geoclue: 10_apparmor-geoclue test failed

  - apparmor-tracker: 11_apparmor-tracker test failed

  - apparmor-tumbler: 8_apparmor-tumbler test failed

  - apparmor-functional-demo: apparmor-functional-demo test failed

  - apparmor-geoclue: apparmor-geoclue test failed

  - apparmor-tracker: apparmor-tracker test failed

  - apparmor-utils: apparmor-utils test failed

  - gettext-i18n: 11_gettext-i18n test failed

  - tracker-indexing-local-storage: 14_tracker-indexing-local-storage
    test failed

  - GuestAdditions fails to launch full screen in the first attempt

  - dbus-installed-tests: trying to overwrite mktemp.1.gz

  - telepathy-ring-automated: Unable to locate package telepathy-phoenix

### Low (127)

  - Remove unnecessary folks package dependencies for automated tests

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - telepathy-ring: Review and fix SMS test

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - apparmor-folks: unable to link contacts to test unlinking

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - folks: random tests fail

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-folks: Several tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - didcot: test_open_uri: assertion failed

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - webkit2gtk-drag-and-drop doesn't work with touch

  - folks-alias-persistence: folks-alias-persistence test failed

  - folks-extended-info: folks-extended-info test failed

  - folks-metacontacts: folks-metacontacts-unlinking test failed

  - folks-metacontacts: folks-metacontacts-linking test failed

  - folks-metacontacts-antilinking: folks-metacontacts-antilinking test
    failed

  - folks-search: folks-search-contacts test failed

  - apparmor-folks: run-test-folks test failed

  - apparmor-folks: R1.13b_folks-metacontacts-unlinking test failed

  - apparmor-folks: R1.13b_folks-metacontacts-linking test failed

  - apparmor-folks: R1.13b_folks-alias-persistence test failed

  - apparmor-folks: R6.4.2_folks-metacontacts-antilinking test failed

  - frome: frome test failed

  - frome: gnome-desktop-testing test failed

  - libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test
    failed

  - folks-telepathy: folks-retrieve-contacts-telepathy test failed

  - apparmor-folks: R1.13b_folks-retrieve-contacts-telepathy test
    failed

  - folks-metacontacts-unlinking: folks-metacontacts-unlinking test
    failed

  - folks-search-contacts: folks-search-contacts test failed

  - folks-metacontacts-linking: folks-metacontacts-linking test failed

  - folks: Unable to locate package telepathy-gabble

### Lowest (2)

  - cgroups-resource-control: 3_cgroups-resource-control test failed

  - tracker-indexing-local-storage: 18_tracker-indexing-local-storage
    test failed
