+++
date = "2020-06-19"
weight = 100

title = "V2020.1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2020.1** is the second **stable** release of the Apertis
v2020 [release
flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2020 release stream until September 2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2020.1 release are available in the following test
reports:

  - [APT
    images](https://qa.apertis.org/report/v2020/20200618.0)
  - [OSTree
    images](https://qa.apertis.org/report/v2020/20200618.0/ostree)
  - [NFS
    artifacts](https://qa.apertis.org/report/v2020/20200618.0/nfs)
  - [LXC
    containers](https://qa.apertis.org/report/v2020/20200618.0/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2020.7.

## Release flow

  - 2019 Q3: v2020dev0
  - 2019 Q4: v2020pre
  - 2020 Q1: v2020.0
  - 2020 Q2: **v2020.1**
  - 2020 Q3: v2020.2
  - 2020 Q4: v2020.3
  - 2021 Q1: v2020.4
  - 2021 Q2: v2020.5
  - 2021 Q3: v2020.6
  - 2021 Q4: v2020.7

### Release downloads

| [Apertis v2020.1 images](https://images.apertis.org/release/v2020/v2020.1/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2020/v2020.1/amd64/minimal/apertis_v2020-minimal-amd64-uefi_v2020.1.img.gz) | [target](https://images.apertis.org/release/v2020/v2020.1/amd64/target/apertis_v2020-target-amd64-uefi_v2020.1.img.gz) | [base SDK](https://images.apertis.org/release/v2020/v2020.1/amd64/basesdk/apertis_v2020-basesdk-amd64-sdk_v2020.1.vdi.gz) | [SDK](https://images.apertis.org/release/v2020/v2020.1/amd64/sdk/apertis_v2020-sdk-amd64-sdk_v2020.1.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2020/v2020.1/armhf/minimal/apertis_v2020-minimal-armhf-uboot_v2020.1.img.gz) | [target](https://images.apertis.org/release/v2020/v2020.1/armhf/target/apertis_v2020-target-armhf-uboot_v2020.1.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2020/v2020.1/arm64/minimal/apertis_v2020-minimal-arm64-uboot_v2020.1.img.gz)


The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2020 repositories

    deb https://repositories.apertis.org/apertis/ v2020 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2020-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small enhancements are aimed at this release stream.

This release includes:

  - the security updates from Debian Buster
  - the latest LTS Linux kernel on the 5.4.x series
  - HTTPS support in apertis-hawkbit-agent
  - a fix for apertis-update-manager to avoid initiating a rollback on
    crashes not due to the update process
  - an AppArmor profile fix for Bluez to access /dev/hidraw devices
  - updated support for ed25519 signatures in OSTree
  - expanded static delta signature support in OSTree
  - FIT support in U-Boot
  - completely disabled the GPL-3 "part" plugin in UDisks2
  - completely disabled the mdraid and crypto plugins in UDisks2
  - a new libwpa-client-dev packge to access the hostapd control protocol
  - a fix for UPower when starting up on a empty /var
  - dropped unsupported architectures from binutils and the GCC
    cross-compiler
  - fixes for Debos to avoid mangling /etc/resolv.conf
  - GitLab-CI image building pipeline

## Deprecations and ABI/API breaks

### The GitLab-CI image building pipeline will replace the Jenkins pipeline

The [GitLab-CI imabe building
pipeline](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/blob/apertis/v2020/.gitlab-ci.yml)
is now available on the v2020 branch as well. It currently co-exist with the
Jenkins pipeline which is still the reference pipeline for this Apertis
release.

During the v2020.2 cycle the project will evaluate having the GitLab-CI
pipeline as the reference pipeline, putting the Jenkins pipeline into
maintenance mode.

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2020 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2020)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2020/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at:

  https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

