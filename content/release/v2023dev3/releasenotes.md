+++
date = "2022-09-13"
weight = 100

title = "v2023dev3 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023dev3** is the fourth **development** release of the Apertis
v2023 stable release flow that will lead to the LTS **Apertis v2023.0**
release in March 2023.

This Apertis release is built on top of Debian Bullseye with several
customizations. It currently ships with the Linux kernel 5.18.x series
but later releases in the v2023 channel will track newer kernel versions
up to the next LTS.

Test results for the v2023dev3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023dev3/20220907.0018/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023dev3/20220907.0018/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023dev3/20220907.0018/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023dev3/20220907.0018/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - 2022 Q2: v2023dev2
  - **2022 Q3: v2023dev3**
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023dev3.0 images](https://images.apertis.org/release/v2023dev3/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023dev3/v2023dev3.0/amd64/fixedfunction/apertis_ostree_v2023dev3-fixedfunction-amd64-uefi_v2023dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev3/v2023dev3.0/amd64/hmi/apertis_ostree_v2023dev3-hmi-amd64-uefi_v2023dev3.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023dev3/v2023dev3.0/amd64/basesdk/apertis_v2023dev3-basesdk-amd64-sdk_v2023dev3.0.ova) | [SDK](https://images.apertis.org/release/v2023dev3/v2023dev3.0/amd64/sdk/apertis_v2023dev3-sdk-amd64-sdk_v2023dev3.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev3/v2023dev3.0/armhf/fixedfunction/apertis_ostree_v2023dev3-fixedfunction-armhf-uboot_v2023dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev3/v2023dev3.0/armhf/hmi/apertis_ostree_v2023dev3-hmi-armhf-uboot_v2023dev3.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev3/v2023dev3.0/arm64/fixedfunction/apertis_ostree_v2023dev3-fixedfunction-arm64-uboot_v2023dev3.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023dev3/v2023dev3.0/arm64/fixedfunction/apertis_ostree_v2023dev3-fixedfunction-arm64-rpi64_v2023dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev3/v2023dev3.0/arm64/hmi/apertis_ostree_v2023dev3-hmi-arm64-rpi64_v2023dev3.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023dev3 repositories

    deb https://repositories.apertis.org/apertis/ v2023dev3 target development sdk


## New features

### Support for the new UP Squared 6000 amd64 reference board

Due to the reduced availability of the MinnowBoard Turbot E3826/E3845 a new reference board have been chosen.
This new Apertis release provides full support to the [UP Squared 6000]({{< ref upsquared6000_setup.md >}})

### Support for lintian validation to packaging CI

In an effort to allow developers to check that their changes follow Debian's best practices, Apertis added
a lintian step in its CI with a custom profile. This results in a better developer experience, specially for
those new in Apertis.

### Improved automated license compliance

With the goal of providing a more reliable copyright information Apertis has improved its automated license
compliance. Now for packages which require a not straight forward configuration for license whitelist and
override, a full copyright report can be enabled, providing a finer configuration.

## Build and integration

### LAVA support for the UP Squared 6000 amd64 reference board

In relation to the support of the new amd64 reference board, the LAVA infrastructure now includes UP Squared 6000
boards. Thanks to this addition, now daily automated tests run in the new reference board, providing an improved
test coverage.

### New IoT image support

A new reference image for IoT products is now supported based on
[Eclipse Kanto](https://projects.eclipse.org/projects/iot.kanto) which provides a modular software stack.
Thanks to this image now product teams have a shortcut to develop their IoT products based on Apertis.

### Improved submission and monitoring of LAVA jobs

The CI in Apertis has been improved to have better control of the jobs submitted to LAVA. With the latest changes
the integration in more tight and simple. Thanks to this change now it is possible to get a finer grained information
about test failures in the Gitlab pipelines. Additionally it allows the use of short lived credential to access
not public repositories from LAVA.

### Reusable CI pipeline to create Flatpak runtime and applications

To provide a better developer experience, Apertis now provides a set of reusable CI pipelines for Flatpak,
which makes it easier to create both runtimes and applications. Following the steps in
[Flatpak CI]({{< ref "flatpak.md#building-flatpaks-on-gitlab-ci" >}})
developers can take advantage of this feature.

### Improved QA reporting

Following the latest changes in relation with [QA report](https://qa.apertis.org) the new site provides better
information to differentiate automated from manual tests. Additionally, a more flexible configuration allows
the creation of separate issues per release when a test fails on LAVA.

## Documentation and designs

### New recommended tooling for SDK API documentation

Following best practices on SDK API documentation Apertis chose **gtk-doc** as the preferred tool. Thanks
to the information found in [Documenting Apertis interfaces]{{< ref documenting-interfaces.md >}} developers
can easily integrate SDK API documentation to their packages which can later be accessed using **devhelp**.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No know breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev3-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #122](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/122) aum-offline-upgrade: test failed
- [Issue #127](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/127) aum-api: test failed
- [Issue #128](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/128) aum-ota-api: test failed
- [Issue #129](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/129) aum-ota-rollback-blacklist: test failed
- [Issue #130](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/130) aum-rollback-blacklist: test failed
- [Issue #134](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/134) aum-ota-signed: test failed
- [Issue #146](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/146) aum-ota-out-of-space: test failed
- [Issue #151](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/151) sdk-debos-image-building: test failed
- [Issue #166](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/166) UP squared 6000 board fails to bring up audio on v2023dev3
- [Issue #167](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/167) aum-ota-auto: test failed
- [Issue #169](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/169) aum-out-of-space: test failed
- [Issue #177](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/177) WPA3-SAE & WPA2-PSK mode connections does not succeed after wrong key attempt
- [Issue #178](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/178) sanity-check: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #8](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/8) arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #12](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/12) gettext-i18n: test failed
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #17](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/17) SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
- [Issue #22](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/22) debos sometimes fails to mount things
- [Issue #26](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/26) newport: test failed
- [Issue #28](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/28) Investigate test failure TestGetSourceMount
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #39](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/39) grilo: test failed
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page
- [Issue #62](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/62) apparmor-ofono: test failed
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #162](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/162) phab-gitlab-bridge duplicate bugs on Phabricator
- [Issue #175](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/175) apparmor-geoclue: test failed

### Low
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK