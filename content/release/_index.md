+++
date = "2020-10-01"
weight = 100

title = "Releases"
+++

This section contains the release notes and schedules for all current and previous releases of Apertis. Please refer to the [release schedule]({{< ref "releases.md" >}}) and [release flow]({{< ref "release-flow.md" >}}) for more information.

Next planned releases:

|Milestone                                  | 2022.5        | 2023.1        | v2024dev2     |
|---                                        | ---           | ---           | ---           |
|Start of Release Cycle                     | 2023-04-01    | 2023-04-01    | 2023-04-01    |
|Soft Feature Freeze                        | 2023-05-03    | 2023-05-10    | 2023-05-17    |
|Hard Feature Freeze / Soft Code Freeze     | 2023-05-10    | 2023-05-17    | 2023-05-24    |
|Release Candidate (RC1) / Hard Code Freeze | 2023-05-17    | 2023-05-24    | 2023-06-21    |
|Release                                    | 2023-06-01    | 2023-06-08    | 2023-06-22    |

{{% notice warning %}}
The release v2024dev2 will be the first based in Debian Bookworm, after the rebase effort is completed. Due to
this fact and the early stage of this release in the release cycle the following decisions have been made:
- The RC date for v2024dev2 was moved to 2023.06.21 in order to provide a more robust images after the rebase.
- The RC testing will be only one day since only automated LAVA testing will be done.
{{% /notice %}}
