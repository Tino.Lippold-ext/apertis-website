+++
title = "GSettings"
weight = 100

aliases = [
    "/old-developer/latest/programming-guide-gsettings.html",
    "/old-developer/v2019/programming-guide-gsettings.html",
    "/old-developer/v2020/programming-guide-gsettings.html",
    "/old-developer/v2021pre/programming-guide-gsettings.html",
    "/old-developer/v2022dev0/programming-guide-gsettings.html",
    "/old-wiki/Guidelines/GSettings"
]
date = "2016-12-12"
+++

[GSettings](https://developer.gnome.org/gio/stable/GSettings.html) is a
GLib API for storing persistent application settings (but not large
amounts of data or user documents). It is the successor to GConf, and is
similar in use to the Windows Registry. GSettings is used by Apertis for
application preferences and configuration data. Its main advantages over
databases or configuration files are its support for structured data
(using GVariant), and its fast reads. It has good integration with
GObject, with powerful features such as binding to object properties
with
[`g_settings_bind()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-bind).

Due to its tight integration with other GLib utilities, it should be
used in preference to (e.g.) [SQLite]( {{< ref "sqlite.md" >}} ) for
configuration data (but not for user data or documents, or high volumes
of data).

# Summary

- [Compile and install]( {{< ref "#schema-installation" >}} ) all schemas using
  the `GLIB_GSETTINGS` autoconf macro.
- Ensure schemas are [named consistently]( {{< ref "#schema-design" >}} ).
- Ensure schemas are [fully translatable]( {{< ref "#schema-design" >}} ).
- [Use relocatable schemas]( {{< ref "#relocatable-schemas" >}} ) where
  appropriate for instantiating the same schema at multiple paths.
- Do not use [schema overrides]( {{< ref "#schema-overrides" >}} ) in upstream
  code.

# Schema design

GSettings uses compiled schemas, which describe the available settings
and their types. A single schema should contain a group of related
settings, such as the settings for a single application. Every setting
used by an application must be in a schema, and that schema must be
compiled and installed. This can be done using the `@GSETTINGS_RULES@`
automake macro [provided by
GLib](https://developer.gnome.org/gio/stable/GSettings.html#GSettings.description).

Each schema has a unique identifier, the *schema ID*. The name of the file
describing the schema must be the schema ID followed by `.gschema.xml`.

Schemas in
[application bundles]( {{< ref "application-design.md#structure-of-an-application-bundle" >}} )
must follow a specific naming convention.  If there is a schema that should be
displayed in a system-wide preferences user interface, then its schema ID must
be the same as the
[bundle ID]( {{< ref "application-design.md#bundle-id" >}} ).
If there are schemas that should not be displayed in a system-wide preferences
user interface, then their schema IDs must start with the bundle ID followed by
a dot. Application authors can use either or both of these arrangements,
depending on how they intend for those preferences to appear.

For example, if an application bundle named `com.example.MyApp` installs a
schema with ID `com.example.MyApp` in a file named
`com.example.MyApp.gschema.xml`, the preferences in that schema can be
displayed in a system-wide preferences user interface. If the same application
bundle installs a schema with ID `com.example.MyApp.Advanced` in a file named
`com.example.MyApp.Advanced.gschema.xml`, the preferences in that second schema
will not be displayed in a system-wide preferences user interface, but can
still be used internally by the application.

All schemas should be translatable, which allows for configuration key
default values to differ for different locales. This can be useful for
settings which are known to be different in different countries. For
example, a setting specifying which is the first day of the week.
Schemas are automatically marked as translatable if intltool 0.50.1 is
in use, and the schema is listed in `po/POTFILES.in` as:

```
[type: gettext/gsettings]data/org.foo.MyApp.gschema.xml
```

# Schema installation

All schemas should be installed on the system. This is automatically
handled by the `@GSETTINGS_RULES@` automake macro, simply by listing the
schema file in `gsettings_SCHEMAS` in `Makefile.am`:

```
gsettings_SCHEMAS = org.foo.MyApp.gschema.xml
EXTRA_DIST = $(gsettings_SCHEMAS)
@GSETTINGS_RULES@
```

Note that the `GLIB_GSETTINGS` macro must have been used in
`configure.ac` for this to work.

By installing schemas, there is no need to check for schema or key
availability at run time, as all keys have default values provided by
the schema. Calls to
[`g_settings_list_schemas()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-list-schemas)
and
[`g_settings_list_keys()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-list-keys)
are almost always unnecessary except when using
[Relocatable schemas]( {{< ref "#relocatable-schemas" >}} ). Instead, schema
keys should be retrieved on the assumption that they exist, simply by calling
(e.g.)
[`g_settings_get_string()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-get-string)
without any checks.

# Relocatable schemas

GSettings has a feature called ‘relocatable schemas’ which is designed
specifically for situations where an unknown set of applications need to
use a common schema template — where one schema is used multiple times
at different GSettings paths. Each path has an instance of the schema
and all its keys, and represents the configuration for a single app.

In such specific situations, a single relocatable schema should be used
rather than copying the schema for each application.
[`g_settings_list_relocatable_schemas()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-list-relocatable-schemas)
should be used with
[`g_settings_new_with_path()`](https://developer.gnome.org/gio/stable/GSettings.html#g-settings-new-with-path)
to instantiate the schema for each path.

See the
[GSettings documentation](https://developer.gnome.org/gio/stable/GSettings.html#GSettings.description)
for more information.

# Schema overrides

GSettings override files are designed for vendors to be able to override
upstream-provided GSettings schema defaults, allowing for customisation
of their distributions of software. If used, they should be added by the
vendors using custom patches, and should not be added upstream in
Apertis.

# Debugging

The GSettings database for the current environment can be explored and
modified using the `gsettings` command line tool. See `gsettings --help`
for more information. Note that the tool needs to be run in the same
environment as the application in order to see the same GSettings
database.

# External links

- [GSettings documentation](https://developer.gnome.org/gio/stable/GSettings.html)
