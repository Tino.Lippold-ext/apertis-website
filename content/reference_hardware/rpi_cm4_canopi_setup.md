+++
date = "2022-09-16"
lastmod = "2022-09-16"
weight = 100

title = "Raspberry Pi CM4 CANOPi Setup"
+++

This is the setup recommended for developers using the Raspberry Pi CM4
(Compute Module 4) CANOPi device.
The [Eclipse KUKSA CANOPi](https://github.com/boschresearch/kuksa.hardware) is
a baseboard for the
[Raspberry Compute Module 4](https://www.raspberrypi.com/products/compute-module-4/).

# Required items

You should have at least:

- 1 Raspberry Pi CM4 CANOPi device.
- 1 Power Supply 12V/5A DC.
- 1 USB to serial cable/converter (3.3v is mandatory)
- 1 USB type A to micro B cable

# RPi CM4 CANOPi board setup

## Attach serial

{{% notice warning %}}
Please ensure you are using the USB to serial which is 3.3v!
{{% /notice %}}

1. Ensure the CANOPi device is off and USB to serial cable is not attached to
   USB

2. Connect serial to UART ports on the CANOPi device.

   ![CANOPi serial](/images/rpi-cm4-canopi-serial.jpg)

   Attach cable wires to the UART pins on the CANOPi. Keep in mind that RXD from
   USB serial should be connected to GPIO14 on CANOPi and TXD from USB to
   GPIO15.

3. Connect USB serial to host.

4. Connect terminal emulator on your host.

   Depending on USB serial and operational system you are using the name of UART
   may differ, as well as the terminal emulator. The parameters for your terminal
   emulator should be `115200/8-N-1`.

   For Linux-based systems it is usually `ttyUSB0` and you may use `minicom` or
   `screen` to connect to serial:
   ```
   minicom -b 115200 -o -D /dev/ttyUSB0
   ```
   or
   ```
   screen /dev/ttyUSB0 115200
   ```

## Update CM4 EEPROM

If you set up a fresh CM4 that has never been used before in CANOPi you need to
update the EEPROM first following the procedure described in
[Updating CM4 EEPROM for CANOPi](https://github.com/eclipse/kuksa.hardware/blob/main/sw_doc/update_eeprom.md).

## Board setup

RPi CM4 CANOPi with integrated eMMC flash can not boot from an SD card (this is
a CM4 hardware limitation), therefore you need to programm the integrated eMMC
flash.

1. Add a jumper the `nRPI_BOOT`/`disable` eMMC boot jumper as depicted in the
   following image:

   ![CANOPi USB boot](/images/rpi-cm4-canopi-usbboot.jpg)

2. Plug a USB type A to micro B cable into the USB-OTG Connector on the CANOPi.
   This connection is used to flash the board firmware using the
   [rpiboot](https://github.com/raspberrypi/usbboot) tool.

3. Power on the CANOPi board and run the `rpiboot` tool.

```
$ sudo ./rpiboot
RPIBOOT: build-date Aug 26 2022 version 20220815~145439 1e651a88
Waiting for BCM2835/6/7/2711...
Loading embedded: bootcode4.bin
Sending bootcode.bin
Successful read 4 bytes
Waiting for BCM2835/6/7/2711...
Loading embedded: bootcode4.bin
Second stage boot server
Loading embedded: start4.elf
File read: start4.elf
Second stage boot server done
```

   On the serial console, the CANOPi log should be something like:

```
OTP boardrev b03141 bootrom 48b0 48b0

RPi: BOOTLOADER release VERSION:2b28285b DATE: 2022/07/14 TIME: 19:58:04 BOOTMODE: 0x00000007 part: 0 BUILD_TIMESTAMP=1657825084 0x90bf56b3 0x00b03141 0x002796e2
PM_RSTS: 0x00001000
part 00000000 reset_info 00000000
uSD voltage 3.3V
Initialising SDRAM 'Samsung' 16Gb x1 total-size: 16 Gbit 3200
DDR 3200 0 0 16 152
```

4. At this point, a mass storage device should be exposed on your Linux host
   allowing you to access the CANOPi eMMC flash, e.g. `/dev/sda`.

5. [Download]({{< ref "download.md" >}}) the Apertis ARM64 RPi HMI or
   fixedfunction image and `.bmap` file.

6. Flash the image to the mass storage device (e.g. `/dev/sda`) using command:
```
sudo bmaptool copy path/to/image.img.gz /dev/sda
```

7. Once finished, power off the board and remove the `nRPI_BOOT`/`disable` eMMC
   boot jumper

8. Rebooting the board should now successfully boot Apertis.
