+++
date = "2020-06-10"
weight = 100

title = "ARM 32-bit Reference Hardware"
+++

The recommended ARM 32-bit hardware is the i.MX6 Sabrelite.
Please see its
[setup guide]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )
for first-time setup.
The following [optional extras]( {{< ref "/reference_hardware/extras.md" >}} )
may be of interest.

If you currently don't have access to any supported hardware, the `amd64`
images can be run on a
[virtual machine]( {{< ref "/guides/virtualbox.md" >}} ).

| Reference                                   | Hardware                                                                                                                                                                             | Comments                                                                                                                      |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------- |
| ARM reference device                        | i.MX6 Sabrelite                                                                                                                                                                      | [Setup guide]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )                                                   |
| Power source                                | The 5V+ power source unit that should come with the i.MX6 Sabrelite                                                                                                                  |                                                                                                                               |
| SD card                                     | Full-sized SD card (or microSD in full-size adapter), 16GB or larger                                                                                                                 | Speed class C10/U1 or better recommended                                                                                      |
| Recommended: 100 Mbit network switch or hub | Use a network connection that *does not* support gigabit                                                                                                                             | Workaround for [reliability issues with this particular hardware when using gigabit](https://community.nxp.com/thread/381119) |
| Recommended: LVDS panel touchscreen         | [HannStar HSD100PXN1-A00-C11](https://eu.mouser.com/ProductDetail/Boundary-Devices/NIT6X_101HANNSTAR/?qs=%2Fha2pyFaduhICqiA62NC348xLf7lkR37nWVKEUqzpogToe6HSSy1X%2FAMJ8uCmShD)       |                                                                                                                               |
| Recommended: LVDS panel cable               | Cable for LVDS panel                                                                                 |                                                                                                                               |
| Recommended: USB to serial adapter          | [USB\<-\>serial adapter cable](https://www.amazon.com/TRENDnet-Converter-Installation-Universal-TU-S9/dp/B0007T27H8)                                                                 | To look at the console                                                                                                        |
| Recommended: USB keyboard                   | USB keyboard                                                                                                                                                                         |                                                                                                                               |
| Optional: Attached camera                   | [OV5640-based 5MP MIPI attached camera module](https://boundarydevices.com/product/ov5640_camera_imx8m/)                                                                                  |                                                                                                                               |
