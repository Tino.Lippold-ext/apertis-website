+++
date = "2022-07-20"
weight = 100

title = "UP Squared 6000 board Setup in LAVA"
+++

# Setting up an UP Squared 6000 board for LAVA

These are the instructions to prepare a device for use as a DUT in the Apertis
LAVA automated test laboratory. See
[UP Squared 6000 board setup]( {{< ref "/reference_hardware/upsquared6000_setup.md" >}} )
for the recommended setup for developers' devices.

## Overview

These instructions are to set up a PXE boot server with `x86_64` stand-alone
GRUB to boot an [UP Squared 6000](https://up-board.org/up-squared-6000/) series
board ([Intel Atom x6425RE](https://ark.intel.com/content/www/us/en/ark/products/207899/intel-atom-x6425re-processor-1-5m-cache-1-90-ghz.html))
in LAVA. It will first boot with a kernel over TFTP and an NFS root file system,
then deploy a full OS image on an SD card and boot it with GRUB.

Note: Any EFI bootloader included in the OS image will not be used in
this scenario. A future improvement would be to make LAVA control the
EFI firmware directly rather than GRUB in order to boot from an
arbitrary EFI bootloader instead.

## PXE boot with GRUB

The first step is to set up the LAVA dispatcher to enable PXE boot with a GRUB
binary. There should already be a TFTP server installed as part of the LAVA
dispatcher set-up. The same server can be used to provide the PXE boot binary
(GRUB).

Create directory `/srv/tftp/lava-grub2/x86_64-efi/` on the dispatcher and
download the [latest GRUB binary](https://gitlab.collabora.com/lava/grub/-/jobs/artifacts/master/raw/core.efi?job=build-amd64)
to it.

Note: To build the GRUB binary (`core.efi`) from source:

```
$ git clone https://gitlab.collabora.com/lava/grub.git
$ cd grub/
$ ./bootstrap
$ ./autogen.sh
$ ./configure --with-platform=efi --target=x86_64 --disable-werror
$ make
$ make install
$ grub-mkstandalone -O x86_64-efi -o core.efi --modules "tftp net efinet linux efifwsetup part_msdos part_gpt btrfs ext2 fat"
```

Install dns-masq on the dispatcher:

```
$ apt install dnsmasq
```

Make it provide the GRUB binary for UP Squared 6000 boards, in
`/etc/dnsmasq.d/dhcp.conf`:

```
# UP Squared 6000 (x86_64)
dhcp-vendorclass=pxe-efi-x86_64,PXEClient:Arch:00007
dhcp-boot=tag:pxe-efi-x86_64,lava-grub2/x86_64-efi/core.efi
```

## Device set-up

Use the standard UEFI firmware provided with the UP Squared 6000 boards, with
settings reset to the factory defaults.

To configure the serial console, follow the instructions from the
[UP Squared 6000 setup]( {{< ref "/reference_hardware/upsquared6000_setup.md#serial-console" >}} ).

The Network interface needs to be enabled and boot order set to try PXE
IPv4/IPv6 first, to make LAVA take the priority over previously installed images
on the device. The following steps needs to be configured:

* Enter BIOS setup pressing `<DEL>` or `<ESC>` during boot.
* Go to tab `Advanced` -> `Network Stack Configuration`.
* Enable option `Network Stack`.
* Go to tab `Boot` -> `Boot Option #1`.
* Select option `Network:UEFI: PXE` for the IPv4/IPv6 variant you use.
* Press F4 to save and Y to confirm.
* Exit the menu and restart the system.

This should then automatically try PXE boot and launch GRUB.
