+++
title = "Application Design"
weight = 100

aliases = [
    "/old-developer/latest/appdev-design.html",
    "/old-developer/v2019/appdev-design.html",
    "/old-developer/v2020/appdev-design.html",
    "/old-developer/v2021pre/appdev-design.html",
    "/old-developer/v2022dev0/appdev-design.html",
]

date = "2016-12-12"
lastmod = "2022-02-01"

status = "Deprecated"
statusDescription = "This document covers frameworks that have been removed in v2022dev2."
+++

Apertis is a platform for automotive infotainment. With this in mind, it is intended to be used only with a touch screen interface by a driver or a passenger in a vehicle. The graphical user interface (GUI), or human-machine interface (HMI), will normally be made up of widgets provided by Mildenhall. Mildenhall takes the nature of Apertis into consideration so many of the widgets will be automatically sized and in some cases even placed for ease of use. There are some widgets which you will be able to place yourself. For more information, see the [API reference]({{< ref "apis-and-services.md" >}}).

You will also need to set some non GUI/HMI properties which will affect the behaviour of your application. It is important to get these properties right as they can affect user experience and performance of the system.

## Navigating to your application

You can set a user readable application name and the category in which your application should appear.

Apart from the executable name and application name, you need to also set an application name which will be shown when the user navigates through the menu.

Do:
- Pick a unique name where possible (for example, the name of the game for games)
- Select a name which clearly indicates what your application is about

Don't:
- Use any of the pre-installed application names (for example, Browser)

If you do not pick a good name for your application, it may be difficult for the user to find it. This could mean that your application is more difficult to find in the app store and is used less often.

Apart from selecting a good name for your application, you also need to assign it to an appropriate category in the menu. This will also help users navigate to your application. For example, chose "music" for a music player, "travel" for a navigational or map app or "video & TV" for a Blu-ray player app.

The category icon will be set for your application by default once.

## Audio behaviour

It is normal to have a number of applications competing for the attention of the driver or passengers at any given time. To help your application have the correct behaviour, there is a number of audio types available for you to chose from. Selecting the correct audio type is important so that your application takes or gives priority correctly in relation to other applications.

For example:
- a phone application is expected to interrupt audio from other applications unless the user has asked for no interruptions so you would choose "interrupt" audio type
- a "music" player application will continue playing music when a user is not looking at the user interface, but will be interrupted by a phone call

Selecting the wrong type of audio behaviour can have a negative impact on the user's experience, ranging from annoyance to distracting a driver, so it is important that you select the correct type of audio profile based on what your application does.

## Interrupt behaviour

You can chose how your application reacts when another application is opened by the user. Applications which provide a service to the user which should be persistent should keep running in the background. On the other hand, an application which requires active interaction from the user should normally be stopped or killed so that it does not take up resources while it is unused, which would provide a better experience to your user.

For example:
- a music or navigation application should keep running in the background while the user browses the internet in a browser
- a game can be killed or stopped while the user is not playing it

## Internet bandwidth

It is likely that there will be times when access to the internet will be limited. If your application requires internet access to work, you need to think about what proportion of the bandwidth it should be allocated.

## Structure of an application bundle

An Apertis **application bundle** is a set of resources installed as a single
unit. The app bundle can include open source / proprietary libraries,
services, and GUI programs.

See the [Application Bundle Specification]( {{< ref "bundle-spec.md" >}} )
for a more formal specification of the contents of application bundles.

### Bundle ID

Each Apertis application bundle is identified by a
[bundle ID]({{< ref "bundle-spec.md#bundle-id" >}}), which is a
[reversed domain name][Reverse domain name notation] controlled by the bundle
author.

For example, the owner of the domain name `example.net` controls the
reversed domain name `net.example` and might choose to name an app bundle
`net.example.ShoppingList`.

### Entry points

The UI programs in a bundle may have one or more entries in the launcher
menu. Applications are typically launched by tapping the menu entry,
or when a file of a particular type is opened.

Application bundles may also contain programs that do not appear in the
menu. These are mainly used when implementing [agents][Agent], which are
started automatically by the application framework and run in the background.

All of these ways a program could be started are referred to as
**entry points**, and each one has an **entry point ID**.

One entry point in each bundle should usually have the bundle ID as its entry
point ID: this is referred to as the **main entry point**.
If there are additional entry points, they must have names that start with
the bundle ID, followed by a dot.

For example, the `net.example.ShoppingList` application bundle might contain
a main entry point named `net.example.ShoppingList` and an [agent] named
`net.example.ShoppingList.CloudSync`.

<!-- External link definitions -->

[Reversed domain name notation]: https://en.wikipedia.org/wiki/Reverse_domain_name_notation
