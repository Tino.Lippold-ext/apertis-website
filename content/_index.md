+++
date = "2020-01-15"
lastmod = "2021-08-04"
weight = 100

title = "Home Page"

aliases = [
    "/old-wiki/Main_Page",
    "/old-wiki",
    "/old-designs",
    "/old-designs/latest",
    "/old-designs/v2019",
    "/old-designs/v2020",
    "/old-designs/v2021dev3",
    "/old-developer",
    "/old-developer/latest",
    "/old-developer/v2019",
    "/old-developer/v2020",
    "/old-developer/v2021pre",
    "/old-developer/v2022dev0",
]
+++

{{% introduction %}}
Apertis is a versatile
[open source infrastructure]({{< ref "platform-guide.md">}}), fit for a wide
variety of electronic devices, with a history within the automotive industry.
Security and modularity are two of its primary strengths. Apertis provides a
feature-rich framework for add-on software and resilient upgrade capabilities.
Beyond an operating system, it offers tools and cloud services to optimise
development and increase reliability.
{{% /introduction %}}

{{% homepage-column-set %}}

{{% homepage-column %}}
## Production Friendly

#### Apertis provides suitably licensed, tested releases.

Apertis aims to accomplish the following goals:

Ensure that all the software shipped in Apertis is
[open source or at least freely distributable]({{< ref "license-expectations.md" >}}),
so that downstreams are entitled to use, modify and redistribute work derived
from our deliverables.

Ensure that Apertis images targeting devices (such as target and fixedfunction), are
not subject to licensing constraints that may conflict with the regulatory
requirementt of some intended use cases.

Apertis undergoes regular
[automated and manual testing](https://qa.apertis.org) on the
[reference hardware]( {{< ref "/reference_hardware/_index.md" >}} ) with
[publicly available results](https://qa.apertis.org).

{{% link-box %}}
#### Further Links

* [Policy documentation]( {{< ref "policies" >}} )
* [Test Definitions](https://qa.apertis.org)
* [Test results](https://qa.apertis.org)
* [Quality assurance]( {{< ref "/qa/_index.md" >}} )
{{% /link-box %}}
{{% /homepage-column %}}


{{% homepage-column %}}
## Frequent Releases

#### A fresh stable version of Apertis is released yearly.

Each [Apertis release]({{< ref "release-flow.md#apertis-release-flow" >}}) is
based on the most recent mainline kernel LTS release and the current Debian
stable release. Since Debian releases roughly once every two years, that means
that there will typically be two Apertis product releases based on a single
Debian stable release. With the Linux kernel providing an LTS release yearly,
each Apertis product release will be based on a different (and then current)
Linux kernel LTS release.

The standard support period for each Apertis stable release is 7 quarters. In
other words from the initial release at the end of Q1 until the end of the next
year.

{{% link-box %}}
#### Further Links

* [Release Schedule]( {{< ref "releases.md" >}} )
* Current stable release: [v2023.1]( {{< ref "/release/v2023.1/releasenotes.md" >}} )
* Current old stable release: [v2022.5]( {{< ref "/release/v2022.5/releasenotes.md" >}} )
* Current development release: [v2024dev1]( {{< ref "/release/v2024dev1/releasenotes.md" >}} )
{{% /link-box %}}
{{% /homepage-column %}}


{{% homepage-column %}}
## Developer Ready

#### Ready to use infrastructure, automation and images.

Apertis provides infrastructure which is configured to enable a
[highly automated, optimised workflow]({{< ref "workflow-guide.md" >}}), with
CI pipelines performing automated testing and packaging.

To facilitate evaluation of Apertis and to provide a foundation for further
development, Apertis provides a number of
[pre-built images]({{< ref "download.md" >}}) for Intel (64-bit) and ARM
(32-bit and 64-bit) for use on the Apertis
[reference hardware]( {{< ref "/reference_hardware/_index.md" >}} ) platforms.

Additionally Apertis provides an [SDK image]({{< ref "virtualbox.md" >}}) to
simplify development for the platform.

Current issues are tracked in the [Apertis issues board](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues)

{{% link-box %}}
#### Further Links

* [Download the current images]( {{< ref "download.md" >}} )
* [Source code](https://gitlab.apertis.org)
* [Architectural documentation]( {{< ref "architecture" >}}) 
* [Development guides]( {{< ref "guides" >}} )
* [Concept designs]( {{< ref "concepts" >}} )
* [Apertis issues board](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues)
{{% /link-box %}}
{{% /homepage-column %}}

{{% /homepage-column-set %}}
