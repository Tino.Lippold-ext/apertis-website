+++
title = "License Exceptions"
short-description = "Document license exceptions for projects in Apertis"
weight = 100
aliases = [
	"/old-designs/latest/license-exceptions.html",
	"/old-designs/v2019/license-exceptions.html",
	"/old-designs/v2020/license-exceptions.html",
	"/old-designs/v2021dev3/license-exceptions.html",
]
outputs = [ "html", "pdf-in",]
date = "2019-04-16"
+++

Exceptions to the Apertis
[license expectations]({{< ref "license-expectations.md" >}}) are listed below.
Each exception must provide the following information:

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>The project name</td>
</tr>
<tr>
  <th>component</th>
  <td>The repository components apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>The date at which the exception was added to this document</td>
</tr>
<tr>
  <th>validator</th>
  <td>The name of the person who validated the exception</td>
</tr>
<tr>
  <th>rule</th>
  <td>The rules that are ignored by this exception</td>
</tr>
<tr>
  <th>reason</th>
  <td>A description of why the exception is granted and makes sense</td>
</tr>
</table>

## gcc-8

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>gcc-8</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>April 17, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>fredo</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
    <p>The GCC source package is granted exception to be present in target repository component
    because it produces binary packages covered by different licensing terms:</p>
    <ul>
      <li>the compiler packages are released under the GPL-3</li>
      <li> the <code>libgcc</code> runtime library is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
    </ul>
    <p>Programs compiled with GCC link to the <code>libgcc</code> library to implement some compiler intrinsics,
    which means that the <code>libgcc</code> must live in the <code>apertis:*:target</code> component
    since it is a direct runtime dependency of packages in the same component.</p>
    <p>For this reason, an exception is granted to the <code>gcc</code> source package
    on the ground that:</p>
    <ul>
      <li>code that is shipped on target devices (that is, <code>libgcc</code>) is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
      <li>the pure GPL-3 code is not meant to be shipped in target devices</li>
    </ul>
  </td>
</tr>
</table>

## gcc-10

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>gcc-10</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>August 11, 2021</td>
</tr>
<tr>
  <th>validator</th>
  <td>wlozano</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
    <p>The GCC source package is granted exception to be present in target repository component
    because it produces binary packages covered by different licensing terms:</p>
    <ul>
      <li>the compiler packages are released under the GPL-3</li>
      <li> the <code>libgcc</code> runtime library is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
    </ul>
    <p>Programs compiled with GCC link to the <code>libgcc</code> library to implement some compiler intrinsics,
    which means that the <code>libgcc</code> must live in the <code>apertis:*:target</code> component
    since it is a direct runtime dependency of packages in the same component.</p>
    <p>For this reason, an exception is granted to the <code>gcc</code> source package
    on the ground that:</p>
    <ul>
      <li>code that is shipped on target devices (that is, <code>libgcc</code>) is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
      <li>the pure GPL-3 code is not meant to be shipped in target devices</li>
    </ul>
  </td>
</tr>
</table>

## libtool

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>libtool</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>August 05, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>ritesh</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   libtool is granted exception to be present in target repository component<br/>
   because all the source files are licensed under the GPLv2 with the exception<br/>
   of build files, which are licensed under GPLv3.<br/>
   These build files are used only to build the binary package and are not<br/>
   GPLv3 violations for the built binary packages.<br/>
  </td>
</tr>
</table>

## elfutils

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>elfutils</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>September 17, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>andrewsh</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   <p><code>elfutils</code> is software dual-licensed as LGPL-3+ or GPL-2+, which
   means that any combined work using it has to be shipped under terms
   compatible with either of those two licenses. To avoid the effects of the
   GPL-3 provisions as required for the <code>target</code> repository, any
   combined work depending on any of the libraries provided by
   <code>elfutils</code> must be effectively licensed under the GPL-2 terms.
   </p>
   <p>
   The following binary packages are linking against <code>elfutils</code> in
   way that no GPL-3 restrictions need to be applied as they only ship
   executables that produce combined works under the GPL-2:
   </p>
   <ul>
     <li><code>linux-perf-4.19</code>: GPL-2, does not ship libraries,
         development tool not meant to be shipped on products
     <li><code>linux-kbuild-4.19</code>: GPL-2, does not ship libraries,
         development tool not meant to be shipped on products
     <li><code>bluez</code>: GPL-2, does not ship libraries
     <li><code>libglib2.0-bin</code>: LGPL-2.1, effectively combined to GPL-2,
         does not ship libraries
   </ul>
   <p>
   In addition, the <code>mesa</code> source package produces binary packages
   containing drivers that need to be linked to <code>libelf</code> and, in
   turn, get linked to graphical applications. This would impose LGPL-3+
   restrictions on <code>libelf</code> unless the application and all the other
   linked libraries can be combined as a GPL-2 work. This is not an acceptable
   restriction, so the affected drivers have been disabled, and no binary
   package produced from the <code>mesa</code> source package links to any
   library shipped by <code>elfutils</code>.
   </p>
  </td>
</tr>
</table>

## nftables

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>nftables</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>July  14, 2022</td>
</tr>
<tr>
  <th>validator</th>
  <td>wlozano</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   <p><code>nftables</code> includes Bison-exception as described in
    <a href="https://spdx.org/licenses/Bison-exception-2.2.html">Bison Exception</a>
    which clearly states that is possible to create a lager work that contains part or
    all of the Bison parser skeleton and distribute that work under terms of your choice,
    so long as that work isn't itself a parser generator using the skeleton or a modified
    version thereof as a parser skeleton.
   </p>
   <p>
    Based on these facts, the package is considered GPL-3 free.
   </p>
  </td>
</tr>
</table>

## libdb5.3

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>db5.3</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>December  28, 2022</td>
</tr>
<tr>
  <th>validator</th>
  <td>wlozano</td>
</tr>
<tr>
  <th>rule</th>
  <td>Sleepycat license</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   <p><code>libdb5.3</code> uses <a href="https://spdx.org/licenses/Sleepycat.html">Sleepycat License</a>
    which requires to provide complete source code for the DB software
    and any accompanying software that <b>uses the DB software</b>. Based on the interpretation of
    <b>uses</b> in this context, different restrictions apply while linking and/or storing data in libdb.
   </p>
   <p>
    Based on these facts, libdb5.3 package can be used by Open Source components although not recommended, but Private
    components should be aware of possible restrictions.
   </p>
  </td>
</tr>
</table>
